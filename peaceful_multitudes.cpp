#include <iostream>
#include <vector>

int main() {
    int64_t n; std::cin >> n;
    // dp[n][k] -- сколько множеств силы n, где минимальный элемент > k
    std::vector<std::vector<int64_t>> dp;
    for (int64_t i = 0; i < n; ++i) {
        std::vector<int64_t> temp(n);
        dp.push_back(temp);
    }

    for (int64_t i = 0; i < n; ++i) {
        dp[i][i] = 1;
    }

    for (int64_t i = 0; i < n; ++i) {
        for (int64_t j = i-1; j >= 0; --j) {
            dp[i][j] += 1;
            for (int64_t now = j+1; 3*now <= (i+1); ++now) {
                dp[i][j] += dp[i-now][2*now-1];
            }
        }
    }

    std::cout << dp[n-1][0] << std::endl;
}
