#include <iostream>

struct Node {
    int size = 1;
    vector<int> keys;                   // массив упорядоченных чисел
    vector<Node*> children;             // указатели на детей
};

class Btree {
public:
    ~Btree() {
        while (head != nullptr) del(0);
    }

    int find(int x) {
        if (head == nullptr || head->size <= x) return 0;
        return _find(head, x);
    }

    void add(int x) {
        if (head == nullptr)
    }


private:
    int _find(Node* vertex, int x) {
        if (vertex->children.size() == 0) return vertex->keys[x];
        int index = 0;
        while (true) {
            if (vertex->children[index]->size > x)  return _find(vertex->children[index], x);
            if (vertex->children[index]->size == x) return vertex->keys[x];
            x -= vertex->children[index]->size + 1;
            ++index;
        }
    }

    int t = 2;
    Node* head = nullptr;
};

int main() {
    Btree tree;

    int n; std::cin >> n;
    for (int useless = 0; useless < n; ++useless) {
        int c, k; std::cin >> c >> k;
        if (c == 1) {
            tree.add(k);
        } else if (c == 0) {
            std::cout << tree.find(k) << std::endl;
        } else if (c == -1) {
            tree.del(k);
        }
    }
}