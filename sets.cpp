#include <iostream>
#include <string>
#include <set>
#include <map>
#include <stdio.h>

int main() {
    int64_t N, M, K;
    scanf("%lld%lld%lld", &N, &M, &K);
    std::map<int64_t, std::set<int64_t>> map_of_real_sets;
    std::map<int64_t, std::set<int64_t>> map_of_elements;

    std::string str;
    for (int i = 0; i < K; ++i) {
        std::cin >> str;
        if (str == "ADD") {
            int64_t e, s; scanf("%lld%lld", &e, &s);
            if (map_of_real_sets.count(s) == 0) {
                std::set<int64_t> t;
                map_of_real_sets[s] = t;
            }
            if (map_of_elements.count(e) == 0) {
                std::set<int64_t> t;
                map_of_elements[e] = t;
            }
            map_of_real_sets[s].insert(e);
            map_of_elements[e].insert(s);
        } else if (str == "DELETE") {
            int64_t e, s; scanf("%lld%lld", &e, &s);
            map_of_real_sets[s].erase(e);
            map_of_elements[e].erase(s);
        } else if (str == "CLEAR") {
            int64_t s; scanf("%lld", &s);
            for (const auto& e : map_of_real_sets[s]) {
                map_of_elements[e].erase(s);
            }
            map_of_real_sets[s].clear();
        } else if (str == "LISTSET") {
            int64_t s; scanf("%lld", &s);
            if (map_of_real_sets[s].size() == 0) {
                printf("-1");
            }
            for (const auto& e : map_of_real_sets[s]) {
                printf("%lld ", e);
            }
            printf("\n");
        } else if (str == "LISTSETSOF") {
            int64_t e; scanf("%lld", &e);
            if (map_of_elements[e].size() == 0) {
                printf("-1");
            }
            for (const auto& s : map_of_elements[e]) {
                printf("%lld ", s);
            }
            printf("\n");
        }
    }
}