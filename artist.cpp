#include <iostream>
#include <vector>
#include "stdio.h"

int64_t M = 1'000'003;
int64_t A = 4363;
int64_t B = 6011;
int64_t C = 97849;

int64_t toHash(int64_t x, int64_t y) {
    return (x*A + B*y + C) % M;
}

int set(std::vector<std::vector<std::pair<int, int>>>& v, int64_t x, int64_t y) {
    int64_t hash = toHash(x, y);
    for (uint64_t i = 0; i < v[hash].size(); ++i) {
        if ((v[hash][i].first == x) && (v[hash][i].second == y)) return 0;
    }
    v[hash].push_back({x, y});
    return 1;
}

bool onCanvas(int64_t x, int64_t y, int64_t w, int64_t h) {
    return (1 <= x) && (x <= w) && (1 <= y) && (y <= h);
}

int setDirt(std::vector<std::vector<std::pair<int, int>>>& v, int64_t x, int64_t y, int64_t w, int64_t h) {
    int res = 0;
    if (onCanvas(x, y, w, h)) res += set(v, x, y);
    if (onCanvas(x-1, y, w, h)) res += set(v, x-1, y);
    if (onCanvas(x, y-1, w, h)) res += set(v, x, y-1);
    if (onCanvas(x+1, y, w, h)) res += set(v, x+1, y);
    if (onCanvas(x, y+1, w, h)) res += set(v, x, y+1);
    return res;
}

int main() {
    std::vector<std::vector<std::pair<int, int>>> v;
    for (int64_t i = 0; i < M; ++i) {
        std::vector<std::pair<int, int>> p;
        v.push_back(p);
    }
    int64_t res = 0;

    int64_t w, h, n; scanf("%lld%lld%lld", &w, &h, &n);
    for (int64_t i = 0; i < n; ++i) {
        int64_t x, y; scanf("%lld%lld", &x, &y);
        res += setDirt(v, x, y, w, h);
    }

    printf(((res == w*h) ? "Yes" : "No"));
}