#include <iostream>
#include <stdio.h>
#include <vector>

int partition(std::vector<int> &v, int left, int right) {
    if (left!=right) {
        std::swap(v[left + rand() % (right-left)], v[right]);
    }
    int x = v[right];
    int i = left-1;
    for (int j = left; j <= right; j++) {
        if (v[j] <= x) {
            std::swap(v[++i], v[j]);
        }
    }
    return i;
}

int k_stat(std::vector<int> &v, int k) {
    int left = 0;
    int right = v.size();

    while (true) {
        int middle = partition(v, left, right - 1);
        if (middle < k) {
            left = middle + 1;
        } else if (middle > k) {
            right = middle;
        } else {
            return v[middle];
        }
    }
}

int main() {
    int n, k;
    scanf("%d%d", &n, &k);

    std::vector<int> v;
    for (int i = 0; i < n; ++i) {
        int t; scanf("%d", &t);
        v.push_back(t);
    }

    printf("%d", k_stat(v, k));

    return 0;
}