#include <iostream>

#define N 1073741824

struct Node {
    int64_t l = 0;
    int64_t r = N-1;
    int64_t wait = 0;
    Node* left = nullptr;
    Node* right = nullptr;
};

class DynamicSegmentTree {
public:
    DynamicSegmentTree() {
        Node* node = new Node;
        head = node;
    }

    ~DynamicSegmentTree() {
        del(head);
    }

    // add x on [l, r]
    void add(int64_t x, int64_t l, int64_t r) {
        add(head, x, l, r);
    }

    int64_t get(int64_t index) {
        return get(head, index);
    }

private:
    void del(Node* vertex) {
        if (vertex->left != nullptr) del(vertex->left);
        if (vertex->right != nullptr) del(vertex->right);
        delete vertex;
    }

    int64_t get(Node* vertex, int64_t index) {
        if ((vertex->left != nullptr) && (index <= ((vertex->l + vertex->r)/2))) 
            return vertex->wait + get(vertex->left, index);
        if ((vertex->right != nullptr) && (index > ((vertex->l + vertex->r)/2))) 
            return vertex->wait + get(vertex->right, index);
        return vertex->wait;
    }

    // add x on [l, r]
    void add(Node* vertex, int64_t x, int64_t l, int64_t r) {
        if ((vertex->l == l) && (vertex->r == r)) {
            vertex->wait += x;
        } else if (((vertex->l + vertex->r)/2) < l) {
            if (vertex->right == nullptr) {
                vertex->right = new Node;
                vertex->right->l = 1 + ((vertex->l + vertex->r)/2);
                vertex->right->r = vertex->r;
            }
            add(vertex->right, x, l, r);
        } else if (((vertex->l + vertex->r)/2) >= r) {
            if (vertex->left == nullptr) {
                vertex->left = new Node;
                vertex->left->l = vertex->l;
                vertex->left->r = ((vertex->l + vertex->r)/2);
            }
            add(vertex->left, x, l, r);
        } else {
            if (vertex->right == nullptr) {
                vertex->right = new Node;
                vertex->right->l = 1 + ((vertex->l + vertex->r)/2);
                vertex->right->r = vertex->r;
            }
            add(vertex->right, x, vertex->right->l, r);
            if (vertex->left == nullptr) {
                vertex->left = new Node;
                vertex->left->l = vertex->l;
                vertex->left->r = ((vertex->l + vertex->r)/2);
            }
            add(vertex->left, x, l, vertex->left->r);
        }
    }

    Node* head = nullptr;
};

int main() {
    DynamicSegmentTree tree;

    int64_t q; std::cin >> q;
    for (int64_t useless = 0; useless < q; ++useless) {
        char t; int64_t x;
        std::cin >> t >> x;
        if (t == '+') {
            tree.add(x, x, N-1);      // +x on [x, 999999999]
        } else {
            std::cout << tree.get(x) << std::endl;      // get tree[x]
        }
    }
}