#include <iostream>
#include <vector>

const int inf = 1073741824;

int findMin(std::vector<std::vector<int>>& dp, int floor, int eggs) {
    int left = 1;
    int right = floor;

    while ((right - left) > 1) {
        int m = (left + right - 1) / 2;
        if (dp[m][eggs-1] < dp[floor-m][eggs]) left  = m+1;
        else                                   right = m+1; 
    }

    if (dp[left][eggs-1] == dp[floor-left][eggs]) return left;
    if (left == 1) return left;
    if (std::max(dp[left][eggs-1], dp[floor-left][eggs]) < std::max(dp[left-1][eggs-1], dp[floor-left+1][eggs]))
        return left;
    else
        return left-1;
}

int log2(int x) {
    int power = 1;
    int number = 2;
    while (number < x) {
        power += 1;
        number *= 2;
    }
    return power;
}

int main() {
    int n, k; std::cin >> n >> k;
    k = std::min(k, log2(n));

    std::vector<std::vector<int>> dp;
    for (int floor = 0; floor <= n; ++floor) {
        std::vector<int> temp(k+1, inf);
        dp.push_back(temp);
    }

    for (int j = 0; j <= k; ++j) {
        dp[0][j] = 0;
        if (n >= 1) dp[1][j] = 0;
    }

    for (int i = 0; i <= n; ++i) {
        if (i > 1)  dp[i][0] = inf;
        if (k >= 1) dp[i][1] = std::max(0, i-1);
    }

    std::vector<int> bestForFloor;
    bestForFloor.push_back(0);
    bestForFloor.push_back(0);

    for (int floor = 2; floor <= n; ++floor) {
        for (int eggs = 2; eggs <= k; ++eggs) {       
            int minIndex = findMin(dp, floor, eggs);
            dp[floor][eggs] = 1 + std::max(dp[minIndex][eggs-1], dp[floor-minIndex][eggs]);
        }
    }

    std::cout << ((dp[n][k] == inf) ? -1 : dp[n][k]) << std::endl;
}