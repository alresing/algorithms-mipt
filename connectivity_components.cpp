#include <iostream>
#include <vector>
#include <algorithm>


void print(const std::vector<auto>& v) {
    for (const auto& i : v)
        std::cout << i << " ";
    std::cout << std::endl;
}


void print(const std::vector<std::vector<auto>>& v) {
    for (const auto& i : v)
        print(i);
    std::cout << std::endl;
}


class Graph {
public:
    Graph(std::vector<std::vector<int>>& graph, int n) : graph(graph), n(n) {
        std::vector<bool> used(n, false);

        for (int i = 0; i < n; ++i)
            componentNumber.push_back(0);
        
        for (int i = 0; i < n; ++i) {
            if (!used[i]) {
                ++count;
                dfs(i, used, count);
            }
        }
    }   

    int countComponents() {
        return count;
    }

    std::vector<int> getComponents() {
        return componentNumber;
    }

private:
    std::vector<std::vector<int>> graph;
    int n = 0;
    int count = 0;
    std::vector<int> componentNumber;


    void dfs(int v, std::vector<bool>& used, int number) {
        used[v] = true;
        componentNumber[v] = number;
        for (int to : graph[v])
            if (!used[to])
                dfs(to, used, number);
    }
};


int main() {
    int n, m; std::cin >> n >> m;
    std::vector<std::vector<int>> vertices;
    for (int i = 0; i < n; ++i) {
        std::vector<int> temp;
        vertices.push_back(temp);
    }

    for (int i = 0; i < m; ++i) {
        int a, b; std::cin >> a >> b;
        vertices[a-1].push_back(b-1);
        vertices[b-1].push_back(a-1);
    }

    Graph graph(vertices, n);

    std::cout << graph.countComponents() << std::endl;
    std::vector<int> answer = graph.getComponents();
    for (int i : answer)
        std::cout << i << " ";
    std::cout << std::endl;
}
