#include <iostream>
#include <vector>


const int64_t MODULE = 1'000'000'007;


struct Three {
    int64_t a = 0;
    int64_t b = 0;
    int64_t y = 0;
};


void print(const std::vector<int64_t>& v) {
    for (const auto& i : v)
        std::cout << i << " ";
    std::cout << std::endl;
}


void print(const std::vector<std::vector<int64_t>>& m) {
    for (const auto& row : m)
        print(row);
    std::cout << std::endl;
}


void multVector(const std::vector<std::vector<int64_t>>& A, std::vector<int64_t>& v) {
    std::vector<int64_t> res(v.size(), 0);
    for (uint64_t i = 0; i < v.size(); ++i) {
        for (uint64_t j = 0; j < v.size(); ++j) {
            res[i] += A[i][j]*v[j];
            res[i] %= MODULE;
        }
    }
    v = res;
}


std::vector<std::vector<int64_t>> mult(const std::vector<std::vector<int64_t>>& left,
                                    const std::vector<std::vector<int64_t>>& right) {
    std::vector<std::vector<int64_t>> res;
    for (uint64_t i = 0; i < left.size(); ++i) {
        res.emplace_back();
        for (uint64_t j = 0; j < left.size(); ++j) {
            int64_t sum = 0;
            for (uint64_t k = 0; k < left.size(); ++k) {
                sum += left[i][k] * right[k][j];
                sum %= MODULE;
            }
            res[i].push_back(sum);
        }
    }
    return res;
}


std::vector<std::vector<int64_t>> e(int64_t n) {
    std::vector<std::vector<int64_t>> res;
    for (int64_t i = 0; i < n; ++i) {
        std::vector<int64_t> row(n, 0);
        row[i] = 1;
        res.push_back(row);
    }
    return res;
}


void binPow(std::vector<std::vector<int64_t>>& A, int64_t pow) {
    std::vector<std::vector<int64_t>> res = e(A.size());
    while (pow != 0) {
        if ((pow % 2) == 1) {
            res = mult(res, A);
        }
        A = mult(A, A);
        pow >>= 1;
    }
    A = res;
}


std::vector<std::vector<int64_t>> getMatrix(int64_t n) {
    std::vector<std::vector<int64_t>> matrix;
    for (int64_t i = 0; i < n; ++i) {
        std::vector<int64_t> row(n,0);
        if ((i-1) >= 0) row[i-1] = 1;
        row[i] = 1;
        if ((i+1) < n) row[i+1] = 1;
        matrix.push_back(row);
    }
    return matrix;
}


int main() {
    int64_t n, k; std::cin >> n >> k;
    std::vector<Three> segments(n);
    for (int64_t i = 0; i < n; ++i)
        std::cin >> segments[i].a >> segments[i].b >> segments[i].y;

    segments[segments.size()-1].b = k;

    std::vector<int64_t> paths(segments[0].y+1, 0);
    paths[paths.size()-1] = 1;

    for (int64_t i = 0; i < n; ++i) {
        std::vector<int64_t> new_paths(segments[i].y+1, 0);
        for (int64_t j = 0; j < new_paths.size(); ++j) {
            int64_t m = paths.size();
            int64_t n = new_paths.size();
            if ((m - (n - j)) >= 0)
                new_paths[j] = paths[paths.size() - (new_paths.size() - j)];
        }

        std::vector<std::vector<int64_t>> matrix = getMatrix(new_paths.size());
        binPow(matrix, segments[i].b - segments[i].a);
        multVector(matrix, new_paths);
        paths = new_paths;
    }

    std::cout << paths[paths.size()-1] << std::endl;
}
