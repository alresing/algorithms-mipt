#include <iostream>
#include <vector>
#include <limits>


const int64_t INF = std::numeric_limits<int64_t>::max();


class Graph {
public:
    Graph(size_t n) : graph(n) {}

    std::vector<std::pair<size_t, int64_t>>& get(size_t index) {
        return graph[index];
    }

    size_t size() const {
        return graph.size();
    }

private:
    std::vector<std::vector<std::pair<size_t, int64_t>>> graph;
};


void mark_dfs(size_t v, std::vector<int64_t>& dist, Graph& graph,
            std::vector<bool>& used) {
    if (used[v])
        return;
    dist[v] = -INF;
    used[v] = true;
    for (const auto& edge: graph.get(v))
        mark_dfs(edge.first, dist, graph, used);
}


std::vector<int64_t> ford(Graph& graph, size_t s) {
    std::vector<int64_t> dist(graph.size(), INF);
    dist[s] = 0;
    for (size_t _ = 0; _ < graph.size(); ++_)
        for (size_t v = 0; v < graph.size(); ++v)
            for (const auto& [to, weight]: graph.get(v))
                if (dist[v] < INF) {
                    if ((dist[v] <= (-INF + 1)) || ((dist[v] + weight) <= (-INF + 1)))
                        dist[to] = -INF + 1;
                    else
                        dist[to] = std::min(dist[to], dist[v] + weight);
                }

    std::vector<int64_t> old_dist = dist;

    for (size_t _ = 0; _ < graph.size(); ++_)
        for (size_t v = 0; v < graph.size(); ++v)
            for (const auto& [to, weight]: graph.get(v))
                if (dist[v] < INF) {
                    if ((dist[v] <= -INF) || ((dist[v] + weight) <= -INF))
                        dist[to] = -INF;
                    else
                        dist[to] = std::min(dist[to], dist[v] + weight);
                }

    std::vector<bool> used(graph.size(), false);
    for (size_t v = 0; v < graph.size(); ++v)
        if (dist[v] < old_dist[v])
            mark_dfs(v, dist, graph, used);

    return dist;
}


int main() {
    size_t n, m, s;
    std::cin >> n >> m >> s;

    Graph graph(n);

    for (size_t _ = 0; _ < m; ++_) {
        size_t from, to;
        int64_t w;
        std::cin >> from >> to >> w;
        graph.get(from-1).emplace_back(to-1, w);
    }

    std::vector<int64_t> answer = ford(graph, s-1);
    for (size_t u = 0; u < n; ++u) {
        if (answer[u] == INF)
            std::cout << '*' << std::endl;
        else if (answer[u] == -INF)
            std::cout << '-' << std::endl;
        else
            std::cout << answer[u] << std::endl;
    }
}
