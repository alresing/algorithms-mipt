#include <iostream>
#include <vector>


enum Color {
    WHITE,
    GRAY,
    BLACK
};


class Graph {
public:
    Graph(std::vector<std::vector<int>>& graph) : graph(graph) {}

    const std::vector<int>& get(int v) const {
        return graph[v];
    }

    int size() const {
        return graph.size();
    }

private:
    std::vector<std::vector<int>> graph;
};


int dfs(int v, std::vector<Color>& color, const Graph& graph) {
    color[v] = GRAY;
    for (int to : graph.get(v)) {
        if (color[to] == GRAY)
            return to;
        else if (color[to] == WHITE) {
            int vertex = dfs(to, color, graph);
            if (vertex != -1)
                return vertex;
        }
    }
    color[v] = BLACK;
    return -1;
}


bool findCycle(int v, const Graph& graph, std::vector<int>& cycle, int cycleVertex) {
    cycle.push_back(v);
    for (int to : graph.get(v)) {
        if (to == cycleVertex) {
            return true;
        } else {
            bool found = findCycle(to, graph, cycle, cycleVertex);
            if (found)
                return true;
        }
    }
    cycle.pop_back();
    return false;
}


std::vector<int> getCycle(const Graph& graph) {
    std::vector<Color> color(graph.size(), WHITE);
    int cycleVertex = -1;
    bool cycled = false;
    for (int i = 0; i < graph.size(); ++i) {
        if (color[i] == WHITE) {
            cycleVertex = dfs(i, color, graph);
            if (cycleVertex != -1) {
                cycled = true;
                break;
            }
        }
    }

    std::vector<int> cycle;

    if (!cycled)
        return cycle;

    findCycle(cycleVertex, graph, cycle, cycleVertex);
    return cycle;
}


int main() {
    int n, m; std::cin >> n >> m;
    std::vector<std::vector<int>> vertices;
    for (int i = 0; i < n; ++i) {
        std::vector<int> temp;
        vertices.push_back(temp);
    }

    for (int i = 0; i < m; ++i) {
        int a, b; std::cin >> a >> b;
        vertices[a-1].push_back(b-1);
    }

    Graph graph(vertices);

    std::vector<int> cycle = getCycle(graph);
    if (cycle.size() != 0) {
        std::cout << "YES" << std::endl;
        for (int i: cycle)
            std::cout << i+1 << " ";
        std::cout << std::endl;
    } else {
        std::cout << "NO" << std::endl;
    }
}
