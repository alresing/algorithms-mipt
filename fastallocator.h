#pragma once

#include <vector>
#include <memory>
#include <iostream>
#include <type_traits>


#define UNUSED(x) (void)(x)

const int bucket = 100;


template <size_t chunkSize>
class FixedAllocator {
public:
    FixedAllocator();
    ~FixedAllocator();
    void* allocate();
    void deallocate(void* ptr, size_t n);

private:
    size_t i = bucket;
    std::vector<void*> ptrs;
};


template <typename T>
class FastAllocator {
public:
    FastAllocator();
    FastAllocator(const FastAllocator& other);
    template <typename U>
    FastAllocator(const FastAllocator<U>& other);
    ~FastAllocator();
    T* allocate(size_t n);
    void deallocate(T* ptr, size_t n);
    static FastAllocator select_on_container_copy_construction();
    using value_type = T;

    template <typename U>
    struct rebind {
        using other = FastAllocator<U>;
    };

private:
    std::shared_ptr<FixedAllocator<4>> fixed4;
    std::shared_ptr<FixedAllocator<24>> fixed8;
};

template <typename T, typename Allocator = std::allocator<T>>
class List {
private:
    struct Node {
        T* value = nullptr;
        Node* next = nullptr;
        Node* prev = nullptr;
    };

public:
    struct const_iterator;

    struct iterator {
    friend List<T, Allocator>::const_iterator::const_iterator(const List<T, Allocator>::iterator& it);
    public:
        iterator(Node* ptr);
        iterator& operator++();
        iterator operator++(int);
        iterator& operator--();
        iterator operator--(int);
        bool operator==(const iterator& it) const;
        bool operator!=(const iterator& it) const;
        T& operator*();
        T* operator->();

        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using pointer = value_type*;
        using reference = value_type&;

    private:
        Node* ptr = nullptr;
    };

    struct const_iterator {
    friend void List<T, Allocator>::insert(const_iterator it, const T& element);
    friend void List<T, Allocator>::erase(const_iterator it);
    public:
        const_iterator(Node* ptr);
        const_iterator(const iterator& it);
        const_iterator& operator++();
        const_iterator operator++(int);
        const_iterator& operator--();
        const_iterator operator--(int);
        bool operator==(const const_iterator& it) const;
        bool operator!=(const const_iterator& it) const;
        const T& operator*() const;
        const T* operator->() const;

        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using pointer = value_type*;
        using reference = const value_type&;

    private:
        Node* ptr = nullptr;
    };

    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    explicit List(const Allocator& alloc = Allocator());
    List(size_t count, const T& value, const Allocator& alloc = Allocator());
    List(size_t count, const Allocator& alloc = Allocator());
    List(const List& other);
    List& operator=(const List& other);
    ~List();
    Allocator& get_allocator();
    size_t size() const;
    void push_back(const T& element);
    void pop_back();
    void push_front(const T& element);
    void pop_front();
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    const_reverse_iterator rbegin() const;
    reverse_iterator rend();
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;
    void insert(const_iterator it, const T& element);
    void erase(const_iterator it);

private:
    void push_back();

    Allocator allocT;
    using AllocatorN = typename std::allocator_traits<Allocator>::template rebind_alloc<Node>;
    AllocatorN allocN;

    size_t size_ = 0;
    Node* first = allocN.allocate(1);
    Node* last = allocN.allocate(1);
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <size_t chunkSize>
FixedAllocator<chunkSize>::FixedAllocator() {
    //std::cout << "create FixedAllocator for " << chunkSize << std::endl;
}

template <size_t chunkSize>
FixedAllocator<chunkSize>::~FixedAllocator() {
    //std::cout << "delete FixedAllocator for " << chunkSize << std::endl;
    for (auto ptr: ptrs)
        ::operator delete(ptr);
}

template <size_t chunkSize>
void* FixedAllocator<chunkSize>::allocate() {
    //std::cout << "allocate FixedAllocator for " << chunkSize << std::endl;
    if (i == bucket) {
        i = 0;
        ptrs.push_back(::operator new(chunkSize*bucket));
    }
    int8_t* ptr = reinterpret_cast<int8_t*>(ptrs[ptrs.size()-1]);
    return ptr + (i++)*chunkSize;
}

template <size_t chunkSize>
void FixedAllocator<chunkSize>::deallocate(void* ptr, size_t n) {
    //std::cout << "deallocate FixedAllocator for " << chunkSize << std::endl;
    UNUSED(ptr);
    UNUSED(n);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename T>
FastAllocator<T>::FastAllocator() {
    //std::cout << "create FastAllocator" << std::endl;
    fixed4 = std::make_shared<FixedAllocator<4>>();
    fixed8 = std::make_shared<FixedAllocator<24>>();
}

template <typename T>
FastAllocator<T>::FastAllocator(const FastAllocator<T>& other) {
    //std::cout << "copy FastAllocator" << std::endl;
    fixed4 = other.fixed4;
    fixed8 = other.fixed8;
}

template <typename T>
template <typename U>
FastAllocator<T>::FastAllocator(const FastAllocator<U>& other) : FastAllocator() {
    UNUSED(other);
}

template <typename T>
FastAllocator<T>::~FastAllocator() {
    //std::cout << "delete FastAllocator" << std::endl;
}
    
template <typename T>
T* FastAllocator<T>::allocate(size_t n) {
    //std::cout << "allocate FastAllocator n: " << n << std::endl;
    // std::cout << n*sizeof(T) << " " << sizeof(T) << std::endl;
    if (n*sizeof(T) == 4)
        return reinterpret_cast<T*>(fixed4->allocate());
    else if (n*sizeof(T) == 24)
        return reinterpret_cast<T*>(fixed8->allocate());
    else
        return reinterpret_cast<T*>(::operator new(n*sizeof(T)));
}

template <typename T>
void FastAllocator<T>::deallocate(T* ptr, size_t n) {
    //std::cout << "deallocate FastAllocator n: " << n << std::endl;
    if (sizeof(T)*n == 4)
        fixed4->deallocate(ptr, n);
    else if (sizeof(T)*n == 24)
        fixed8->deallocate(ptr, n);
    else
        ::operator delete(reinterpret_cast<void*>(ptr));
}

template <typename T>
FastAllocator<T> FastAllocator<T>::select_on_container_copy_construction() {
    return FastAllocator<T>();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename T, typename Allocator>
List<T, Allocator>::List(const Allocator& alloc) : allocT(alloc) {
    // std::cout << "construct empty" << std::endl;
    first->prev = last;
    last->next = first;
}

template <typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T& value, const Allocator& alloc) : allocT(alloc) {
    // std::cout << "construct count with value" << std::endl;
    first->prev = last;
    last->next = first;

    for (int i = 0; i < count; ++i)
        push_back(value);
}

template <typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const Allocator& alloc) : allocT(alloc) {
    // std::cout << "construct count without value" << std::endl;
    first->prev = last;
    last->next = first;

    for (size_t i = 0; i < count; ++i)
        push_back();
}

template <typename T, typename Allocator>
List<T, Allocator>::List(const List& other) :
                allocT(std::allocator_traits<Allocator>::select_on_container_copy_construction(other.allocT)),
                allocN(std::allocator_traits<AllocatorN>::select_on_container_copy_construction(other.allocN)) {
    // std::cout << "construct from other" << std::endl;
    first->prev = last;
    last->next = first;

    for (const auto& el: other)
        push_back(el);
}

template <typename T, typename Allocator>
List<T, Allocator>& List<T, Allocator>::operator=(const List<T, Allocator>& other) {
    // std::cout << "construct equalty" << std::endl;
    if (this == &other)
        return *this;

    while (size_ != 0)
        pop_back();

    if (std::allocator_traits<Allocator>::propagate_on_container_copy_assignment::value)
        allocT = other.allocT;
        
    if (std::allocator_traits<AllocatorN>::propagate_on_container_copy_assignment::value)
        allocN = other.allocN;

    for (const auto& el: other)
        push_back(el);

    return *this;
}

template <typename T, typename Allocator>
List<T, Allocator>::~List() {
    // std::cout << "destroy" << std::endl;
    while (size_ != 0) pop_back();
    allocN.deallocate(first, 1);
    allocN.deallocate(last, 1);
}

template <typename T, typename Allocator>
Allocator& List<T, Allocator>::get_allocator() {
    return allocT;
}

template <typename T, typename Allocator>
size_t List<T, Allocator>::size() const {
    return size_;
}

template <typename T, typename Allocator>
void List<T, Allocator>::push_back(const T& element) {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value, element);
    new_node->next = last->next;
    new_node->prev = last;
    last->next->prev = new_node;
    last->next = new_node;
}

template <typename T, typename Allocator>
void List<T, Allocator>::push_back() {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value);
    new_node->next = last->next;
    new_node->prev = last;
    last->next->prev = new_node;
    last->next = new_node;
}

template <typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
    --size_;
    Node* to_del = last->next;
    to_del->next->prev = last;
    last->next = to_del->next;
    std::allocator_traits<Allocator>::destroy(allocT, to_del->value);
    allocT.deallocate(to_del->value, 1);
    std::allocator_traits<AllocatorN>::destroy(allocN, to_del);
    allocN.deallocate(to_del, 1);
}

template <typename T, typename Allocator>
void List<T, Allocator>::push_front(const T& element) {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value, element);
    new_node->next = first;
    new_node->prev = first->prev;
    first->prev->next = new_node;
    first->prev = new_node;
}

template <typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
    --size_;
    Node* to_del = first->prev;
    to_del->prev->next = first;
    first->prev = to_del->prev;
    std::allocator_traits<Allocator>::destroy(allocT, to_del->value);
    allocT.deallocate(to_del->value, 1);
    std::allocator_traits<AllocatorN>::destroy(allocN, to_del);
    allocN.deallocate(to_del, 1);
}


template <typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::begin() {
    return iterator(first->prev);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::begin() const {
    return const_iterator(first->prev);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::end() {
    return iterator(last);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::end() const {
    return const_iterator(last);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cbegin() const {
    return const_iterator(first->prev);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cend() const {
    return const_iterator(last);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rbegin() {
    return reverse_iterator(last);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rbegin() const {
    return const_reverse_iterator(last);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rend() {
    return reverse_iterator(first->prev);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rend() const {
    return const_reverse_iterator(first->prev);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crbegin() const {
    return const_reverse_iterator(last);
}

template <typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crend() const {
    return const_reverse_iterator(first->prev);
}

template <typename T, typename Allocator>
void List<T, Allocator>::insert(const_iterator it, const T& element) {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value, element);
    new_node->next = it.ptr->next;
    new_node->prev = it.ptr;
    it.ptr->next->prev = new_node;
    it.ptr->next = new_node;
}

template <typename T, typename Allocator>
void List<T, Allocator>::erase(const_iterator it) {
    --size_;
    it.ptr->prev->next = it.ptr->next;
    it.ptr->next->prev = it.ptr->prev;
    std::allocator_traits<Allocator>::destroy(allocT, it.ptr->value);
    allocT.deallocate(it.ptr->value, 1);
    std::allocator_traits<AllocatorN>::destroy(allocN, it.ptr);
    allocN.deallocate(it.ptr, 1);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename T, typename Allocator = std::allocator<T>>
List<T, Allocator>::iterator::iterator(List<T, Allocator>::Node* ptr) : ptr(ptr) {}

template <typename T, typename Allocator = std::allocator<T>>
typename List<T, Allocator>::iterator& List<T, Allocator>::iterator::operator++() {
    ptr = ptr->prev;
    return *this;
}

template <typename T, typename Allocator = std::allocator<T>>
typename List<T, Allocator>::iterator List<T, Allocator>::iterator::operator++(int) {
    List<T, Allocator>::iterator copy = *this;
    ++*this;
    return copy;
}

template <typename T, typename Allocator = std::allocator<T>>
typename List<T, Allocator>::iterator& List<T, Allocator>::iterator::operator--() {
    ptr = ptr->next;
    return *this;
}

template <typename T, typename Allocator = std::allocator<T>>
typename List<T, Allocator>::iterator List<T, Allocator>::iterator::operator--(int) {
    List<T, Allocator>::iterator copy = *this;
    --*this;
    return copy;
}

template <typename T, typename Allocator = std::allocator<T>>
bool List<T, Allocator>::iterator::operator==(const iterator& it) const {
    return ptr == it.ptr;
}

template <typename T, typename Allocator = std::allocator<T>>
bool List<T, Allocator>::iterator::operator!=(const iterator& it) const {
    return !(*this == it);
}

template <typename T, typename Allocator = std::allocator<T>>
T& List<T, Allocator>::iterator::operator*() {
    return *(ptr->value);
}

template <typename T, typename Allocator = std::allocator<T>>
T* List<T, Allocator>::iterator::operator->() {
    return ptr->value;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename T, typename Allocator = std::allocator<T>>
List<T, Allocator>::const_iterator::const_iterator(List<T, Allocator>::Node* ptr) : ptr(ptr) {}

template <typename T, typename Allocator = std::allocator<T>>
List<T, Allocator>::const_iterator::const_iterator(const List<T, Allocator>::iterator& it) : ptr(it.ptr) {}


template <typename T, typename Allocator = std::allocator<T>>
typename List<T, Allocator>::const_iterator& List<T, Allocator>::const_iterator::operator++() {
    ptr = ptr->prev;
    return *this;
}

template <typename T, typename Allocator = std::allocator<T>>
typename List<T, Allocator>::const_iterator List<T, Allocator>::const_iterator::operator++(int) {
    List<T, Allocator>::const_iterator copy = *this;
    ++*this;
    return copy;
}

template <typename T, typename Allocator = std::allocator<T>>
typename List<T, Allocator>::const_iterator& List<T, Allocator>::const_iterator::operator--() {
    ptr = ptr->next;
    return *this;
}

template <typename T, typename Allocator = std::allocator<T>>
typename List<T, Allocator>::const_iterator List<T, Allocator>::const_iterator::operator--(int) {
    List<T, Allocator>::const_iterator copy = *this;
    --*this;
    return copy;
}

template <typename T, typename Allocator = std::allocator<T>>
bool List<T, Allocator>::const_iterator::operator==(const const_iterator& it) const {
    return ptr == it.ptr;
}

template <typename T, typename Allocator = std::allocator<T>>
bool List<T, Allocator>::const_iterator::operator!=(const const_iterator& it) const {
    return !(*this == it);
}

template <typename T, typename Allocator = std::allocator<T>>
const T& List<T, Allocator>::const_iterator::operator*() const {
    return *(ptr->value);
}

template <typename T, typename Allocator = std::allocator<T>>
const T* List<T, Allocator>::const_iterator::operator->() const {
    return ptr->value;
}
