//----------------------------------------------------------------
// Get type by index
//----------------------------------------------------------------

template <size_t index, typename T, typename... Args>
struct get_type {
    using type = typename get_type<index - 1, Args...>::type;
};

template <typename T, typename... Args>
struct get_type<0, T, Args...> {
    using type = T;
};

template <size_t index, typename... Args>
using get_type_t = typename get_type<index, Args...>::type;

//----------------------------------------------------------------
// Tuple
//----------------------------------------------------------------

template <typename T, typename... Args>
class Tuple {
template <size_t index, typename... GetArgs>
friend get_type_t<index, GetArgs...>& get(Tuple<GetArgs...>& tuple);
template <size_t index, typename... GetArgs>
friend const get_type_t<index, GetArgs...>& get(const Tuple<GetArgs...>& tuple);
template <size_t index, typename... GetArgs>
friend get_type_t<index, GetArgs...>&& get(Tuple<GetArgs...>&& tuple);
template <size_t index, typename... GetArgs>
friend const get_type_t<index, GetArgs...>&& get(const Tuple<GetArgs...>&& tuple);

template <typename... LArgs, typename... RArgs>
friend auto _tupleCat(Tuple<LArgs...>& lhs, Tuple<RArgs...>& rhs);
template <typename... LArgs, typename... RArgs>
friend auto _tupleCat(Tuple<LArgs...>&& lhs, Tuple<RArgs...>& rhs);
template <typename... LArgs, typename... RArgs>
friend auto _tupleCat(Tuple<LArgs...>& lhs, Tuple<RArgs...>&& rhs);
template <typename... LArgs, typename... RArgs>
friend auto _tupleCat(Tuple<LArgs...>&& lhs, Tuple<RArgs...>&& rhs);

public:
    Tuple() {}

    template <typename TConstr, typename... ArgsConstr,
        typename = std::enable_if_t<!std::is_same_v<std::decay_t<TConstr>, Tuple<T, Args...>>>>
    Tuple(TConstr&& arg, ArgsConstr&&... args) 
        : head(std::forward<TConstr>(arg)), tail(std::forward<ArgsConstr>(args)...)  {}

    template <typename TConstr, typename... ArgsConstr>
    Tuple(const Tuple<TConstr, ArgsConstr...>& other)
        : head(other.head), tail(other.tail) {}

    template <typename TConstr, typename... ArgsConstr>
    Tuple(Tuple<TConstr, ArgsConstr...>& other)
        : head(std::forward<TConstr>(other.head)), tail(std::forward<Tuple<ArgsConstr...>>(other.tail)) {}

    template <typename TConstr, typename... ArgsConstr>
    Tuple(Tuple<TConstr, ArgsConstr...>&& other) 
        : head(std::forward<TConstr>(other.head)), tail(std::forward<Tuple<ArgsConstr...>>(other.tail)) {}

    template <typename TConstr, typename... ArgsConstr>
    Tuple<T, Args...>& operator=(const Tuple<TConstr, ArgsConstr...>& other) {
        if (this == &other)
            return *this;
        head = other.head;
        tail = other.tail;
        return *this;
    }

    template <typename TConstr, typename... ArgsConstr>
    Tuple<T, Args...>& operator=(Tuple<TConstr, ArgsConstr...>& other) {
        if (this == &other)
            return *this;
        head = other.head;
        tail = other.tail;
        return *this;
    }

    template <typename TConstr, typename... ArgsConstr>
    Tuple<T, Args...>& operator=(Tuple<TConstr, ArgsConstr...>&& other) {
        if (this == &other)
            return *this;
        head = std::move(other.head);
        tail = std::move(other.tail);
        return *this;
    }

    ~Tuple() {};

    void swap(Tuple<T, Args...>& other) {
        std::swap(head, other.head);
        tail.swap(other.tail);
    }

    template<typename TConstr, typename... ArgsConstr, typename Other, typename... Others>
    Tuple(Tuple<TConstr, ArgsConstr...>& first, Other&& other, Others&&... others) 
        : head(first.head),
          tail(first.tail, std::forward<Other>(other), std::forward<Others>(others)...) {}

    template<typename TConstr, typename Other, typename... Others>
    Tuple(Tuple<TConstr>& first, Other&& other, Others&&... others)
        : head(first.head),
          tail(std::forward<Other>(other), std::forward<Others>(others)...) {}

    template<typename TConstr, typename... ArgsConstr, typename Other, typename... Others>
    Tuple(Tuple<TConstr, ArgsConstr...>&& first, Other&& other, Others&&... others)
        : head(std::move(first.head)),
          tail(std::move(first.tail), std::forward<Other>(other), std::forward<Others>(others)...) {}

    template<typename TConstr, typename Other, typename... Others>
    Tuple(Tuple<TConstr>&& first, Other&& other, Others&&... others)
        : head(std::move(first.head)),
          tail(std::forward<Other>(other), std::forward<Others>(others)...) {}

    T head;
    Tuple<Args...> tail;
};


template <typename T>
class Tuple<T> {
template <size_t index, typename... GetArgs>
friend get_type_t<index, GetArgs...>& get(Tuple<GetArgs...>& tuple);
template <size_t index, typename... GetArgs>
friend const get_type_t<index, GetArgs...>& get(const Tuple<GetArgs...>& tuple);
template <size_t index, typename... GetArgs>
friend get_type_t<index, GetArgs...>&& get(Tuple<GetArgs...>&& tuple);
template <size_t index, typename... GetArgs>
friend const get_type_t<index, GetArgs...>&& get(const Tuple<GetArgs...>&& tuple);

public:
    Tuple() {}

    template <typename TConstr,
        typename = std::enable_if_t<!std::is_same_v<std::decay_t<TConstr>, Tuple<std::decay_t<T>> >>>
    Tuple(TConstr&& arg) : head(std::forward<TConstr>(arg)) {}
    
    template <typename S>
    Tuple(const Tuple<S>& other) : head(other.head) {}

    template <typename S>
    Tuple(Tuple<S>& other) : head(other.head) {}

    template <typename S>
    Tuple(Tuple<S>&& other) : head(std::move(other.head)) {}

    template <typename S>
    Tuple<T>& operator=(const Tuple<S>& other) {
        head = other.head;
        return *this;
    }

    template <typename S>
    Tuple<T>& operator=(Tuple<S>& other) {
        head = other.head;
        return *this;
    }

    template <typename S>
    Tuple<T>& operator=(Tuple<S>&& other)  {
        head = std::move(other.head);
        return *this;
    }

    Tuple<T>& operator=(const Tuple<T>& other) {
        if (this == &other)
            return *this;
        head = other.head;
        return *this;
    }

    Tuple<T>& operator=(Tuple<T>& other) {
        if (this == &other)
            return *this;
        head = other.head;
        return *this;
    }

    Tuple<T>& operator=(Tuple<T>&& other)  {
        if (this == &other)
            return *this;
        head = std::move(other.head);
        return *this;
    }

    ~Tuple() {};

    void swap(Tuple<T>& other) {
        std::swap(head, other.head);
    }

    T head;
};

//----------------------------------------------------------------
// Get by index
//----------------------------------------------------------------

template <size_t index, typename... Args>
get_type_t<index, Args...>& get(Tuple<Args...>& tuple) {
    if constexpr (index == 0)
        return tuple.head;
    else
        return get<index - 1>(tuple.tail);
}

template <size_t index, typename... Args>
const get_type_t<index, Args...>& get(const Tuple<Args...>& tuple) {
    if constexpr (index == 0)
        return tuple.head;
    else
        return get<index - 1>(tuple.tail);   
}

template <size_t index, typename... Args>
get_type_t<index, Args...>&& get(Tuple<Args...>&& tuple) {
    if constexpr (index == 0)
        return std::move(tuple.head);
    else
        return get<index - 1>(std::move(tuple.tail));
}

template <size_t index, typename... Args>
const get_type_t<index, Args...>&& get(const Tuple<Args...>&& tuple) {
    if constexpr (index == 0)
        return std::move(tuple.head);
    else
        return get<index - 1>(std::move(tuple.tail));   
}

//----------------------------------------------------------------
// Get index by type
//----------------------------------------------------------------

template <typename type, typename... Args>
struct no_more : std::true_type {};

template <typename type, typename T, typename... Args>
struct no_more<type, T, Args...> {
    constexpr static bool value = !std::is_same_v<type, T> && no_more<type, Args...>::value;
};

template <typename type, typename... Args>
constexpr bool no_more_v = no_more<type, Args...>::value;

template <size_t index, typename type, typename T, typename... Args>
constexpr size_t get_index() {
    static_assert(!(std::is_same_v<type, T> && !no_more_v<type, Args...>),
        "There must be only one element of this type at the tuple.");
    if constexpr (std::is_same_v<type, T> && no_more_v<type, Args...>)
        return index;
    else
        return get_index<index + 1, type, Args...>();
}

template <typename type, typename... Args>
constexpr size_t get_index_v = get_index<0, type, Args...>();

//----------------------------------------------------------------
// Get by type
//----------------------------------------------------------------

template <typename type, typename... Args>
auto& get(Tuple<Args...>& tuple) {
    return get<get_index_v<type, Args...>>(tuple);
}

template <typename type, typename... Args>
const auto& get(const Tuple<Args...>& tuple) {
    return get<get_index_v<type, Args...>>(tuple);
}

template <typename type, typename... Args>
type&& get(Tuple<Args...>&& tuple) {
    return get<get_index_v<type, Args...>>(std::move(tuple));
}

template <typename type, typename... Args>
const type&& get(const Tuple<Args...>&& tuple) {
    return get<get_index_v<type, Args...>>(std::move(tuple));
}

//----------------------------------------------------------------
// Comparation
//----------------------------------------------------------------

template <size_t index, typename... Args>
bool are_equal(const Tuple<Args...>& lhs, const Tuple<Args...>& rhs) {
    if constexpr (index == 0)
        return get<0>(lhs) == get<0>(rhs);
    else
        return (get<index>(lhs) == get<index>(rhs)) && are_equal<index - 1>(lhs, rhs);
}

template <typename... Args>
bool operator==(const Tuple<Args...>& lhs, const Tuple<Args...>& rhs) {
    return are_equal<sizeof...(Args) - 1>(lhs, rhs);
}

template <typename... Args>
bool operator!=(const Tuple<Args...>& lhs, const Tuple<Args...>& rhs) {
    return !(lhs == rhs);
}

template <size_t index, typename... Args>
bool is_less(const Tuple<Args...>& lhs, const Tuple<Args...>& rhs) {
    if constexpr (index < sizeof...(Args))
        return (get<index>(lhs) < get<index>(rhs)) || ((get<index>(lhs) == get<index>(rhs)) && is_less<index + 1>(lhs, rhs));
    else
        return false;
}

template <typename... Args>
bool operator<(const Tuple<Args...>& lhs, const Tuple<Args...>& rhs) {
    return is_less<0>(lhs, rhs);
}

template <typename... Args>
bool operator>(const Tuple<Args...>& lhs, const Tuple<Args...>& rhs) {
    return rhs < lhs;
}

template <typename... Args>
bool operator<=(const Tuple<Args...>& lhs, const Tuple<Args...>& rhs) {
    return (lhs < rhs) || (lhs == rhs);
}

template <typename... Args>
bool operator>=(const Tuple<Args...>& lhs, const Tuple<Args...>& rhs) {
    return (lhs > rhs) || (lhs == rhs);
}


//----------------------------------------------------------------
// Make_tuple
//----------------------------------------------------------------

template <typename T>
struct unref {
    using type = T;
};

template <typename T>
struct unref<std::reference_wrapper<T>> {
    using type = T&;
};

template <typename T>
using ud_t = typename unref<typename std::decay_t<T>>::type;

template <typename... Args>
Tuple<ud_t<Args>...> makeTuple(Args&&... args) {
    return Tuple<ud_t<Args>...>(std::forward<Args>(args)...);
}

//----------------------------------------------------------------
// Make_tuple
//----------------------------------------------------------------

template <typename First, typename Second, typename... Tuples>
auto tupleCat(First&& first, Second&& second, Tuples&&... tuples) {
    return tupleCat(_tupleCat(std::forward<First>(first), std::forward<Second>(second)), std::forward<Tuples>(tuples)...);
}

template <typename T>
auto tupleCat(T&& arg) {
    return arg;
}

template <typename... LArgs, typename... RArgs>
auto _tupleCat(Tuple<LArgs...>& lhs, Tuple<RArgs...>& rhs) {
    return Tuple<LArgs..., RArgs...>(lhs, rhs);
}

template <typename... LArgs, typename... RArgs>
auto _tupleCat(Tuple<LArgs...>&& lhs, Tuple<RArgs...>& rhs) {
    return Tuple<LArgs..., RArgs...>(std::move(lhs), rhs);
}

template <typename... LArgs, typename... RArgs>
auto _tupleCat(Tuple<LArgs...>& lhs, Tuple<RArgs...>&& rhs) {
    return Tuple<LArgs..., RArgs...>(lhs, std::move(rhs));
}

template <typename... LArgs, typename... RArgs>
auto _tupleCat(Tuple<LArgs...>&& lhs, Tuple<RArgs...>&& rhs) {
    return Tuple<LArgs..., RArgs...>(std::move(lhs), std::move(rhs));
}
