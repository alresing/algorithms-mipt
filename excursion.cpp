#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>


const size_t INF = 1073741824;


size_t bfs(const std::vector<std::vector<size_t>>& graph, size_t s) {
    std::vector<size_t> dist(graph.size(), INF);
    std::vector<size_t> p(graph.size(), -1);
    dist[s] = 0;
    std::deque<int> q;
    q.push_back(s);
    size_t res = INF;
    while (!q.empty()) {
        auto v = q.front();
        q.pop_front();
        for (const auto& to : graph[v]) {
            if (dist[to] == INF) {
                dist[to] = 1 + dist[v];
                p[to] = v;
                q.push_back(to);
            } else if (to != p[v]) {
                res = std::min(res, dist[to] + dist[v] + 1);
            }
        }
        if (res != INF)
            break;
    }
    return res;
}


int main() {
    size_t start_n, m, n; std::cin >> start_n >> m;
    n = start_n;
    std::vector<std::vector<size_t>> graph(n);
    for (size_t i = 0; i < m; ++i) {
        size_t s, t, w;
        std::cin >> s >> t >> w;
        size_t from = s-1;
        size_t to = (w == 1) ? t-1 : n++;
        for (size_t j = 0; j < w; ++j) {
            if (j > 0) {
                from = to;
                to = (j == (w-1)) ? t-1 : n++;
            }
            if (to >= graph.size())
                graph.emplace_back();
            graph[from].push_back(to);
            graph[to].push_back(from);
        }
    }

    size_t res = INF;
    for (size_t s = 0; s < start_n; ++s)
         res = std::min(res, bfs(graph, s));

    std::cout << res << std::endl;
}
