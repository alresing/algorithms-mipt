#include <iostream>
#include <vector>
#include <algorithm>


enum Color {
    WHITE,
    GRAY,
    BLACK
};


class CycledGraph {
public:
    CycledGraph(std::vector<std::vector<int>>& graph, int n) : graph(graph), n(n) {
        std::vector<Color> color(n, WHITE);
        for (int i = 0; i < n; ++i) {
            if (color[i] == WHITE) {
                cycleVertex = dfs(i, color);
                if (cycleVertex != -1) {
                    cycled = true;
                    break;
                }
            }
        }
    }

    bool isCycled() {
        return cycled;
    }

private:
    std::vector<std::vector<int>> graph;
    int n = 0;
    bool cycled = false; 
    int cycleVertex = -1;

    int dfs(int v, std::vector<Color>& color) {
        color[v] = GRAY;
        for (int to : graph[v]) {
            if (color[to] == GRAY)
                return to;
            else if (color[to] == WHITE) {
                int vertex = dfs(to, color);
                if (vertex != -1)
                    return vertex;
            }
        }
        color[v] = BLACK;
        return -1;
    }
};


class Graph {
public:
    Graph(std::vector<std::vector<int>>& graph, int n) : graph(graph), n(n) {
        std::vector<Color> color(n, WHITE);
        int timer = 0;
        for (int i = 0; i < n; ++i) {
            if (color[i] == WHITE) {
                dfs(i, color, timer);
            }
        }
    }

    std::vector<int> getSort() {
        std::sort(tout.begin(), tout.end());
        std::vector<int> answer;
        for (int i = n-1; i > -1; --i)
            answer.push_back(tout[i].second);
        return answer;
    }

private:
    std::vector<std::vector<int>> graph;
    int n = 0;
    std::vector<std::pair<int, int>> tout;  // (tout, v)

    void dfs(int v, std::vector<Color>& color, int& timer) {
        color[v] = GRAY;
        for (int to : graph[v]) {
            if (color[to] == WHITE)
                dfs(to, color, timer);
        }
        tout.push_back({timer++, v});
        color[v] = BLACK;
    }
};


int main() {
    int n, m; std::cin >> n >> m;
    std::vector<std::vector<int>> vertices;
    for (int i = 0; i < n; ++i) {
        std::vector<int> temp;
        vertices.push_back(temp);
    }

    for (int i = 0; i < m; ++i) {
        int a, b; std::cin >> a >> b;
        vertices[a-1].push_back(b-1);
    }

    CycledGraph cycledGraph(vertices, n);
    if (cycledGraph.isCycled()) {
        std::cout << -1 << std::endl;
    } else {
        Graph graph(vertices, n);
        std::vector<int> answer = graph.getSort();
        for (int i : answer)
            std::cout << i+1 << " ";
        std::cout << std::endl;
    }
}
