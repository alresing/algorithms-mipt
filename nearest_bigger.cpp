#include <iostream>
#include <vector>
#include <stdio.h>

#define N 262144

struct Node {
    Node(int64_t a, uint64_t b) : max(a), size(b) {}

    int64_t max = 0;
    uint64_t size = 1;
};

class SegmentTree {
public:
    SegmentTree(std::vector<int64_t>& numbers) {
        for (uint64_t i = 0; i < 2*N; ++i) t.push_back(Node(-1, 1));
        build(numbers, 0, 0, N);
    }

    void set(uint64_t i, int64_t x) {
        set(0, i, x);
    }

    int64_t get(uint64_t i, int64_t x) {
        std::vector<int64_t> indexs = getIndexs(0, i);           // индексы фундаментальных отрезков [i, t.size()-1]
        for (uint64_t j = 0; j < indexs.size(); ++j) {
            if (t[indexs[j]].max >= x) {
                return (getLeftMoreX(indexs[j], x) - (N-1));
            }
        }
        return -1;
    }

private:
    void build(std::vector<int64_t>& numbers, uint64_t i, uint64_t tl, uint64_t tr) {
        if (tr - tl == 1) {
            if (tl < numbers.size()) {
                t[i] = Node(numbers[tl], 1);
            } else {
                t[i] = Node(-1, 1);
            }
        } else {
            int64_t tm = (tl + tr)/2;
            build(numbers, 2*i+1, tl, tm);
            build(numbers, 2*i+2, tm, tr);
            t[i].max = std::max(t[2*i+1].max, t[2*i+2].max);
            t[i].size = t[2*i+1].size + t[2*i+2].size;
        }
    }

    void set(uint64_t i, uint64_t index, int64_t x) {
        if ((2*i+2) < t.size()) {
            if (t[2*i+1].size > index) {
                set(2*i+1, index, x);
                t[i].max = std::max(t[2*i+1].max, t[2*i+2].max);
            } else {
                set(2*i+2, index - t[2*i+1].size, x);
                t[i].max = std::max(t[2*i+1].max, t[2*i+2].max);
            }
        } else {
            t[i].max = x;
        }
    }

    std::vector<int64_t> getIndexs(uint64_t i, uint64_t index) {
        std::vector<int64_t> ans;
        if (i >= t.size()) return ans;
        if (index == 0) {
            ans.push_back(i);
            return ans;
        }
        if (index >= (t[i].size/2)) return getIndexs(2*i+2, index-(t[i].size/2));
        
        std::vector<int64_t> left = getIndexs(2*i+1, index);
        for (const int64_t& j : left) ans.push_back(j);
        std::vector<int64_t> right = getIndexs(2*i+2, 0);
        for (const int64_t& j : right) ans.push_back(j);
        return ans;
    }

    int64_t getLeftMoreX(int64_t i, int64_t x) {
        if (t[i].size == 1) return i;
        if (t[2*i+1].max >= x) return getLeftMoreX(2*i+1, x);
        return getLeftMoreX(2*i+2, x);
    }

    std::vector<Node> t;
};

int main() {
    int64_t n, m;
    scanf("%lld%lld", &n, &m);
    std::vector<int64_t> v;
    for (int64_t i = 0; i < n; ++i) {
        int64_t p; scanf("%lld", &p);
        v.push_back(p);
    }

    SegmentTree tree(v);

    for (int64_t i = 0; i < m; ++i) {
        int64_t type, index, x;
        scanf("%lld%lld%lld", &type, &index, &x);
        if (type == 0) {
            tree.set(index-1,x);
        } else {
            int64_t ans = tree.get(index-1, x);
            printf("%lld\n", (ans == -1) ? -1 : (ans+1));
        }
    }
}