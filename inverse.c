/*
    Для заданного массива найти количество инверсий в нем.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int64_t merge(int a[], int l, int m, int r) {
    int64_t res = 0;
    int n1 = m-l+1;
    int n2 = r-m;

    int* L = malloc(n1 * sizeof(int));
    int* R = malloc(n2 * sizeof(int));

    for (int i = 0; i < n1; i++) {
        L[i] = a[l+i];
    }
    for (int j = 0; j < n2; j++) {
        R[j] = a[m+1+j];
    }

    int i = 0;
    int j = 0;
    int t = l;
    while (i < n1 && j < n2) {      // пока не дошли до конца какого-либо из массивов
        if (L[i] <= R[j]) {         // если элемент из L меньше,
            a[t] = L[i];            // то добавляем его в конец нашего итогового массива
            i += 1;                 // и увеличиваем на 1 индекс в массиве L
        } else {                    // иначе
            a[t] = R[j];            // добавляем элемент из R (т.к. он меньше)
                                    // элемент из R < чем из L, значит это инверсия, т.к. дальше все элементы L
            res += n1-i;            // не меньше этого, то с ними тоже получаются инверсии
            j += 1;                 // и увеличиваем на 1 индекс в массиве R
        }
        t += 1;
    }

    while (i < n1) {
        a[t] = L[i];
        i += 1;
        t += 1;
    }

    while (i < n2) {
        a[t] = R[j];
        j += 1;
        t += 1;
    }

    free(L);
    free(R);

    return res;
}

int64_t mergeSortWithInverseCounting(int a[], int l, int r) {
    int64_t res = 0;

    if (l<r) {
        int m = l+(r-l)/2;
        res += mergeSortWithInverseCounting(a, l, m);
        res += mergeSortWithInverseCounting(a, m+1, r);

        res += merge(a, l, m, r);
    }

    return res;
}

int main() {
    FILE* fin;
    FILE* fout;
    fin = fopen("./inverse.in", "r");
    fout = fopen("./inverse.out", "w");

    int n; fscanf(fin, "%d", &n);
    int* a = malloc(n * sizeof(int));

    for (int i = 0; i < n; i++) {
        fscanf(fin, "%d", &a[i]);
    }

    fprintf(fout, "%d", mergeSortWithInverseCounting(a, 0, n-1));

    free(a);

    return 0;
}