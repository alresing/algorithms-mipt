#include <iostream>
#include <vector>


class Graph {
public:
    Graph(size_t n) : graph(n) {}

    std::vector<size_t>& get(size_t v) {
        return graph[v];
    }

    const std::vector<size_t>& get(size_t v) const {
        return graph[v];
    }

private:
    std::vector<std::vector<size_t>> graph;
};


void dfs(const Graph& graph, std::vector<bool>& used, size_t v) {
    if (used[v])
        return;
    used[v] = true;
    for (const auto& u: graph.get(v))
        dfs(graph, used, u);
}


std::pair<std::vector<size_t>, std::vector<size_t>> getMinimumSet (
        size_t m, size_t n, std::vector<std::vector<size_t>> left_pre,
        std::vector<size_t> right, std::vector<bool> free) {
    Graph graph(m+n);
    for (size_t i = 0; i < m; ++i)
        for (size_t j = 0; j < left_pre[i].size(); ++j)
            if (right[left_pre[i][j]] != i)
                graph.get(i).push_back(left_pre[i][j]+m);
    for (size_t i = m; i < m+n; ++i)
        graph.get(i).push_back(right[i-m]);
    
    std::vector<bool> used(n+m);
    for (size_t i = 0; i < m; ++i)
        if (free[i])
            dfs(graph, used, i);
    std::vector<size_t> VCleft;
    std::vector<size_t> VCright;
    for (size_t i = 0; i < m; ++i)
        if (!used[i])
            VCleft.push_back(i+1);
    for (size_t i = m; i < m+n; ++i)
        if (used[i])
            VCright.push_back(i - m  + 1);

    return {VCleft, VCright};
 }


int main() {
    size_t m, n;
    std::cin >> m >> n;

    std::vector<std::vector<size_t>> left_pre(m);
    std::vector<size_t> right(n);
    std::vector<bool> free(m, true);


    for (size_t i = 0; i < m; ++i) {
        size_t k = 0;
        std::cin >> k;
        for (size_t j = 0; j < k; ++j) {
            size_t r;
            std::cin >> r;
            left_pre[i].push_back(--r);
        }
    }

    for (size_t i = 0; i < m; ++i) {
        size_t r;
        std::cin >> r;
        if (r == 0)
            continue;
        --r;
        right[r] = i;
        free[i] = false;
    }

    auto [VCleft, VCright] = getMinimumSet(m, n, left_pre, right, free);

    std::cout << VCright.size() + VCleft.size() << std::endl;
    std::cout << VCleft.size() << " ";
    for (const auto& v : VCleft)
        std::cout << v << " ";
    std::cout << std::endl;
    std::cout << VCright.size() << " ";
    for (const auto& v : VCright)
        std::cout << v << " ";
    std::cout << std::endl;
}
