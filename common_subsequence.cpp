#include <iostream>
#include <string>
#include <vector>

int main() {
    std::string a, b;
    std::cin >> a >> b;

    std::vector<int> now, last;
    for (uint64_t j = 0; j < (b.size()+1); ++j) {
        now.push_back(0);
        last.push_back(0);
    }

    std::vector<std::vector<bool>> isUp;
    for (uint64_t i = 0; i < (a.size()+1); ++i) {
        std::vector<bool> temp(b.size()+1, true);
        isUp.push_back(temp);
    }
    std::vector<std::vector<bool>> isLeft;
    for (uint64_t i = 0; i < (a.size()+1); ++i) {
        std::vector<bool> temp(b.size()+1, true);
        isLeft.push_back(temp);
    }

    for (uint64_t aIndex = 1; aIndex <= a.size(); ++aIndex) {
        for (uint64_t bIndex = 1; bIndex <= b.size(); ++bIndex) {
            if (last[bIndex] > now[bIndex-1]) {
                now[bIndex] = last[bIndex];
                isUp[aIndex][bIndex] = true;
                isLeft[aIndex][bIndex] = false;
            } else {
                now[bIndex] = now[bIndex-1];
                isUp[aIndex][bIndex] = false;
                isLeft[aIndex][bIndex] = true;
            }
                
            if (a[aIndex-1] == b[bIndex-1]) {
                if (now[bIndex] <= (1 + last[bIndex-1])) {
                    now[bIndex] = last[bIndex-1] + 1;
                    isUp[aIndex][bIndex] = true;
                    isLeft[aIndex][bIndex] = true;
                }
            }
        }
        last = now;
    }
    
    int i = a.size();
    int j = b.size();
    std::string common = "";
    while ((i > 0) && (j > 0)) {
        if ((isUp[i][j] == true) && (isLeft[i][j] == true))

            common += a[i-1];
        int new_i = isUp[i][j] ? (i-1) : i;
        int new_j = isLeft[i][j] ? (j-1) : j;
        i = new_i;
        j = new_j;
    }

    std::string res = "";
    for (int i = common.size()-1; i >= 0; --i) {
        res += common[i];
    }

    std::cout << res << std::endl;
    
}