#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>


const int64_t neg_inf = std::numeric_limits<int64_t>::lowest();


std::vector<int64_t> dp(int64_t n, const std::vector<int64_t>& a) {
    std::vector<int64_t> dp(n, neg_inf);
    std::vector<int64_t> posBest(n, neg_inf);
    std::vector<int64_t> prevIndex(n, neg_inf);

    for (int64_t index = 0; index < n; ++index) {
        int64_t i = static_cast<int64_t>(std::lower_bound(dp.begin(), dp.end(), a[index], std::greater_equal<int64_t>()) - dp.begin()) ;
        if ((dp[i-1] >= a[index]) and (a[index] >= dp[i])) {
            dp[i] = a[index];
            posBest[i] = index;
            prevIndex[index] = (i > 0) ? posBest[i-1] : neg_inf;
        }
    }

    std::vector<int64_t> res;
    int64_t k = posBest.size()-1;
    while ((k > -1) && (posBest[k] == neg_inf)) --k;
    int64_t index = posBest[k];
    while (index > -1) {
        res.push_back(index+1);
        index = prevIndex[index];
    }
    std::reverse(res.begin(), res.end());

    return res;
}


int main() {
    int64_t n; std::cin >> n;
    std::vector<int64_t> a;
    for (int64_t i = 0; i < n; ++i) {
        int64_t p; std::cin >> p;
        a.push_back(p);
    }

    std::vector<int64_t> res = dp(n, a);

    std::cout << res.size() << std::endl;
    for (const auto& i : res) std::cout << i << " ";
    std::cout << std::endl;
}
