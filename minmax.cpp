#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

int binSearch(int A[], int B[], int len) {
    int left = -1;
    int right = len-1;

    while (right - left > 1) {
        int m = (right + left)/2;
        if (A[m] >= B[m]) {
            right = m;
        } else {
            left = m;
        }
    }

    return right;
}

int foo(int A[], int B[], int len) {
    if (A[len-1] < B[len-1]) return len-1;

    int i = binSearch(A, B, len);

    if (i == 0) return 0;

    if (std::max(A[i], B[i]) <= std::max(A[i-1], B[i-1])) return i;
    return i-1;
}

int main() {
    int n, m, l;
    scanf("%d%d%d", &n, &m, &l);
    
    int **A = (int **)malloc(n*sizeof(int *));
    for (int i = 0; i < n; i++) {
        A[i] = (int *)malloc(l*sizeof(int));
        for (int j = 0; j < l; j++) {
            scanf("%d", &A[i][j]);
        }
    }

    int **B = (int **)malloc(m*sizeof(int *));
    for (int i = 0; i < m; i++) {
        B[i] = (int *)malloc(l*sizeof(int));
        for (int j = 0; j < l; j++) {
            scanf("%d", &B[i][j]);
        }
    }

    int q; scanf("%d", &q);
    for (int useless = 0; useless < q; useless++) {
        int i, j; scanf("%d%d", &i, &j);

        printf("%d\n", foo(A[i-1], B[j-1], l) + 1);
    }

    for(int i = 0; i < n; i++) {
        free(A[i]);
    }
    free(A);

    for(int i = 0; i < m; i++) {
        free(B[i]);
    }
    free(B);

    
    return 0;
}