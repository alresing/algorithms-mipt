#include <iostream>
#include <vector>
#include <string>

class HashTable {
public:
    HashTable() {
        for (int i = 0; i < M; ++i) {
            std::vector<std::pair<std::string,int>> p;
            v.push_back(p);
        }
    }

    int addAndReturn(const std::string& s, int i) {
        int hashFromS = hash(s);
        for (uint64_t j = 0; j < v[hashFromS].size(); ++j) {
            if (v[hashFromS][j].first == s) {
                v[hashFromS][j].second += i;
                return v[hashFromS][j].second;
            }
        }
        v[hashFromS].push_back({s, i});
        return i;
    }

private:
    int hash(const std::string& s) {
        int res = 0;
        for (uint64_t i = 0; i < s.size(); ++i) {
            res *= 10;
            res += s[i] - '0';
            res %= M;
        }
        return res;
    }
    
    const int M = 1'000'003;
    std::vector<std::vector<std::pair<std::string,int>>> v;
};



int main() {
    HashTable table;

    std::string s;
    while (std::cin >> s) {
        int i; std::cin >> i;
        std::cout << table.addAndReturn(s, i) << std::endl;
    }
}