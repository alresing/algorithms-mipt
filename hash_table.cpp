#include <fstream>
#include <string>
#include <vector>

class HashTable {
public:
    HashTable() {
        for (int i = 0; i < M; ++i) {
            std::vector<std::pair<std::string, std::string>> p;
            v.push_back(p);
        }
    }

    void put(const std::string& x, const std::string& y) {
        int hashFromX = hash(x);
        for (uint64_t i = 0; i < v[hashFromX].size(); ++i) {
            if (v[hashFromX][i].first == x) {
                v[hashFromX][i].second = y;
                return;
            }
        }
        v[hashFromX].push_back({x, y});
    }

    void del(const std::string& x) {
        int hashFromX = hash(x);
        for (uint64_t i = 0; i < v[hashFromX].size(); ++i) {
            if (v[hashFromX][i].first == x) {
                v[hashFromX][i].second = "none";
                return;
            }
        }
    }

    std::string get(const std::string& x) {
        int hashFromX = hash(x);
        for (uint64_t i = 0; i < v[hashFromX].size(); ++i) {
            if (v[hashFromX][i].first == x) {
                return v[hashFromX][i].second;
            }
        }
        return "none";
    }

private:
    int hash(const std::string& x) {
        int res = 0;
        for (uint64_t i = 0; i < x.size(); ++i) {
            res *= 10;
            res += x[i] - '0';
            res %= M;
        }
        return res;
    }
    
    const int M = 1'000'003;
    std::vector<std::vector<std::pair<std::string, std::string>>> v;
};

int main() {
    HashTable table;

    std::ifstream inf("map.in");
    std::ofstream outf("map.out");

    std::string s;
    while (inf >> s) {
        if (s == "put") {
            std::string x, y; inf >> x >> y;
            table.put(x, y);
        } else if (s == "delete") {
            std::string x; inf >> x;
            table.del(x);
        } else { // get
            std::string x; inf >> x;
            outf << table.get(x) << std::endl;
        }
    }
}