#include <iostream>
#include <string>

struct Node {
    int val = 0;
    int height = 1;
    Node* left = nullptr;
    Node* right = nullptr;
};

// left: <= ; right: >
class AVLTree {
public:
    ~AVLTree() {
        while (size > 0) del(head->val);
    }

    void add(int x) {
        Node* node = new Node;
        node->val = x;
        if (size == 0) head = node;
        else           _add(head, node);
        ++size;
    }

    void del(int x) {
        if (size == 0) return;
        if (size == 1 && head->val == x) {
            delete head;
            head = nullptr;
            --size;
        } else {
            _del(head, x);
        }
    }

    bool exists(int x) const {
        if (size == 0) return false;
        Node* vertex = head;
        while (true) {
            if (vertex == nullptr) return false;
            if (vertex->val == x)  return true;
            if (vertex->val < x)   vertex = vertex->right;
            else                   vertex = vertex->left;
        }
    }

    Node* next(int x) const {
        return _next(head, x);
    }

    Node* prev(int x) const {
        return _prev(head, x);
    }

    int getSize() const {
        return size;
    }

    void print() const {
        std::cout << "size: " <<  size << std::endl;
        if (size > 0) _print(head);
    }

private:
    Node* _next(Node* vertex, int x) const {
        if (vertex->val <= x && vertex->right != nullptr) return _next(vertex->right, x);
        if (vertex->val <= x && vertex->right == nullptr) return nullptr;
        if (vertex->val > x) {
            if (vertex->left == nullptr) return vertex;
            Node* fromLeft = _next(vertex->left, x);
            if (fromLeft == nullptr) return vertex;
            return fromLeft;
        }
    }

    Node* _prev(Node* vertex, int x) const {
        if (vertex->val >= x && vertex->left != nullptr) return _prev(vertex->left, x);
        if (vertex->val >= x && vertex->left == nullptr) return nullptr;
        if (vertex->val < x) {
            if (vertex->right == nullptr) return vertex;
            Node* fromRight = _prev(vertex->right, x);
            if (fromRight == nullptr) return vertex;
            return fromRight;
        }
    }

    void _print(Node* vertex) const {
        std::cout << vertex->val << " h: " << vertex->height << " s: ";
        std::cout << (vertex->left == nullptr ? -1 : vertex->left->val) << " ";
        std::cout << (vertex->right == nullptr ? -1 : vertex->right->val) << " ";
        std::cout << std::endl;
        if (vertex->left != nullptr) _print(vertex->left);
        if (vertex->right != nullptr) _print(vertex->right);
    }

    void balance(Node* vertex) {
        int leftHeight  = (vertex->left != nullptr)  ? vertex->left->height  : 0;
        int rightHeight = (vertex->right != nullptr) ? vertex->right->height : 0;

        vertex->height = 1 + std::max(leftHeight, rightHeight);


        if (leftHeight - rightHeight == -2) {
            int bLeftHeight  = (vertex->right->left != nullptr)  ? vertex->right->left->height  : 0;
            int bRightHeight = (vertex->right->right != nullptr) ? vertex->right->right->height : 0;
            if (bLeftHeight - bRightHeight < 1) rotateLeft(vertex);
            else                                bigRotateLeft(vertex);
        } else if (leftHeight - rightHeight == 2) {
            int bLeftHeight  = (vertex->left->left != nullptr)  ? vertex->left->left->height  : 0;
            int bRightHeight = (vertex->left->right != nullptr) ? vertex->left->right->height : 0;
            if (bLeftHeight - bRightHeight > -1) rotateRight(vertex);
            else                                 bigRotateRight(vertex);
        }
    }

    void _add(Node* vertex, Node* node) {
        if      (vertex->val >= node->val && vertex->left != nullptr) _add(vertex->left, node);
        else if (vertex->val < node->val && vertex->right != nullptr) _add(vertex->right, node);
        else if (vertex->val >= node->val && vertex->left == nullptr) vertex->left = node;
        else if (vertex->val < node->val && vertex->right == nullptr) vertex->right = node;

        balance(vertex);
    }

    int delMostRight(Node* vertex) {
        if (vertex->right->right == nullptr) {
            int res = vertex->right->val;
            Node* rightLeft = vertex->right->left;
            delete vertex->right;
            vertex->right = rightLeft;
            balance(vertex);
            return res;
        }
        int res = delMostRight(vertex->right);
        balance(vertex);
        return res;
    }

    bool _del(Node* vertex, int x) {
        bool is_left  = false;
        bool is_right = false;
        if      (vertex->val > x && vertex->left  != nullptr) is_left  = _del(vertex->left, x);
        else if (vertex->val < x && vertex->right != nullptr) is_right = _del(vertex->right, x);
        else if (vertex->val > x && vertex->left  == nullptr) return false;
        else if (vertex->val < x && vertex->right == nullptr) return false;
        else if (vertex->val == x) {
            --size;
            if (vertex->left == nullptr && vertex->right == nullptr) {
                return true;
            } else if (vertex->left == nullptr) {
                Node* right = vertex->right;
                vertex->val = right->val;
                vertex->height = right->height;
                vertex->left = right->left;
                vertex->right = right->right;
                delete right;
            } else if (vertex->right == nullptr) {
                Node* left = vertex->left;
                vertex->val = left->val;
                vertex->height = left->height;
                vertex->right = left->right;
                vertex->left = left->left;
                delete left;
            } else if (vertex->left->right != nullptr) {
                vertex->val = delMostRight(vertex->left);
            } else if (vertex->left->right == nullptr) {
                Node* left = vertex->left;
                vertex->val = left->val;
                vertex->left = left->left;
                delete left;
            }
        }

        if (is_left) {
            delete vertex->left;
            vertex->left = nullptr;
        }
        if (is_right) {
            delete vertex->right;
            vertex->right = nullptr;
        }

        balance(vertex);

        return false;
    }

    void rotateLeft(Node* vertex) {
        Node* a = vertex->left;
        Node* b = vertex->right->left;
        Node* c = vertex->right->right;
        std::swap(vertex->val, vertex->right->val);
        vertex->left = vertex->right;
        vertex->left->left = a;
        vertex->left->right = b;
        vertex->right = c;
        if (vertex->left->right == nullptr && vertex->left->left == nullptr) {
            vertex->left->height = 1;
        } else if (vertex->left->right == nullptr) {
            vertex->left->height = 1 + vertex->left->left->height;
        } else if (vertex->left->left == nullptr) {
            vertex->left->height = 1 + vertex->left->right->height;
        } else {
            vertex->left->height = 1 + std::max(vertex->left->right->height, vertex->left->left->height);
        }
        if (vertex->right == nullptr) {
            vertex->height = 1 + vertex->left->height;
        } else {
            vertex->height = 1 + std::max(vertex->right->height, vertex->left->height);
        }
    }

    void rotateRight(Node* vertex) {
        Node* a = vertex->left->left;
        Node* b = vertex->left->right;
        Node* c = vertex->right;
        std::swap(vertex->val, vertex->left->val);
        vertex->right = vertex->left;
        vertex->right->right = c;
        vertex->right->left = b;
        vertex->left = a;
        if (vertex->right->right == nullptr && vertex->right->left == nullptr) {
            vertex->right->height = 1;
        } else if (vertex->right->right == nullptr) {
            vertex->right->height = 1 + vertex->right->left->height;
        } else if (vertex->right->left == nullptr) {
            vertex->right->height = 1 + vertex->right->right->height;
        } else {
            vertex->right->height = 1 + std::max(vertex->right->right->height, vertex->right->left->height);
        }
        if (vertex->left == nullptr) {
            vertex->height = 1 + vertex->right->height;
        } else {
            vertex->height = 1 + std::max(vertex->right->height, vertex->left->height);
        }
    }

    void bigRotateLeft(Node* vertex) {
        rotateRight(vertex->right);
        rotateLeft(vertex);
    }

    void bigRotateRight(Node* vertex) {
        rotateLeft(vertex->left);
        rotateRight(vertex);
    }

    Node* head = nullptr;
    int size = 0;
};

int main() {
    AVLTree tree;

    std::string s;
    while (std::cin >> s) {
        if (s == "insert") {
            int x; std::cin >> x;
            if (! tree.exists(x)) tree.add(x);
        } else if (s == "delete") {
            int x; std::cin >> x;
            tree.del(x);
        } else if (s == "exists") {
            int x; std::cin >> x;
            std::cout << (tree.exists(x) ? "true" : "false") << std::endl;
        } else if (s == "next") {
            int x; std::cin >> x;
            if (tree.getSize() == 0 || tree.next(x) == nullptr) {
                std::cout << "none" << std::endl;
            } else {
                std::cout << tree.next(x)->val << std::endl;
            }
        } else if (s == "prev") {
            int x; std::cin >> x;
            if (tree.getSize() == 0 || tree.prev(x) == nullptr) {
                std::cout << "none" << std::endl;
            } else {
                std::cout << tree.prev(x)->val << std::endl;
            }
        }
    }

    return 0;
}