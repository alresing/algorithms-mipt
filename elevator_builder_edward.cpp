#include <iostream>
#include <vector>
#include <queue>


const int64_t INF = 1152921504606846976;


std::vector<int64_t> dijkstra(std::vector<std::vector<std::pair<int64_t, int64_t>>>& graph, int64_t s) {
    std::vector<int64_t> dist(graph.size(), INF);
    std::vector<bool> used(graph.size(), false);
    std::priority_queue<std::pair<int64_t, int64_t>> q;
    dist[s] = 0;
    q.push({-dist[s], s});
    while (!q.empty()) {
        int64_t v = q.top().second;
        q.pop();
        if (dist[v] == INF)
            break;
        if (used[v])
            continue;
        used[v] = true;
        for (const auto& pair: graph[v]) {
            int64_t to = pair.first;
            dist[to] = std::min(dist[to], dist[v] + pair.second);
            q.push({-dist[to], to});
        }
    }

    return dist;
}


int main() {
    int64_t h, a, b, c;
    std::cin >> h >> a >> b >> c;
    --h;
    std::vector<std::vector<std::pair<int64_t, int64_t>>> graph(a);

    for (int64_t v = 0; v < a; ++v) {
        graph[v].push_back({(v + b) % a, b});
        graph[v].push_back({(v + c) % a, c});
    }
    
    std::vector<int64_t> dist = dijkstra(graph, 0);

    int64_t result = 0;

    for (int64_t i = 0; i < a; ++i) {
        if (dist[i] > h)
            continue;
        result += ((h - dist[i]) / a) + 1;
    }

    std::cout << result << std::endl;
}
