#include <iostream>
#include <vector>
#include <string>
#include <algorithm>


bool augment(const std::vector<std::vector<int64_t>>& g, std::vector<bool>& used,
        std::vector<int64_t>& match, int64_t v) {
    if (used[v])
        return false;
    used[v] = true;
    for (int64_t to: g[v]) {
        if ((match[to] == -1) || augment(g, used, match, match[to])) {
            match[to] = v;
            return true;
        }
    }
    return false;
}


int64_t kun(const std::vector<std::vector<int64_t>>& g, int64_t n) {
    std::vector<bool> used(n, false);
    std::vector<int64_t> match(n, -1);
    for (int64_t v = 0; v < n; ++v) {
        used.assign(n, false);
        augment(g, used, match, v);
    }

    int64_t res = 0;
    for (int64_t i = 0; i < n; ++i)
        if (match[i] != -1)
            ++res;

    return res;
}


int main() {
    int64_t n, m, a, b;
    std::cin >> n >> m >> a >> b;
    std::vector<std::vector<char>> map(n, std::vector<char>(m));
    for (int64_t i = 0; i < n; ++i) {
        std::string s;
        std::cin >> s;
        for (int64_t j = 0; j < m; ++j)
            map[i][j] = s[j];
    }

    int64_t count = 0;
    std::vector<std::vector<int64_t>> g(n*m);
    for (int64_t i = 0; i < n; ++i)
        for (int64_t j = 0; j < m; ++j)
            if (map[i][j] == '*') {
                ++count;
                if (((i+j) % 2) == 0) {
                    if ((i > 0) && (map[i-1][j] == '*'))
                        g[i*m + j].push_back((i-1)*m + j);
                    if ((j > 0) && (map[i][j-1] == '*'))
                        g[i*m + j].push_back(i*m + j-1);
                    if ((i < (n-1)) && (map[i+1][j] == '*'))
                        g[i*m + j].push_back((i+1)*m + j);
                    if ((j < (m-1)) && (map[i][j+1] == '*'))
                        g[i*m + j].push_back(i*m + j+1);
                }
            }

    int64_t res = kun(g, n*m);
    if (a <= (2*b))
        res = (count - 2*res) * b + res * a;
    else
        res = count*b;
    std::cout << res << std::endl;
}
