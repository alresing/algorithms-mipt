/*
    Есть очередь и запросы:
    "+ i" -- добавить в конец
    "* i" -- добавить в середину очереди
    "-"   -- забрать из начала очереди
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct Node Node;

struct Node {
    int val;
    Node* next;
};

typedef struct Queue Queue;

struct Queue {
    int size;
    Node* front;
    Node* mid;
    Node* last;
};

void pushBack(Queue* q, int t) {
    Node* newNode = malloc(sizeof(Node));
    newNode->val = t;
    if (q->size > 0) {
        q->last->next = newNode;
        if (q->size%2 == 0) {
            q->mid = q->mid->next;
        }
    } else {
        q->front = newNode;
        q->mid = newNode;
    }
    q->last = newNode;
    q->size += 1;
}

void pushMid(Queue* q, int t) {
    Node* newNode = malloc(sizeof(Node));
    newNode->val = t;
    if (q->size > 0) {
        if (q->size > 1) {
            newNode->next = q->mid->next; 
        } else {
            q->last = newNode;
        }
        q->mid->next = newNode;
    } else {
        q->front = newNode;
        q->last = newNode;
    }

    if (q->size%2 == 0) {
        q->mid = newNode;
    }

    q->size += 1;
}

int popFront(Queue* q) {
    int t = q->front->val;
    Node* wasFirst = q->front;

    if (q->size > 1) {
        q->front = q->front->next;
        if (q->size%2 == 0) {
            q->mid = q->mid->next;
        }
    }
    
    q->size -= 1;
    free(wasFirst);

    return t;
}

int front(Queue* q) {
    return q->front->val;
}

int size(Queue* q) {
    return q->size;
}

void clear(Queue* q) {
    while (q->size > 0) {
        popFront(q);
    }
}


void show(Queue* q) {
    Node* now = q->front;
    for (int i = 0; i < q->size; i++) {
        printf("%d%s", now->val, " ");
        now = now->next;
    }
    printf("%s%d\n", "-- ", q->mid->val);
}


int main() {
    Queue q; q.size = 0;

    int n; scanf("%d", &n);
    int count = 0;
    while (count < n) {
        char c; scanf("%c", &c);
        if (c == '-') {
            printf("%d\n", popFront(&q));
            count += 1;

        } else if (c == '+') {
            int t; scanf("%d", &t);
            pushBack(&q, t);
            count += 1;
            
        } else if (c == '*') {
            int t; scanf("%d", &t);
            pushMid(&q, t);
            count += 1;
        }
    }

    clear(&q);

    return 0;
}