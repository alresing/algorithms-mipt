#include <iostream>
#include <vector>
#include <string>


bool is_zero(std::vector<int64_t>& m) {
    for (uint64_t i = 0; i < m.size(); ++i) {
        if (m[i] != 0) return false;
    }
    return true;
}


void div2(std::vector<int64_t>& m) {
    for (int64_t i = (m.size()-1); i >= 1; --i) {
        if (m[i] % 2 != 0)
            m[i-1] += 10;
        m[i] /= 2;
        if ((m[i] == 0) && (m.size() == (i+1)) && (m.size() > 1))
            m.pop_back();
    }
    m[0] /= 2;
}


std::vector<std::vector<int64_t>> mult(const std::vector<std::vector<int64_t>>& left,
                                    const std::vector<std::vector<int64_t>>& right,
                                    int64_t mod) {
    std::vector<std::vector<int64_t>> res;
    for (uint64_t i = 0; i < left.size(); ++i) {
        res.emplace_back();
        for (uint64_t j = 0; j < left.size(); ++j) {
            int64_t sum = 0;
            for (uint64_t k = 0; k < left.size(); ++k) {
                sum += left[i][k] * right[k][j];
                sum %= mod;
            }
            res[i].push_back(sum);
        }
    }
    return res;
}


int64_t bit(int64_t a, int64_t i) {
    return ((a >> i) & 1);
}


bool good(const int64_t& m1, const int64_t& m2, const int64_t& n) {
    for (int64_t i = 0; i < (n-1); ++i) {
        if ((bit(m1, i) == bit(m2,i)) && (bit(m1,i+1) == bit(m2,i+1)) && (bit(m1,i) == bit(m1,i+1)))
            return false;
    }
    return true;
}


std::vector<std::vector<int64_t>> dp(std::vector<int64_t>& m,
                                const std::vector<std::vector<int64_t>>& E,
                                std::vector<std::vector<int64_t>>& A,
                                const int64_t& mod) {
    std::vector<std::vector<int64_t>> res = E;
    while (!is_zero(m)) {
        if ((m[0] % 2) == 1) {
            res = mult(res, A, mod);
        }
        A = mult(A, A, mod);
        div2(m);
    }

    return res;
}


int main() {
    int64_t n, mod;
    std::vector<int64_t> m;
    std::string s;
    std::cin >> s >> n >> mod;
    for (int i = s.size()-1; i >=0; --i)
        m.push_back(s[i]-48);

    for (int i = 0; i < m.size(); ++i) {
        if (m[i] == 0) m[i] = 9;
        else {
            m[i] -= 1;
            if ((i = (m.size()-1)) && (m[i] == 0) && (m.size() > 1))
                m.pop_back();
            break;
        }
    }

    int64_t pow2n = 1 << n;

    std::vector<std::vector<int64_t>> correct;
    for (int64_t mask = 0; mask < pow2n; ++mask) {
        std::vector<int64_t> temp;
        for (int64_t mask2 = 0; mask2 < pow2n; ++mask2)
            temp.push_back(good(mask, mask2, n) ? 1 : 0);
        correct.push_back(temp);
    }

    std::vector<std::vector<int64_t>> E;
    for (int64_t mask = 0; mask < pow2n; ++mask) {
        std::vector<int64_t> temp(pow2n, 0);
        E.push_back(temp);
    }
    for (int64_t mask = 0; mask < pow2n; ++mask)
        E[mask][mask] = 1;

    std::vector<std::vector<int64_t>> matrix = dp(m, E, correct, mod);

    int64_t result = 0;

    for (int64_t i = 0; i < pow2n; ++i) {
        for (int64_t j = 0; j < pow2n; ++j) {
            result += matrix[i][j];
            result %= mod;
        }
    }

    std::cout << result << std::endl;
}
