#pragma once

#include <vector>
#include <functional>
#include <memory>
#include <stdexcept>


const size_t START_BUCKETS = 5;
const float START_MAX_LOAD = 1;


template <typename Key, typename T, typename Allocator = std::allocator<T>>
class List {
private:
    struct Node {
        T* value = nullptr;
        Node* next = nullptr;
        Node* prev = nullptr;
    };

public:
    struct const_iterator;

    struct iterator {
    friend List<Key, T, Allocator>::const_iterator::const_iterator(const List<Key, T, Allocator>::iterator& it);
    friend void List<Key, T, Allocator>::delete_single_node(iterator it);
    friend void List<Key, T, Allocator>::insert_node(const_iterator to_it, iterator from_it);
    public:
        iterator();
        iterator(Node* ptr);
        iterator& operator++();
        iterator operator++(int);
        iterator& operator--();
        iterator operator--(int);
        bool operator==(const iterator& it) const;
        bool operator!=(const iterator& it) const;
        T& operator*();
        T* operator->();
        bool empty() const;

        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using pointer = value_type*;
        using reference = value_type&;

    private:
        Node* ptr = nullptr;
    };

    struct const_iterator {
    friend void List<Key, T, Allocator>::insert(const_iterator it, const T& element);
    friend void List<Key, T, Allocator>::insert(const_iterator it, T&& element);
    friend void List<Key, T, Allocator>::erase(const_iterator it);
    friend void List<Key, T, Allocator>::insert_node(const_iterator to_it, iterator from_it);
    public:
        const_iterator();
        const_iterator(Node* ptr);
        const_iterator(const iterator& it);
        const_iterator& operator++();
        const_iterator operator++(int);
        const_iterator& operator--();
        const_iterator operator--(int);
        bool operator==(const const_iterator& it) const;
        bool operator!=(const const_iterator& it) const;
        const T& operator*() const;
        const T* operator->() const;
        bool empty() const;

        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using pointer = value_type*;
        using reference = const value_type&;

    private:
        Node* ptr = nullptr;
    };

    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    explicit List(const Allocator& alloc = Allocator());
    List(size_t count, const T& value, const Allocator& alloc = Allocator());
    List(size_t count, const Allocator& alloc = Allocator());
    List(const List& other);
    List(List&& other);
    List& operator=(const List& other);
    List& operator=(List&& other);
    ~List();
    Allocator& get_allocator();
    size_t size() const;
    void push_back(const T& element);
    void push_back(T&& element);
    void pop_back();
    void push_front(const T& element);
    void pop_front();
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    const_reverse_iterator rbegin() const;
    reverse_iterator rend();
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;
    void insert(const_iterator it, const T& element);
    void insert(const_iterator it, T&& element);
    void erase(const_iterator it);
    template< class... Args >
    iterator create_single_node(Args&&... args);
    void delete_single_node(iterator it);
    void insert_node(const_iterator to_it, iterator from_it);

private:
    void push_back();

    Allocator allocT;
    using AllocatorN = typename std::allocator_traits<Allocator>::template rebind_alloc<Node>;
    AllocatorN allocN;

    size_t size_ = 0;
    Node* first = allocN.allocate(1);
    Node* last = allocN.allocate(1);
};



template <typename Key, typename Value, typename Hash = std::hash<Key>, typename Equal = std::equal_to<Key>, typename Alloc = std::allocator<std::pair<const Key, Value>>>
class UnorderedMap {
public:
    using NodeType = std::pair<const Key, Value>;

    struct Iterator {
    friend void UnorderedMap<Key, Value, Hash, Equal, Alloc>::erase(Iterator it);
    template< class... Args >
    friend std::pair<typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Alloc>::emplace(Args&&... args);
    public:
        Iterator() = delete;
        Iterator(const typename List<Key, NodeType, Alloc>::iterator& it);
        Iterator& operator++();
        Iterator operator++(int);
        bool operator==(const Iterator& it) const;
        bool operator!=(const Iterator& it) const;
        NodeType& operator*();
        NodeType* operator->();

        using iterator_category = std::forward_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = NodeType;
        using pointer = value_type*;
        using reference = value_type&;

    private:
        typename List<Key, NodeType, Alloc>::iterator lit;
    };

    struct ConstIterator {
    friend void UnorderedMap<Key, Value, Hash, Equal, Alloc>::erase(Iterator it);
    public:
        ConstIterator() = delete;
        ConstIterator(const Iterator& it);
        ConstIterator(const typename List<Key, NodeType, Alloc>::const_iterator& it);
        ConstIterator& operator++();
        ConstIterator operator++(int);
        bool operator==(const ConstIterator& it) const;
        bool operator!=(const ConstIterator& it) const;
        const NodeType& operator*() const;
        const NodeType* operator->() const;

        using iterator_category = std::forward_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = NodeType;
        using pointer = value_type*;
        using reference = const value_type&;

    private:
        typename List<Key, NodeType, Alloc>::const_iterator lit;
    };

    UnorderedMap();
    UnorderedMap(const UnorderedMap& other);
    UnorderedMap(UnorderedMap&& other);
    UnorderedMap& operator=(const UnorderedMap& other);
    UnorderedMap& operator=(UnorderedMap&& other);
    ~UnorderedMap();
    Value& operator[](const Key& key);
    Value& at(const Key& key);
    const Value& at(const Key& key) const;
    size_t size() const;
    Iterator begin();
    ConstIterator begin() const;
    Iterator end();
    ConstIterator end() const;
    ConstIterator cbegin() const;
    ConstIterator cend() const;
    std::pair<Iterator, bool> insert(const NodeType& node);
    std::pair<Iterator, bool> insert(NodeType&& node);
    template< class InputIt >
    void insert(InputIt first, InputIt last);
    template< class... Args >
    std::pair<Iterator, bool> emplace(Args&&... args);
    void erase(Iterator it);
    void erase(Iterator it1, Iterator it2);
    Iterator find(const Key& key);
    ConstIterator find(const Key& key) const;
    size_t max_size() const;
    float load_factor() const;
    float max_load_factor() const;
    void max_load_factor(float ml);
    void reserve(size_t count);

private:
    float max_load = START_MAX_LOAD;
    List<Key, NodeType, Alloc> list;
    std::vector<typename List<Key, NodeType, Alloc>::iterator> data;
    Hash hash;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::UnorderedMap() : data(START_BUCKETS) {}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::UnorderedMap(const UnorderedMap& other)
    : max_load(other.max_load), list(other.list), data(other.data), hash(other.hash) {
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::UnorderedMap(UnorderedMap&& other)
        : max_load(other.max_load), list(std::move(other.list)), data(std::move(other.data)), hash(other.hash) {
    other.max_load = START_MAX_LOAD;
    while (other.list.size() != 0)
        other.list.pop_back();
    other.data.clear();
    other.data.resize(START_BUCKETS);
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>& UnorderedMap<Key, Value, Hash, Equal, Alloc>::operator=(const UnorderedMap& other) {
    if (this == &other)
        return *this;

    max_load = other.max_load;
    list = other.list;
    data = other.data;
    hash = other.hash;

    return *this;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>& UnorderedMap<Key, Value, Hash, Equal, Alloc>::operator=(UnorderedMap&& other) {
    if (this == &other)
        return *this;

    max_load = other.max_load;
    list = std::move(other.list);
    data = std::move(other.data);
    hash = other.hash;

    other.max_load = START_MAX_LOAD;
    other.data.clear();
    other.data.resize(START_BUCKETS);
    while (other.list.size() != 0)
        other.list.pop_back();

    return *this;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::~UnorderedMap() {}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
Value& UnorderedMap<Key, Value, Hash, Equal, Alloc>::operator[](const Key& key) {
    Iterator found(find(key));
    if (found != end())
        return found->second;
    
    return insert({key, Value()}).first->second;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
Value& UnorderedMap<Key, Value, Hash, Equal, Alloc>::at(const Key& key) {
    Iterator found(find(key));
    if (found == end())
        throw std::out_of_range("No value with this key");

    return found->second;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
const Value& UnorderedMap<Key, Value, Hash, Equal, Alloc>::at(const Key& key) const {
    ConstIterator found(find(key));
    if (found == end())
        throw std::out_of_range("No value with this key");

    return found->second;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
size_t UnorderedMap<Key, Value, Hash, Equal, Alloc>::size() const {
    return list.size();
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::begin() {
    return Iterator(list.begin());
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::begin() const {
    return ConstIterator(list.cbegin());
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::end() {
    return Iterator(list.end());
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::end() const {
    return ConstIterator(list.cend());
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::cbegin() const {
    return ConstIterator(list.cbegin());
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::cend() const {
    return ConstIterator(list.cend());
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
std::pair<typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Alloc>::insert(const NodeType& node) {
    Iterator found(find(node.first));
    if (found != end())
        return {found, false};

    size_t hash_code = hash(node.first);
    if (data[hash_code % data.size()].empty())
        data[hash_code % data.size()] = list.begin();

    list.insert(data[hash_code % data.size()], node);

    if (load_factor() >= max_load_factor())
            reserve(2 * size());

    return {--data[hash_code % data.size()], true};
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
std::pair<typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Alloc>::insert(NodeType&& node) {
    Iterator found(find(node.first));
    if (found != end())
        return {found, false};

    size_t hash_code = hash(node.first);
    if (data[hash_code % data.size()].empty())
        data[hash_code % data.size()] = list.begin();

    list.insert(data[hash_code % data.size()], std::move(node));

    if (load_factor() >= max_load_factor())
            reserve(2 * size());

    return {--data[hash_code % data.size()], true};
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
template <typename InputIt>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::insert(InputIt first, InputIt last) {
    for (; first != last; ++first)
        insert(*first);
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
template <typename... Args>
std::pair<typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator, bool> UnorderedMap<Key, Value, Hash, Equal, Alloc>::emplace(Args&&... args) {
    Iterator it(list.create_single_node(std::forward<Args>(args)...));
    
    Iterator found(find(it->first));
    if (found != end()) {
        list.delete_single_node(it.lit);
        return {found, false};
    }

    size_t hash_code = hash(it->first);
    if (data[hash_code % data.size()].empty())
        data[hash_code % data.size()] = list.begin();

    list.insert_node(data[hash_code % data.size()], it.lit);

    if (load_factor() >= max_load_factor())
            reserve(2 * size());

    return {--data[hash_code % data.size()], true};
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::erase(Iterator it) {
    size_t hash_code = hash(it->first) % data.size();
    if (Equal{}(data[hash_code]->first, it->first)) {
        auto next = data[hash_code];
        ++next;
        if ((next != list.end()) && ((hash(next->first) % data.size()) == hash_code))
            ++data[hash_code];
        else
            data[hash_code] = typename List<Key, NodeType, Alloc>::iterator();
    }
    list.erase(it.lit);
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::erase(Iterator it1, Iterator it2) {
    while (it1 != it2)
        erase(it1++);
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::find(const Key& key) {
    size_t hash_code = hash(key) % data.size();
    if (data[hash_code].empty())
        return end();

    Iterator it(data[hash_code]);

    while ((hash(it->first) % data.size()) == hash_code) {
        if (Equal{}(it->first, key))
            return it;
        ++it;
    }

    return end();
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::find(const Key& key) const {
    size_t hash_code = hash(key) % data.size();
    if (data[hash_code].empty())
        return end();

    ConstIterator it(data[hash_code]);

    while ((hash(it->first) % data.size()) == hash_code) {
        if (Equal{}(it->first, key))
            return it;
        ++it;
    }

    return end();
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
size_t UnorderedMap<Key, Value, Hash, Equal, Alloc>::max_size() const {
    return data.size();
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
float UnorderedMap<Key, Value, Hash, Equal, Alloc>::load_factor() const {
    return static_cast<float>(size()) / static_cast<float>(max_size());
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
float UnorderedMap<Key, Value, Hash, Equal, Alloc>::max_load_factor() const {
    return max_load;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::max_load_factor(float ml) {
    max_load = ml;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
void UnorderedMap<Key, Value, Hash, Equal, Alloc>::reserve(size_t count) {
    data.clear();
    data.resize(count);
    List<Key, NodeType, Alloc> old_list(std::move(list));

    for (auto&& el: old_list)
        insert(std::move(el));
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator::Iterator(const typename List<Key, NodeType, Alloc>::iterator& it)
    : lit(it) {}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator& UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator::operator++() {
    ++lit;
    return *this;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator::operator++(int) {
    UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator copy = *this;
    ++*this;
    return copy;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
bool UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator::operator==(const Iterator& it) const {
    return lit == it.lit;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
bool UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator::operator!=(const Iterator& it) const {
    return !(*this == it);
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::NodeType& UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator::operator*() {
    return *lit;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::NodeType* UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator::operator->() {
    return &(*lit);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator::ConstIterator(const Iterator& it)
    : lit(it.lit) {}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator::ConstIterator(const typename List<Key, NodeType, Alloc>::const_iterator& it)
    : lit(it) {}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator& UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator::operator++() {
    ++lit;
    return *this;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator::operator++(int) {
    UnorderedMap<Key, Value, Hash, Equal, Alloc>::Iterator copy = *this;
    ++*this;
    return copy;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
bool UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator::operator==(const ConstIterator& it) const {
    return lit == it.lit;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
bool UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator::operator!=(const ConstIterator& it) const {
    return !(*this == it);
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
const typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::NodeType& UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator::operator*() const {
    return *lit;
}

template <typename Key, typename Value, typename Hash, typename Equal, typename Alloc>
const typename UnorderedMap<Key, Value, Hash, Equal, Alloc>::NodeType* UnorderedMap<Key, Value, Hash, Equal, Alloc>::ConstIterator::operator->() const {
    return &(*lit);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename Key, typename T, typename Allocator>
List<Key, T, Allocator>::List(const Allocator& alloc) : allocT(alloc) {
    first->prev = last;
    last->next = first;
}

template <typename Key, typename T, typename Allocator>
List<Key, T, Allocator>::List(size_t count, const T& value, const Allocator& alloc) : allocT(alloc) {
    first->prev = last;
    last->next = first;

    for (int i = 0; i < count; ++i)
        push_back(value);
}

template <typename Key, typename T, typename Allocator>
List<Key, T, Allocator>::List(size_t count, const Allocator& alloc) : allocT(alloc) {
    first->prev = last;
    last->next = first;

    for (size_t i = 0; i < count; ++i)
        push_back();
}

template <typename Key, typename T, typename Allocator>
List<Key, T, Allocator>::List(const List& other) :
                allocT(std::allocator_traits<Allocator>::select_on_container_copy_construction(other.allocT)),
                allocN(std::allocator_traits<AllocatorN>::select_on_container_copy_construction(other.allocN)) {
    first->prev = last;
    last->next = first;

    for (const auto& el: other)
        push_back(el);
}

template <typename Key, typename T, typename Allocator>
List<Key, T, Allocator>::List(List&& other) :
                allocT(std::allocator_traits<Allocator>::select_on_container_copy_construction(other.allocT)),
                allocN(std::allocator_traits<AllocatorN>::select_on_container_copy_construction(other.allocN)) {
    first->prev = last;
    last->next = first;

    for (auto&& el: other)
        push_back(std::move(el));

    while (other.size() != 0)
        other.pop_back();
}

template <typename Key, typename T, typename Allocator>
List<Key, T, Allocator>& List<Key, T, Allocator>::operator=(const List<Key, T, Allocator>& other) {
    if (this == &other)
        return *this;

    while (size_ != 0)
        pop_back();

    if (std::allocator_traits<Allocator>::propagate_on_container_copy_assignment::value)
        allocT = other.allocT;
        
    if (std::allocator_traits<AllocatorN>::propagate_on_container_copy_assignment::value)
        allocN = other.allocN;

    for (const auto& el: other)
        push_back(el);

    return *this;
}

template <typename Key, typename T, typename Allocator>
List<Key, T, Allocator>& List<Key, T, Allocator>::operator=(List<Key, T, Allocator>&& other) {
    if (this == &other)
        return *this;

    while (size_ != 0)
        pop_back();

    if (std::allocator_traits<Allocator>::propagate_on_container_copy_assignment::value)
        allocT = other.allocT;
        
    if (std::allocator_traits<AllocatorN>::propagate_on_container_copy_assignment::value)
        allocN = other.allocN;

    for (auto&& el: other)
        push_back(std::move(el));

    return *this;
}

template <typename Key, typename T, typename Allocator>
List<Key, T, Allocator>::~List() {
    while (size_ != 0) pop_back();
    allocN.deallocate(first, 1);
    allocN.deallocate(last, 1);
}

template <typename Key, typename T, typename Allocator>
Allocator& List<Key, T, Allocator>::get_allocator() {
    return allocT;
}

template <typename Key, typename T, typename Allocator>
size_t List<Key, T, Allocator>::size() const {
    return size_;
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::push_back(const T& element) {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value, element);
    new_node->next = last->next;
    new_node->prev = last;
    last->next->prev = new_node;
    last->next = new_node;
}


template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::push_back(T&& element) {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value,
        std::move(const_cast<Key&>(element.first)), std::move(element.second));
    new_node->next = last->next;
    new_node->prev = last;
    last->next->prev = new_node;
    last->next = new_node;
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::push_back() {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value);
    new_node->next = last->next;
    new_node->prev = last;
    last->next->prev = new_node;
    last->next = new_node;
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::pop_back() {
    --size_;
    Node* to_del = last->next;
    to_del->next->prev = last;
    last->next = to_del->next;
    std::allocator_traits<Allocator>::destroy(allocT, to_del->value);
    allocT.deallocate(to_del->value, 1);
    std::allocator_traits<AllocatorN>::destroy(allocN, to_del);
    allocN.deallocate(to_del, 1);
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::push_front(const T& element) {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value, element);
    new_node->next = first;
    new_node->prev = first->prev;
    first->prev->next = new_node;
    first->prev = new_node;
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::pop_front() {
    --size_;
    Node* to_del = first->prev;
    to_del->prev->next = first;
    first->prev = to_del->prev;
    std::allocator_traits<Allocator>::destroy(allocT, to_del->value);
    allocT.deallocate(to_del->value, 1);
    std::allocator_traits<AllocatorN>::destroy(allocN, to_del);
    allocN.deallocate(to_del, 1);
}


template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::iterator List<Key, T, Allocator>::begin() {
    return iterator(first->prev);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::const_iterator List<Key, T, Allocator>::begin() const {
    return const_iterator(first->prev);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::iterator List<Key, T, Allocator>::end() {
    return iterator(last);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::const_iterator List<Key, T, Allocator>::end() const {
    return const_iterator(last);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::const_iterator List<Key, T, Allocator>::cbegin() const {
    return const_iterator(first->prev);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::const_iterator List<Key, T, Allocator>::cend() const {
    return const_iterator(last);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::reverse_iterator List<Key, T, Allocator>::rbegin() {
    return reverse_iterator(last);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::const_reverse_iterator List<Key, T, Allocator>::rbegin() const {
    return const_reverse_iterator(last);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::reverse_iterator List<Key, T, Allocator>::rend() {
    return reverse_iterator(first->prev);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::const_reverse_iterator List<Key, T, Allocator>::rend() const {
    return const_reverse_iterator(first->prev);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::const_reverse_iterator List<Key, T, Allocator>::crbegin() const {
    return const_reverse_iterator(last);
}

template <typename Key, typename T, typename Allocator>
typename List<Key, T, Allocator>::const_reverse_iterator List<Key, T, Allocator>::crend() const {
    return const_reverse_iterator(first->prev);
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::insert(const_iterator it, const T& element) {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value, element);
    new_node->next = it.ptr->next;
    new_node->prev = it.ptr;
    it.ptr->next->prev = new_node;
    it.ptr->next = new_node;
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::insert(const_iterator it, T&& element) {
    ++size_;
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value,
        std::move(const_cast<Key&>(element.first)), std::move(element.second));
    new_node->next = it.ptr->next;
    new_node->prev = it.ptr;
    it.ptr->next->prev = new_node;
    it.ptr->next = new_node;
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::erase(const_iterator it) {
    --size_;
    it.ptr->prev->next = it.ptr->next;
    it.ptr->next->prev = it.ptr->prev;
    std::allocator_traits<Allocator>::destroy(allocT, it.ptr->value);
    allocT.deallocate(it.ptr->value, 1);
    std::allocator_traits<AllocatorN>::destroy(allocN, it.ptr);
    allocN.deallocate(it.ptr, 1);
}

template <typename Key, typename T, typename Allocator>
template< class... Args >
typename List<Key, T, Allocator>::iterator List<Key, T, Allocator>::create_single_node(Args&&... args) {
    Node* new_node = allocN.allocate(1);
    std::allocator_traits<AllocatorN>::construct(allocN, new_node);
    new_node->value = allocT.allocate(1);
    std::allocator_traits<Allocator>::construct(allocT, new_node->value, std::forward<Args>(args)...);
    return iterator(new_node);
}

template <typename Key, typename T, typename Allocator>
void List<Key, T, Allocator>::delete_single_node(iterator it) {
    Node* to_del = it.ptr;
    std::allocator_traits<Allocator>::destroy(allocT, to_del->value);
    allocT.deallocate(to_del->value, 1);
    std::allocator_traits<AllocatorN>::destroy(allocN, to_del);
    allocN.deallocate(to_del, 1);
}

template <typename Key, typename T, typename Allocator>  
void List<Key, T, Allocator>::insert_node(const_iterator to_it, iterator from_it) {
    ++size_;
    Node* new_node = from_it.ptr;
    new_node->next = to_it.ptr->next;
    new_node->prev = to_it.ptr;
    to_it.ptr->next->prev = new_node;
    to_it.ptr->next = new_node;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename Key, typename T, typename Allocator = std::allocator<T>>
List<Key, T, Allocator>::iterator::iterator() {}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
List<Key, T, Allocator>::iterator::iterator(List<Key, T, Allocator>::Node* ptr) : ptr(ptr) {}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
typename List<Key, T, Allocator>::iterator& List<Key, T, Allocator>::iterator::operator++() {
    ptr = ptr->prev;
    return *this;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
typename List<Key, T, Allocator>::iterator List<Key, T, Allocator>::iterator::operator++(int) {
    List<Key, T, Allocator>::iterator copy = *this;
    ++*this;
    return copy;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
typename List<Key, T, Allocator>::iterator& List<Key, T, Allocator>::iterator::operator--() {
    ptr = ptr->next;
    return *this;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
typename List<Key, T, Allocator>::iterator List<Key, T, Allocator>::iterator::operator--(int) {
    List<Key, T, Allocator>::iterator copy = *this;
    --*this;
    return copy;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
bool List<Key, T, Allocator>::iterator::operator==(const iterator& it) const {
    return ptr == it.ptr;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
bool List<Key, T, Allocator>::iterator::operator!=(const iterator& it) const {
    return !(*this == it);
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
T& List<Key, T, Allocator>::iterator::operator*() {
    return *(ptr->value);
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
T* List<Key, T, Allocator>::iterator::operator->() {
    return ptr->value;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
bool List<Key, T, Allocator>::iterator::empty() const {
    return ptr == nullptr;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template <typename Key, typename T, typename Allocator = std::allocator<T>>
List<Key, T, Allocator>::const_iterator::const_iterator() {}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
List<Key, T, Allocator>::const_iterator::const_iterator(List<Key, T, Allocator>::Node* ptr) : ptr(ptr) {}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
List<Key, T, Allocator>::const_iterator::const_iterator(const List<Key, T, Allocator>::iterator& it) : ptr(it.ptr) {}


template <typename Key, typename T, typename Allocator = std::allocator<T>>
typename List<Key, T, Allocator>::const_iterator& List<Key, T, Allocator>::const_iterator::operator++() {
    ptr = ptr->prev;
    return *this;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
typename List<Key, T, Allocator>::const_iterator List<Key, T, Allocator>::const_iterator::operator++(int) {
    List<Key, T, Allocator>::const_iterator copy = *this;
    ++*this;
    return copy;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
typename List<Key, T, Allocator>::const_iterator& List<Key, T, Allocator>::const_iterator::operator--() {
    ptr = ptr->next;
    return *this;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
typename List<Key, T, Allocator>::const_iterator List<Key, T, Allocator>::const_iterator::operator--(int) {
    List<Key, T, Allocator>::const_iterator copy = *this;
    --*this;
    return copy;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
bool List<Key, T, Allocator>::const_iterator::operator==(const const_iterator& it) const {
    return ptr == it.ptr;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
bool List<Key, T, Allocator>::const_iterator::operator!=(const const_iterator& it) const {
    return !(*this == it);
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
const T& List<Key, T, Allocator>::const_iterator::operator*() const {
    return *(ptr->value);
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
const T* List<Key, T, Allocator>::const_iterator::operator->() const {
    return ptr->value;
}

template <typename Key, typename T, typename Allocator = std::allocator<T>>
bool List<Key, T, Allocator>::const_iterator::empty() const {
    return ptr == nullptr;
}
