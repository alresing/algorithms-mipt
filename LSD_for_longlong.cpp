#include <stdint.h>
#include <stdio.h>
#include <vector>

void lsd_sort(std::vector<int64_t> &v) {
    int64_t number = 1;
    for (int r = 0; r < 8; ++r) {
        std::vector<int64_t> count(256,0);
        for (int i = 0; i < v.size(); ++i) {
            ++count[ (v[i] / number) % 256 ];
        }
        for (int i = 1; i < 256; ++i) {
            count[i] += count[i-1];
        }
        std::vector<int64_t> ans(v.size(), 0);
        for (int i = v.size()-1; i >= 0; --i) {
            ans[ --count[(v[i] / number) % 256] ] = v[i];
        }
        v = ans;

        number *= 256;
    }
}

int main() {
    int n; scanf("%d", &n);
    std::vector<int64_t> v;

    for (int i = 0; i < n; ++i) {
        int64_t t;
        scanf("%lld", &t);
        v.push_back(t);
    }

    lsd_sort(v);

    for(int i = 0; i < n; ++i) {
        printf("%lld ", v[i]);
    }
    printf("\n");

    return 0;
}