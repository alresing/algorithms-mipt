#include <iostream>
#include <vector>
#include <limits>
#include <queue>


const size_t INF = std::numeric_limits<size_t>::max();


class Graph {
public:
    Graph(std::vector<std::vector<std::pair<size_t, size_t>>> graph) 
        : graph(graph) {}

    const std::vector<std::pair<size_t, size_t>>& get(size_t index) const {
        return graph[index];
    }

    size_t size() const {
        return graph.size();
    }

private:
    std::vector<std::vector<std::pair<size_t, size_t>>> graph;
};


size_t prima(const Graph& graph) {
    std::vector<bool> used(graph.size(), false);
    size_t res = 0;
    std::priority_queue<std::pair<size_t, size_t>, 
                        std::vector<std::pair<size_t, size_t>>,
                        std::greater<std::pair<size_t, size_t>>> q;
    q.push({0, 0});

    while (!q.empty()) {
        auto [dist, v] = q.top();
        q.pop();

        if (used[v])
            continue;

        used[v] = true;
        res += dist;

        for (const auto& [to, w]: graph.get(v))
            if (!used[to])
                q.push({w, to});
    }

    return res;
}


int main() {
    size_t n, m;
    std::cin >> n >> m;
    std::vector<std::vector<std::pair<size_t, size_t>>> graph_vec(n, std::vector<std::pair<size_t, size_t>>());
    for (size_t i = 0; i < m; ++i) {
        size_t a, b, w;
        std::cin >> a >> b >> w;
        --a; --b;
        graph_vec[a].emplace_back(b, w);
        graph_vec[b].emplace_back(a, w);
    }
    Graph graph(graph_vec);

    std::cout << prima(graph) << std::endl;
}
