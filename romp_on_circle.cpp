#include <iostream>
#include <stdio.h>
#include <vector>

void merge(std::vector<int64_t> &a, int64_t l, int64_t m, int64_t r, int64_t k) {
    int64_t n1 = m-l+1;
    int64_t n2 = r-m;
 
    std::vector<int64_t> L;
    std::vector<int64_t> R;
 
    for (int64_t i = 0; i < n1; i++) {
        L.push_back(a[l+i]);
    }
    for (int64_t j = 0; j < n2; j++) {
        R.push_back(a[m+1+j]);
    }
 
    int64_t i = 0;
    int64_t j = 0;
    int64_t t = l;
    while (i < n1 && j < n2) {
        if (L[i]%k <= R[j]%k) {
            a[t] = L[i];
            i += 1;
        } else {
            a[t] = R[j];
            j += 1;
        }
        t += 1;
    }
 
    while (i < n1) {
        a[t] = L[i];
        i += 1;
        t += 1;
    }
 
    while (i < n2) {
        a[t] = R[j];
        j += 1;
        t += 1;
    }
}
  
void mergeSort(std::vector<int64_t> &a, int64_t l, int64_t r, int64_t k) {
    if (l<r) {
        int64_t m = l+(r-l)/2;
        mergeSort(a, l, m, k);
        mergeSort(a, m+1, r, k);
 
        merge(a, l, m, r, k);
    }
}

int main() {
    int64_t l, n, k; scanf("%lld%lld%lld", &l, &n, &k);

    std::vector<int64_t> data;
    std::vector<int64_t> counts(k, 0);

    for (int i = 0; i < n; ++i) {
        int64_t t; scanf("%lld", &t);
        data.push_back(t-1);
        ++counts[(t-1)/(l/k)];
    }

    mergeSort(data, 0, data.size()-1, l/k);     // по v[i] % (l/k)

    int64_t shift = 0;
    int64_t index = 0;

    int64_t nowMax = counts[0];
    int64_t nowMin = counts[0];

    for (int i = 0; i < k; ++i) {
        if (nowMax < counts[i]) {
            nowMax = counts[i];
        }
        if (nowMin > counts[i]) {
            nowMin = counts[i];
        }
    }

    int64_t minF = nowMax - nowMin;
    int64_t countMinFCuts = 1;
    int64_t minFExampleCut = 0;

    while (true) {
        if (shift == minFExampleCut) {
            countMinFCuts += data[index] % (l/k) - shift;
        }
        shift = data[index] % (l/k) + 1;

        while (data[index]%(l/k) + 1 == shift && index < data.size()) {
            --counts[data[index]/(l/k)];
            ++counts[(data[index]/(l/k) - 1 + k)%k];
            ++index;
        }

        int64_t nowMax = counts[0];
        int64_t nowMin = counts[0];
        for (int i = 0; i < k; ++i) {
            if (nowMax < counts[i]) {
                nowMax = counts[i];
            }
            if (nowMin > counts[i]) {
                nowMin = counts[i];
            }
        }

        if (nowMax - nowMin < minF) {
            minF = nowMax - nowMin;
            countMinFCuts = 1;
            minFExampleCut = shift;
        } else if (nowMax - nowMin == minF) {
            countMinFCuts += 1;
            minFExampleCut = shift;
        }

        if (index == data.size()) { 
            if (minFExampleCut == shift) {
                countMinFCuts += (l/k) -shift-1;
            }
            break;
        }
    }

    printf("%lld %lld\n%lld", minF, countMinFCuts*k, minFExampleCut);

    return 0;
}