#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node Node;

struct Node {
    int val;
    Node* last;
};

typedef struct Stack Stack;

struct Stack {
    int size;
    Node* last;
};

int push(Stack* st, int n) {
    Node* newNode = malloc(sizeof(Node));
    if (newNode == NULL) return -1;

    newNode->val = n;

    if (st->size > 0) {
        newNode->last = st->last;
    } else {
        newNode->last = NULL;
    }
    st->last = newNode;
    st->size += 1;
    return 0;
}

int pop(Stack* st, int* res) {
    if (st->size == 0) return -1;

    *res = st->last->val;
    Node* wasLastNode = st->last;

    st->last = wasLastNode->last;
    st->size -= 1;

    free(wasLastNode);

    return 0;
}

int back(Stack* st, int* res) {
    if (st->size == 0) return -1;

    *res = st->last->val;
    return 0;
}

int size(Stack* st) {
    return st->size;
}

void clear(Stack* st) {
    while (st->size > 0) {
        int t;
        pop(st, &t);
    }
}

int isGoodPSP(Stack* st) {
    char s;
    int t;

    while ((s = getchar()) != '\n' && s != EOF) {
        if (s == '(' || s == '[' || s == '{') {
            push(st, s);
        } else if (back(st, &t) == -1) {
            return 1;
        } else if  ((t == '(' && s == ')') ||
                    (t == '[' && s == ']') ||
                    (t == '{' && s == '}') ) {
                pop(st, &t);
        } else {
            return 1;
        }
    }

    if (size(st) == 0) return 0;
    return 1;
}


int main() {
    Stack st; st.size = 0;
    if (isGoodPSP(&st) == 0) {
        printf("yes");
    } else {
        printf("no");
    }

    return 0;
}