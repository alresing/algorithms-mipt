#include <iostream>
#include <vector>
#include <algorithm>


struct Edge {
    uint64_t w;
    uint64_t v;
    uint64_t u;
};


bool operator<(const Edge& lhs, const Edge& rhs) {
    return lhs.w < rhs.w;
}


uint64_t get(uint64_t v, std::vector<int64_t>& p) {
    if (p[v] == -1)
        return v;
    else
        return p[v] = get(p[v], p);
}


void unite(uint64_t v, uint64_t u, std::vector<int64_t>& p, std::vector<uint64_t>& size) {
    u = get(u, p); v = get(v, p);
    if (u == v)
        return;
    if (size[u] < size[v])
        std::swap(u, v);
    p[v] = u;
    size[u] += size[v];
}


uint64_t kruskala(std::vector<Edge>& edges, uint64_t n) {
    std::vector<int64_t> p(n, -1);
    std::vector<uint64_t> size(n, 1);
    uint64_t res = 0;

    for (const auto& t: edges) {
        if (get(t.v, p) == get(t.u, p))
            continue;

        res += t.w;
        unite(t.v, t.u, p, size);
    }

    return res;
}


int main() {
    uint64_t n, m;
    std::cin >> n >> m;
    std::vector<uint64_t> a(n);
    for (uint64_t i = 0; i < n; ++i)
        std::cin >> a[i];
    std::vector<Edge> edges;
    for (uint64_t i = 0; i < m; ++i) {
        uint64_t x, y, w;
        std::cin >> x >> y >> w;
        edges.push_back({w, --x, --y});
    }

    uint64_t minimum = 0;
    for (uint64_t i = 0; i < n; ++i)
        if (a[minimum] > a[i])
            minimum = i;
    for (uint64_t i = 0; i < n; ++i)
        if (i != minimum)
            edges.push_back({a[i] + a[minimum], i, minimum});

    std::sort(edges.begin(), edges.end());
    std::cout << kruskala(edges, n) << std::endl;
}
