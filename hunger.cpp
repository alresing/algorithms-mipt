#include <iostream>
#include <vector>


const int64_t MODULE = 1'000'000'007;


void print(const std::vector<auto>& v) {
    for (const auto& i : v)
        std::cout << i << " ";
    std::cout << std::endl;
}


void print(const std::vector<std::vector<auto>>& m) {
    for (const auto& row : m)
        print(row);
    std::cout << std::endl;
}


void print(const std::vector<std::vector<std::vector<auto>>>& m) {
    for (const auto& column : m)
        print(column);
    std::cout << std::endl;
}


bool isGood(const int64_t& j,
            const int64_t& i,
            const int64_t& mask,
            const std::vector<std::vector<char>>& cities) {
    for (int64_t index = 0; index <= i; ++index)
        if (((mask >> index) & 1) && cities[index][j] != '.')
            return false;

    if (j > 0)
        for (uint64_t index = i; index < cities.size(); ++index)
            if (((mask >> (index+1)) & 1) && cities[index][j-1] != '.')
                return false;

    return true;
}


int64_t one(int64_t mask, const int64_t& i) {
    if (((mask >> i) & 1) != 1)
        mask += 1 << i;
    return mask;
}


int64_t zero(int64_t mask, const int64_t& i) {
    if (((mask >> i) & 1) != 0)
        mask -= 1 << i;
    return mask;
}


int64_t slide(const int64_t& mask, const int64_t& i, const int64_t& n) {
    int64_t res = i;
    res += (mask << 1);
    if ((res >> (n+1)) != 0)
        res -= (1 << (n+1));
    return res;
}


int main() {
    int64_t n, m; std::cin >> n >> m;
    std::vector<std::vector<char>> cities;
    for (int64_t i = 0; i < n; ++i) {
        std::vector<char> row(m, 0);
        for (int64_t j = 0; j < m; ++j)
            std::cin >> row[j];
        cities.push_back(row);
    }

    // dp[j][i][mask]
    // i -- после какой строкой излом (в i строке 2 клетки находятся в mask)
    // j -- в каком столбце излом
    // mask -- набор 1 или 0 в профиле из (1+n) значения
    std::vector<std::vector<std::vector<int64_t>>> dp;
    for (int64_t j = 0; j < 2; ++j) {
        std::vector<std::vector<int64_t>> column;
        for (int64_t i = 0; i < n; ++i) {
            std::vector<int64_t> row(1 << (1+n), 0);
            column.push_back(row);
        }
        dp.push_back(column);
    }

    for (int64_t i = 0; i < n; ++i)
        for (int64_t mask = 0; mask < (1 << (n+1)); ++mask)
            if (isGood(0, i, mask, cities))
                dp[0][i][mask] = 1;

    for (int64_t mask = 0; mask < (1 << n); ++mask) {
        if (isGood(0, n-1, mask, cities)) {
            int64_t slided0 = slide(mask, 0, n);
            int64_t slided1 = slide(mask, 1, n);
            dp[1][0][slided0] += 1;
            if (cities[0][1] == '.')
                dp[1][0][slided1] += 1;
        }
    }

    for (int i = 0; i < n; ++i)
        for (int mask = 0; mask < (1 << (n+1)); ++mask)
            dp[0][i][mask] = 0;

    for (int64_t j = 1; j < m; ++j) {
        for (int64_t i = 0; i < n; ++i) {
            for (int64_t mask = 0; mask < (1 << (n+1)); ++mask) {
                if (dp[j%2][i][mask] != 0) {
                    if (i < (n-1)) {
                        int64_t a = ((mask >> i) & 1) || (cities[i][j] == '+');
                        int64_t b = ((mask >> (i+1)) & 1) || (cities[i][j-1] == '+');
                        int64_t c = ((mask >> (i+2)) & 1) || (cities[i+1][j-1] == '+');
                        int64_t oned = one(mask, i+1);
                        int64_t zeroed = zero(mask, i+1);
                        if (((a + b + c) == 1) && (cities[i+1][j] == '+')) {
                            dp[j%2][i+1][zeroed] += dp[j%2][i][mask];
                            dp[j%2][i+1][zeroed] %= MODULE;
                        }
                        if (((a + b + c) == 1) && (cities[i+1][j] == '.') && (cities[i+1][j] == '.')) {
                            dp[j%2][i+1][oned] += dp[j%2][i][mask];
                            dp[j%2][i+1][oned] %= MODULE;
                        }
                            
                        if (((a + b + c) == 2) && (cities[i+1][j] == '-')) {
                            dp[j%2][i+1][zeroed] += dp[j%2][i][mask];
                            dp[j%2][i+1][zeroed] %= MODULE;
                        }
                        if (((a + b + c) == 2) && (cities[i+1][j] == '.')) {
                            dp[j%2][i+1][zeroed] += dp[j%2][i][mask];
                            dp[j%2][i+1][zeroed] %= MODULE;
                        }

                    } else if (j < (m-1)) {    //  i == n-1
                        int64_t slided0 = slide(mask, 0, n);
                        int64_t slided1 = slide(mask, 1, n);
                        dp[(j+1)%2][0][slided0] += dp[j%2][i][mask];
                        dp[(j+1)%2][0][slided0] %= MODULE;
                        if (cities[0][j+1] == '.') {
                            dp[(j+1)%2][0][slided1] += dp[j%2][i][mask];
                            dp[(j+1)%2][0][slided1] %= MODULE;
                        }
                    }
                }
                if (j != (m-1))
                    dp[j%2][i][mask] = 0;
            }
        }
    }

    int64_t res = 0;
    for (int64_t mask = 0; mask < (1 << (n+1)); ++mask) {
        res += dp[(m+1)%2][n-1][mask];
        res %= MODULE;
    }
    std::cout << res << std::endl;
}
