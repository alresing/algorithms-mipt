#pragma once

#include <iostream>
#include <cstring>

#define ll long unsigned int

class String{
friend bool operator==(const String& s1, const String& s2);
friend std::ostream& operator<<(std::ostream& out, const String& s);

public:
    String() {
        capacity = 1;
        str = new char[1];
    }

    String(const char* c) {
        size = strlen(c);
        capacity = size;
        str = new char[capacity];
        std::copy(c, c+capacity, str);
    }

    String(const ll n, const char c) {
        size = n;
        capacity = n;
        str = new char[capacity];
        memset(str, c, capacity);
    }

    String(const String& s) {
        size = s.size;
        capacity = s.capacity;
        str = new char[capacity];
        std::copy(s.str, s.str+size, str);
    }

    ~String() {
        delete[] str;
    }

    String& operator=(const String& s) {
        if (this == &s) {
            return *this;
        }

        delete[] str;
        capacity = s.capacity;
        size = s.size;
        str = new char[capacity];
        std::copy(s.str, s.str+size, str);

        return *this;
    }

    String& operator+=(const char c) {
        push_back(c);
        return *this;
    }

    String& operator+=(const String& s) {
        capacity += s.capacity;
        char* new_str = new char[capacity];
        std::copy(str, str+size, new_str);
        std::copy(s.str, s.str+s.size, new_str+size);
        size += s.size;
        delete[] str;
        str = new_str;

        return *this;
    }

    const char& operator[](ll index) const {
        return str[index];
    }

    char& operator[](ll index) {
        return str[index];
    }

    const char& front() const{
        return str[0];
    }

    char& front() {
        return str[0];
    }

    const char& back() const {
        return str[size-1];
    }

    char& back() {
        return str[size-1];
    }

    void push_back(const char c) {
        if (size >= capacity) extend();
        str[size] = c;
        ++size;
    }

    void pop_back() {
        if (size == 0) return;
        --size;
        if (size < (capacity/4)) decrease();
    }

    bool empty() const {
        return (size == 0);
    }

    void clear() {
        delete[] str;
        size = 0;
        capacity = 1;
        str = new char[1];
    }

    ll length() const {
        return size;
    }

    String substr(ll start, ll count) const {
        String s;
        s.capacity = count;
        s.size = count;
        s.str = new char[count];
        std::copy(str+start, str+start+count, s.str);

        return s;
    }

    ll find(const String& s) const {
        for (ll i = 0; i < 1 + size - s.size; ++i) {
            if (substr(i, s.size) == s) {
                return i;
            }
        }

        return size;
    }

    ll rfind(const String& s) const {
        for (int64_t i = size - s.size; i > -1; --i) {
            if (substr(i, s.size) == s) {
                return i;
            }
        }

        return size;
    }

private:
    void extend() {
        char* new_str = new char[capacity*2];
        std::copy(str, str+size, new_str);

        delete[] str;
        str = new_str;
        capacity *= 2;
    }

    void decrease() {
        capacity = (capacity+1) / 2;
        char* new_str = new char[capacity];
        std::copy(str, str+size, new_str);

        delete[] str;
        str = new_str;
    }

    ll size = 0;
    ll capacity = 0;
    char* str = nullptr;
};

String operator+(const String& a, const String& b) {
    String copy = a;
    copy += b;
    return copy;
}

String operator+(const String& a, const char& b) {
    String copy = a;
    copy += b;
    return copy;
}

String operator+(const char& a, const String& b) {
    String copy(1, a);
    copy += b;
    return copy;
}

bool operator==(const String& s1, const String& s2) {
    if (s1.size != s2.size) return false;
    return memcmp(s1.str, s2.str, s1.size) == 0;
}

std::ostream& operator<<(std::ostream& out, const String& s) {
    for (ll i = 0; i < s.size; ++i) {
        out << s.str[i];
    }
    return out;
}

std::istream& operator>>(std::istream& in, String& s) {
    s.clear();

    char c;
    while (in.get(c)) {
        if (std::isspace(c)) break;
        s.push_back(c);
    }
    return in;
}