#include <iostream>
#include <vector>

class HashTable {
public:
    HashTable() {
        for (int64_t i = 0; i < M; ++i) {
            v.push_back({-1, -1});
        }
    }

    int64_t swap(int64_t a, int64_t b) {
        int64_t aIndex = getRealIndex(a);
        int64_t bIndex = getRealIndex(b);

        std::swap(v[aIndex].second, v[bIndex].second);
        return std::max(v[aIndex].second - v[bIndex].second, v[bIndex].second - v[aIndex].second);
    }

private:
    const int64_t M = 1'000'003;
    const int64_t A = 733;
    const int64_t B = 29251;

    int64_t prop(int64_t index) {
        return (index + 1) % M;
    }

    int64_t hash(int64_t a) {
        return (a*A + B) % M;
    }

    std::vector<std::pair<int64_t, int64_t>> v;

    int64_t getRealIndex(int64_t a) {
        int64_t hashFromA = hash(a);
        while ((v[hashFromA].first != -1) && (v[hashFromA].first != a)) {
            hashFromA = prop(hashFromA);
        }
        if (v[hashFromA].first == -1) {
            v[hashFromA] = {a, a};
        }
        return hashFromA;
    }
};

int main() {
    HashTable table;

    int64_t n; std::cin >> n;
    for (int64_t i = 0; i < n; ++i) {
        int64_t a, b; std::cin >> a >> b;
        std::cout << table.swap(a, b) << std::endl;
    }
}