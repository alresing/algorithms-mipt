#include <iostream>
#include <vector>
#include <queue>
#include <limits>


const int INF = std::numeric_limits<int>::max();


class Graph {
public:
    Graph(size_t n) : graph(n) {}

    std::vector<std::pair<int, int>>& get(size_t index) {
        return graph[index];
    }

    size_t size() {
        return graph.size();
    }

private:
    std::vector<std::vector<std::pair<int, int>>> graph;
};


int dijkstra(Graph& graph, int s, int t) {
    std::vector<int> dist(graph.size(), INF);
    std::vector<bool> used(graph.size(), false);
    std::priority_queue<std::pair<int, int>> q;
    dist[s] = 0;
    q.emplace(-dist[s], s);
    while (!q.empty()) {
        int v = q.top().second;
        q.pop();
        if (dist[v] == INF)
            break;
        if (used[v])
            continue;
        used[v] = true;
        for (const auto& [to, weight] : graph.get(v)) {
            dist[to] = std::min(dist[to], dist[v] + weight);
            q.emplace(-dist[to], to);
        }
    }

    return (dist[t] == INF) ? -1 : dist[t];
}


int main() {
    size_t n, m, s, t;
    std::cin >> n >> m >> s >> t;
    Graph graph(n);

    for (size_t i = 0; i < m; ++i) {
        int v, to, w;
        std::cin >> v >> to >> w;
        graph.get(v-1).emplace_back(to-1, w);
        graph.get(to-1).emplace_back(v-1, w);
    }

    std::cout << dijkstra(graph, s-1, t-1) << std::endl;
}
