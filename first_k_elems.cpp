#include <iostream>
#include <vector>
 
class Heap {
public:
    Heap() {
        Size = 0;
    }

    ~Heap() {}

    void AddElem(int t) {
        int index = Size;
        Data.push_back(t);
        int parent = (index-1)/2;
        while (parent >= 0 && index > 0) {
            if (Data[index] > Data[parent]) {
                std::swap(Data[index], Data[parent]);
            }
            index = parent;
            parent = (index-1)/2;
        }
        Size += 1;
    }

    int ExtractMax() {
        if (Size == 0) {
            throw std::runtime_error("Heap is empty for extract");
        }
        int element = Data[0];
        Data[0] = Data.back();
        Data.pop_back();
        Size -= 1;
        SiftDown(0);

        return element;
    }

    size_t GetSize() const {
        return Size;
    }

    int GetMax() const {
        return Data[0];
    }

private:
    void SiftDown(int index) {
        int left = 2*index + 1;
        int right = 2*index + 2;

        if (left < Size) {
            if (Data[index] < Data[left]) {
                std::swap(Data[index], Data[left]);
                SiftDown(left);
            }
        }

        if (right < Size) {
            if (Data[index] < Data[right]) {
                std::swap(Data[index], Data[right]);
                SiftDown(right);
            }
        }
    }

    size_t Size;
    std::vector<int> Data;
};


int main() {
    int n, k;
    std::cin >> n >> k;

    Heap heap;

    for (int i = 0; i < n; ++i) {
        int t;
        std::cin >> t;

        if (heap.GetSize() < k) {
            heap.AddElem(t);
        } else if (heap.GetMax() > t) {
            heap.ExtractMax();
            heap.AddElem(t);
        }
    }

    std::vector<int> v;

    while (heap.GetSize() > 0) {
        v.push_back( heap.ExtractMax() );
    }

    for (int i = v.size()-1; i >= 0; --i) {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}