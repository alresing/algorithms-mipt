#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>


class Graph {
public:
    void push_back(std::tuple<size_t, size_t, size_t> tuple) {
        edges.push_back(tuple);
    }

    void sort() {
        std::sort(edges.begin(), edges.end());
    }

    size_t size() {
        return edges.size();
    }

    std::tuple<size_t, size_t, size_t>& get(size_t i) {
        return edges[i];
    }

private:
    std::vector<std::tuple<size_t, size_t, size_t>> edges;
};


class MST {
public:
    MST(size_t n) : p(n, -1) {}

    size_t get(size_t v) {
        if (p[v] == -1)
            return v;
        return p[v] = get(p[v]);
    }

    void unite(size_t v, size_t u) {
        u = get(u); v = get(v);
        if (u != v)
            p[v] = u;
    }

private:
    std::vector<int> p;
};


size_t kruskal(Graph& edges, size_t n) {
    edges.sort();

    MST mst(n);
    size_t res = 0;

    for (size_t i = 0; i < edges.size(); ++i) {
        auto [w, v, u] = edges.get(i);
        if (mst.get(v) == mst.get(u))
            continue;

        res += w;
        mst.unite(v, u);
    }

    return res;
}


int main() {
    size_t n, m;
    std::cin >> n >> m;
    Graph edges;
    for (size_t i = 0; i < m; ++i) {
        size_t a, b, w;
        std::cin >> a >> b >> w;
        --a; --b;
        edges.push_back({w, a, b});
    }

    std::cout << kruskal(edges, n) << std::endl;
}
