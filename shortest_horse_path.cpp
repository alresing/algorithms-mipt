#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <deque>
#include <algorithm>

const int INF = 1073741824;


bool good(int i, int j) {
    if ((i < 0) || (j < 0) || (i > 7) || (j > 7))
        return false;
    return true;
}


class Graph {
public:
    Graph() {
        for (size_t i = 0; i < 64; ++i)
            graph.emplace_back();

        for (size_t i = 0; i < 8; ++i) {
            for (size_t j = 0; j < 8; ++j) {
                if (good(i-1, j-2))
                    graph[i*8 + j].push_back((i-1)*8 + j-2);
                if (good(i-2, j-1))
                    graph[i*8 + j].push_back((i-2)*8 + j-1);
                if (good(i-1, j+2))
                    graph[i*8 + j].push_back((i-1)*8 + j+2);
                if (good(i-2, j+1))
                    graph[i*8 + j].push_back((i-2)*8 + j+1);
                if (good(i+1, j-2))
                    graph[i*8 + j].push_back((i+1)*8 + j-2);
                if (good(i+2, j-1))
                    graph[i*8 + j].push_back((i+2)*8 + j-1);
                if (good(i+1, j+2))
                    graph[i*8 + j].push_back((i+1)*8 + j+2);
                if (good(i+2, j+1))
                    graph[i*8 + j].push_back((i+2)*8 + j+1);
            }
        }
    }

    const std::vector<int>& get(int i) const {
        return graph[i];
    }

private:
    std::vector<std::vector<int>> graph;
};


std::vector<int> bfs(const Graph& graph, int s, int t) {
    std::vector<int> dist(64, INF);
    dist[s] = 0;
    std::vector<int> p(64, -1);
    p[s] = -1;
    std::deque<int> q;
    q.push_back(s);
    while (!q.empty()) {
        int v = q.front();
        q.pop_front();
        for (int to : graph.get(v)) {
            if (dist[to] == INF) {
                dist[to] = 1 + dist[v];
                p[to] = v;
                q.push_back(to);
            }
        }
    }

    std::vector<int> path;
    for (int v = t; v != -1; v = p[v])
        path.push_back(v);
    std::reverse(path.begin(), path.end());
    return path;
}


int main() {
    std::string ss; std::cin >> ss;
    std::string ts; std::cin >> ts;
    int s = ss[0] - 97 + (ss[1] - 49) * 8;
    int t = ts[0] - 97 + (ts[1] - 49) * 8;

    Graph graph;
    std::vector<int> answer = bfs(graph, s, t);

    for (const auto& v : answer)
        std::cout << static_cast<char>(97 + (v % 8)) << (v / 8) + 1 << std::endl;
}
