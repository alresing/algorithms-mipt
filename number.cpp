#include <vector>
#include <string>
#include <fstream>

int main() {
    std::ifstream inf("number.in");

    std::vector<std::string> v;
    std::string s;

    while(inf >> s) {
        v.push_back(s);
    }

    for(int j = v.size()-1; j >= 0; --j) {
        for(int i = 0; i < j; ++i) {
            if (v[i+1]+v[i] > v[i]+v[i+1]) {
                swap(v[i],v[i+1]);
            }
        }
    }

    std::ofstream outf("number.out");

    for(int j = 0; j < v.size(); ++j) {
        outf << v[j];
    }
}