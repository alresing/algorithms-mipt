/*
    Массив из n чисел. Можно вычесть из одного p и из всех остальных q.
    За какое минимальное кол-во таких шагов можно сделать все числа
    массива не больше, чем 0?
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int check(int64_t t, int n, int a[], int64_t p, int64_t q) {
    int64_t res = 0;
    if (p == 0) {
        for (int i = 0; i < n; i++) {
            if ((a[i] - q*t) > 0) return 0;
        }
        return 1;
    }

    for (int i = 0; i < n; ++i) {
        int64_t tmp = a[i] - q*t;
        if (tmp >= 0) {
            tmp /= p;
            tmp += (a[i] - q*t) % p == 0 ? 0 : 1;
            res += tmp;
        }
    }

    return res <= t;
}


int64_t binSearch(int a[], int n, int p, int q) {
    int64_t left = 0;
    int64_t right = 10000000000;

    while (right - left > 1) {
        int64_t m = (right + left - 1) / 2;
        if (!check(m, n, a, p, q))  { left = m+1; }
        else                        { right = m+1; }
    }

    return left;
}

int main() {
    int n, p, q;
    scanf("%d%d%d", &n, &p, &q);
    p -= q;

    int a[n];
    
    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
    }

    printf("%lld\n", binSearch(a, n, p, q)); 

    return 0;
}