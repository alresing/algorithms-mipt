#include <iostream>
#include <vector>

int main() {
    int n; std::cin >> n;
    std::vector<int> a;
    for (int i = 0; i < n; ++i) {
        int p; std::cin >> p;
        a.push_back(p);
    }

    int m; std::cin >> m;
    std::vector<int> b;
    for (int i = 0; i < m; ++i) {
        int p; std::cin >> p;
        b.push_back(p);
    }

    std::vector<std::vector<int>> dp;
    for (int i = 0; i <= n; ++i) {
        std::vector<int> temp;
        for (int j = 0; j <= m; ++j)
            temp.push_back(0);
        dp.push_back(temp);
    }

    for (int aIndex = 1; aIndex <= n; ++aIndex) {
        for (int bIndex = 1; bIndex <= m; ++bIndex) {
            dp[aIndex][bIndex] = std::max(dp[aIndex-1][bIndex], dp[aIndex][bIndex-1]);
            if (a[aIndex-1] == b[bIndex-1]) 
                dp[aIndex][bIndex] = std::max(dp[aIndex][bIndex], 1 + dp[aIndex-1][bIndex-1]);
        }
    }
    
    std::cout << dp[n][m] << std::endl;
}