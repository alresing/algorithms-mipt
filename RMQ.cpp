#include <iostream>
#include <vector>
#include <set>

#define SIZE_OF_TREE 2147483647

class SparseTable {
public:
    SparseTable(std::vector<int64_t>& a) {
        std::vector<std::pair<std::pair<int64_t,int64_t>,std::pair<int64_t,int64_t>>> v1;
        sparse.push_back(v1);
        for (uint64_t i = 0; i < a.size(); ++i) 
            sparse[0].push_back({{a[i], i}, {SIZE_OF_TREE, -1}});
        uint64_t TwoInR = 2;
        for (int64_t r = 1; TwoInR < (a.size() + 1); ++r) {
            std::vector<std::pair<std::pair<int64_t,int64_t>,std::pair<int64_t,int64_t>>> vr;
            sparse.push_back(vr);
            for (uint64_t i = 0; i < (a.size()-TwoInR+1); ++i) {
                sparse[r].push_back({{0,0}, {0,0}});
                std::set<std::pair<int64_t,int64_t>> s;
                int64_t j = std::min(static_cast<uint64_t>(sparse[r-1].size()-1), i+(TwoInR/2));
                s.insert(sparse[r-1][i].first);
                s.insert(sparse[r-1][i].second);
                s.insert(sparse[r-1][j].first);
                s.insert(sparse[r-1][j].second);
                sparse[r][i] = {*(s.begin()), *next(s.begin())};
            }
            TwoInR *= 2;
        }

        k.push_back(0);
        TwoInK.push_back(0);

        int64_t nowk = 0;
        int64_t nowTwoInK = 1;
        for (uint64_t i = 1; i < a.size(); ++i) {
            if (((i&(i-1)) == 0) && (i != 1)) {
                ++nowk;
                nowTwoInK *= 2;
            }
            k.push_back(nowk);
            TwoInK.push_back(nowTwoInK);
        }
    }

    int64_t get(int64_t l, int64_t r) {
        std::set<std::pair<int64_t,int64_t>> s;
        s.insert(sparse[k[r-l]][l].first);
        s.insert(sparse[k[r-l]][l].second);
        s.insert(sparse[k[r-l]][r-TwoInK[r-l]+1].first);
        s.insert(sparse[k[r-l]][r-TwoInK[r-l]+1].second);
        return (*(next(s.begin()))).first;
    }

private:
    std::vector<int64_t> k;
    std::vector<int64_t> TwoInK;
    std::vector<std::vector<std::pair<std::pair<int64_t,int64_t>,std::pair<int64_t,int64_t>>>> sparse;
};

// идея: вторая статистика в объединении -- вторая среди двух пар минимумов каждого
int main() {
    int64_t N, M;
    std::cin >> N >> M;

    std::vector<int64_t> v;
    for (int64_t i = 0; i < N; ++i) {
        int64_t p; std::cin >> p;
        v.push_back(p);
    }

    SparseTable table(v);

    for (int64_t i = 0; i < M; ++i) {
        int64_t l, r;
        std::cin >> l >> r;
        std::cout << table.get(l-1, r-1) << std::endl;
    }
}