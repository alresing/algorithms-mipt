#include <iostream>
#include <vector>
#include <set>

const int INF = 1073741824;

int main() {
    int n, m; std::cin >> n >> m;
    std::vector<int> v;
    for (int i = 0; i < n; ++i) {
        int temp; std::cin >> temp;
        v.push_back(temp);
    }

    std::vector<std::vector<std::pair<int, std::set<int>>>> dp;
    for (int i = 0; i < m; ++i) {
        std::vector<std::pair<int, std::set<int>>> temp;
        for (int j = 0; j < n; ++j) {
            std::set<int> s;
            temp.push_back({INF, s});
        }
        dp.push_back(temp);
    }

    for (int i = 0; i < m; ++i) {
        for (int j = 0; (j <= i) && (j < n); ++j) {
            for (int k = 0; k < j; ++k)
                dp[i][j].second.insert(v[k]);
            dp[i][j].first = 0;
        }
    }

    for (int i = 0; i < m; ++i) {
        for (int j = i+1; j < n; ++j) {
            int minSum = INF;
            std::set<int> minSet;
            if (i > 0) {
                for (int k = 0; k < j; ++k) {
                    int sum = dp[i-1][k].first;
                        for (int p = k+1; p < j; ++p)
                            sum += std::min(v[j]-v[p], v[p]-v[k]);
                    if (sum < minSum) {
                        minSum = sum;
                        minSet = dp[i-1][k].second;
                    }
                }
            } else {
                minSum = 0;
                for (int k = 0; k < j; ++k) {
                    minSum += v[j]-v[k];
                }
            }
            minSet.insert(v[j]);
            dp[i][j].first = minSum;
            dp[i][j].second = minSet;
        }
    }
    
    for (int j = 0; j < n; ++j) {
        for (int k = j+1; k < n; ++k) {
            dp[m-1][j].first += v[k]-v[j];
        }
    }

    int res = 0;
    for (int j = 0; j < n; ++j)
        if (dp[m-1][j].first < dp[m-1][res].first)
            res = j;

    std::cout << dp[m-1][res].first << std::endl;

    for (int k : dp[m-1][res].second)
        std::cout << k << " ";
    std::cout << std::endl;
}