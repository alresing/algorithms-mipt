#include <iostream>
#include <vector>


int64_t dp(int64_t n, int64_t m,
            const std::vector<int64_t>& a,
            const std::vector<int64_t>& b) {

    std::vector<std::vector<int64_t>> dp;
    for (int64_t i = 0; i <= n; ++i) {
        std::vector<int64_t> temp(m+1, 0);
        dp.push_back(temp);
    }

    for (int64_t i = 1; i <= n; ++i) {
        int64_t last = 0;
        for (int64_t j = 1; j <= m; ++j) {
            if ((a[i-1] == b[j-1]) && (dp[i-1][j] < last + 1)) {
                dp[i][j] = last + 1;
            } else {
                dp[i][j] = dp[i-1][j];
            }

            if ((a[i-1] > b[j-1]) && (dp[i-1][j] > last))
                last = dp[i-1][j];
        }
    }
    
    int64_t res = 0;
    for (int64_t j = 0; j <= m; ++j) {
        res = std::max(res, dp[n][j]);
    }

    return res;
}


int main() {
    int64_t n, m; std::cin >> n >> m;
    std::vector<int64_t> a;
    for (int64_t i = 0; i < n; ++i) {
        int64_t temp; std::cin >> temp;
        a.push_back(temp);
    }
    std::vector<int64_t> b;
    for (int64_t i = 0; i < m; ++i) {
        int64_t temp; std::cin >> temp;
        b.push_back(temp);
    }

    std::cout << dp(n, m, a, b) << std::endl;
}