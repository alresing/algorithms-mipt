#include <iostream>
#include <stdio.h>
#include <set>
#include <vector>
#include <map>
#include <deque>

int main() {
    int64_t n, k, P;
    scanf("%lld%lld%lld", &n, &k, &P);
    
    std::vector<int64_t> v;
    std::map<int64_t,std::deque<int64_t>> m;

    for (int64_t i = 0; i < P; ++i) {
        int64_t t; scanf("%lld", &t);
        v.push_back(t);
        if (m.count(t) == 0) {
            std::deque<int64_t> vi;
            vi.push_back(i);
            m[t] = vi;
        } else {
            m[t].push_back(i);
        }
    }

    int64_t res = 0;
    std::set<std::pair<int64_t, int64_t>> floor;

    int64_t index = 0;

    while (index < P) {
        int64_t t = v[index];
        
        m[t].pop_front();
        if (floor.count({index, t}) == 1) {
            floor.erase({index, t});
            floor.insert({ (m[t].size() == 0) ? 5000000000000 : m[t].front(), t});
        } else {
            if (floor.size() >= k) {
                floor.erase(prev(floor.end()));
            }
            floor.insert({ (m[t].size() == 0) ? 5000000000000 : m[t].front(), t});
            ++res;
        }

        ++index;
    }

    printf("%lld", res);
}