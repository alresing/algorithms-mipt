#include <iostream>
#include <vector>


enum Color {
    WHITE,
    GRAY,
    BLACK
};


class Graph {
public:
    Graph(const std::vector<int>& ancestors)
                        : graph(ancestors.size()), ancestors(ancestors) {
        for (size_t i = 0; i < ancestors.size(); ++i)
            if (ancestors[i] != -1)
                graph[ancestors[i]].push_back(i);
    }

    const std::vector<int>& get(int v) const {
        return graph[v];
    }

    const std::vector<int>& anc() const {
        return ancestors;
    }

private:
    std::vector<std::vector<int>> graph;
    std::vector<int> ancestors;
};


void dfs(const Graph& graph, int v, int& timer, std::vector<int>& tin, std::vector<int>& tout) {
    tin[v] = timer++;
    for (int to : graph.get(v))
        dfs(graph, to, timer, tin, tout);
    tout[v] = timer++;
}


std::pair<std::vector<int>, std::vector<int>> dfs(const Graph& graph) {
    std::vector<int> tin;
    std::vector<int> tout;
    int root = 0;
    for (size_t i = 0; i < graph.anc().size(); ++i) {
        if (graph.anc()[i] == -1)
            root = i;
        tin.push_back(-1);
        tout.push_back(-1);
    }

    std::vector<Color> color(graph.anc().size(), WHITE);
    int timer = 0;

    dfs(graph, root, timer, tin, tout);

    return {tin, tout};
}

bool isAncestors(const std::vector<int>& tin, const std::vector<int>& tout, int a, int b) {
    return ((tin[a] < tin[b]) && (tout[b] < tout[a]));
}

int main() {
    int n; std::cin >> n;
    std::vector<int> ancestors(n, 0);
    for (int i = 0; i < n; ++i) {
        std::cin >> ancestors[i];
        --ancestors[i];
    }

    Graph graph(ancestors);

    std::pair<std::vector<int>, std::vector<int>> time = dfs(graph);

    int m; std::cin >> m;
    for (int useless = 0; useless < m; ++useless) {
        int a, b; std::cin >> a >> b;
        std::cout << isAncestors(time.first, time.second, a-1, b-1) << std::endl;
    }
}
