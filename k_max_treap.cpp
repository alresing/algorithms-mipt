#include <iostream>

struct Node {
    int64_t size = 1;                               // size of subtree
    int64_t x = 0;                                  // for tree
    int64_t y = rand();                             // for heap
    Node* left = nullptr;
    Node* right = nullptr;
};

class Treap {
public:
    ~Treap() {
        while (head != nullptr) del(head->x);
    }

    void add(int x) {
        Node* node = new Node;
        node->x = x;
        if (head == nullptr) {
            head = node;
        } else {
            std::pair<Node*, Node*> p = split(head, x);
            head = merge(merge(p.first, node), p.second);
        }
    }

    void del(int x) {
        if (!contains(x)) return;
        std::pair<Node*, Node*> p = split(head, x);

        if (p.first->right == nullptr) {
            Node* to_del = p.first;
            p.first = p.first->left;
            delete to_del;
        } else {
            Node* vertex = p.first;
            while (vertex->right->right != nullptr) {
                --vertex->size;
                vertex = vertex->right;
            }
            --vertex->size;
            Node* to_del = vertex->right;
            vertex->right = vertex->right->left;
            delete to_del;
        }

        head = merge(p.first, p.second);
    }

    int find(int index) const {
        if (head == nullptr || head->size <= index) return -1;
        return _find(head, index);
    }

    int size() const {
        if (head == nullptr) return 0;
        return head->size;
    }

    void print() const {
        if (head != nullptr) _print(head);
    }

private:
    void _print(Node* vertex) const {
        std::cout << vertex->x << " size: " << vertex->size << " ch: ";
        std::cout << (vertex->left == nullptr ? -1 : vertex->left->x) << " ";
        std::cout << (vertex->right == nullptr ? -1 : vertex->right->x) << " ";
        std::cout << std::endl;
        if (vertex->left != nullptr) _print(vertex->left);
        if (vertex->right != nullptr) _print(vertex->right);
    }

    int _find(Node* vertex, int index) const {
        if (vertex->left == nullptr && index == 0) {
            return vertex->x;
        } else if (vertex->left == nullptr && index > 0) {
            return _find(vertex->right, index-1);
        } else if (vertex->left != nullptr && index < vertex->left->size) {
            return _find(vertex->left, index);
        } else if (vertex->left != nullptr && index == vertex->left->size) {
            return vertex->x;
        }   // vertex->left != nullptr && x > vertex->left->size
        return _find(vertex->right, index-1-vertex->left->size);
    }

    bool contains(int x) const {
        Node* vertex = head;
        while (true) {
            if (vertex == nullptr) return false;
            if (vertex->x == x)  return true;
            if (vertex->x < x)   vertex = vertex->right;
            else                 vertex = vertex->left;
        }
    }

    Node* merge(Node* left, Node* right) {
        if (left == nullptr) return right;
        if (right == nullptr) return left;

        if (left->y < right->y) {
            left->size += right->size;
            left->right = merge(left->right, right);
            return left;
        } else {
            right->size += left->size;
            right->left = merge(left, right->left);
            return right;
        }
    }

    // left: <=x ; right >x
    std::pair<Node*, Node*> split(Node* node, int64_t x) {
        if (node == nullptr) return {nullptr, nullptr};
        if (node->x <= x) {
            node->size -= (node->right == nullptr) ? 0 : node->right->size;
            std::pair<Node*, Node*> p = split(node->right, x);
            node->right = p.first;
            node->size += (node->right == nullptr) ? 0 : node->right->size;
            return {node, p.second};
        } else {
            node->size -= (node->left == nullptr) ? 0 : node->left->size;
            std::pair<Node*, Node*> p = split(node->left, x);
            node->left = p.second;
            node->size += (node->left == nullptr) ? 0 : node->left->size;
            return {p.first, node};
        }
    }

    Node* head = nullptr;
};


int main() {
    Treap tree;

    int n; std::cin >> n;
    for (int useless = 0; useless < n; ++useless) {
        int c, k; std::cin >> c >> k;
        if (c == 1) {
            tree.add(k);
        } else if (c == 0) {
            std::cout << tree.find(tree.size()-k) << std::endl;
        } else if (c == -1) {
            tree.del(k);
        }
    }
}