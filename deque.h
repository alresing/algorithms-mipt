#pragma once

#include <vector>
#include <stdexcept>


template <typename T>
class Deque {
public:
    struct iterator {
    public:
        iterator(int i, int j, Deque<T>* deque);
        iterator& operator++();
        iterator operator++(int);
        iterator& operator--();
        iterator operator--(int);
        iterator operator+(const int& a) const;
        iterator operator-(const int& a) const;
        iterator& operator+=(const int& a);
        iterator& operator-=(const int& a);
        int operator-(const iterator& it) const;
        bool operator==(const iterator& it) const;
        bool operator!=(const iterator& it) const;
        bool operator<(const iterator& it) const;
        bool operator>(const iterator& it) const;
        bool operator>=(const iterator& it) const;
        bool operator<=(const iterator& it) const;
        T& operator*();
        T* operator->();

    private:
        int i = 0;
        int j = 0;
        Deque<T>* deque;
    };

    struct const_iterator {
    public:
        const_iterator(int i, int j, const Deque<T>* deque);
        const_iterator(const iterator& it);
        const_iterator& operator++();
        const_iterator operator++(int);
        const_iterator& operator--();
        const_iterator operator--(int);
        const_iterator operator+(const int& a) const;
        const_iterator operator-(const int& a) const;
        const_iterator& operator+=(const int& a);
        const_iterator& operator-=(const int& a);
        int operator-(const const_iterator& it) const;
        bool operator==(const const_iterator& it) const;
        bool operator!=(const const_iterator& it) const;
        bool operator<(const const_iterator& it) const;
        bool operator>(const const_iterator& it) const;
        bool operator>=(const const_iterator& it) const;
        bool operator<=(const const_iterator& it) const;
        const T& operator*() const;
        const T* operator->() const;

    private:
        int i = 0;
        int j = 0;
        const Deque<T>* deque;
    };

    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    Deque();
    Deque(const Deque<T>& other);
    Deque(const int& count);
    Deque(const int& count, const T& element);
    Deque& operator=(const Deque& other);
    size_t size() const;
    T& operator[](size_t index);
    const T& operator[](size_t index) const;
    T& at(size_t index);
    const T& at(size_t index) const;
    void push_back(const T& element);
    void pop_back();
    void push_front(const T& element);
    void pop_front();
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;
    void insert(typename Deque<T>::iterator it, const T& element);
    void erase(typename Deque<T>::iterator it);
    ~Deque();

private:
    std::vector<T*> data;
    size_t front_i = 1;
    size_t front_j = 0;
    size_t back_i = 0;
    size_t back_j = bucket_size-1;
    static const size_t bucket_size = 50;   // at least 1
};

template <typename T>
Deque<T>::Deque() {
    data.push_back(nullptr);
    data.push_back(nullptr);
}

template <typename T>
Deque<T>::Deque(const Deque<T>& other) {
    front_i = other.front_i;
    front_j = other.front_j;
    back_i = other.back_i;
    back_j = other.back_j;

    size_t i = 0;
    size_t j = 0;
    try {
        for (; i < other.data.size(); ++i) {
            if (other.data[i] == nullptr) {
                data.push_back(nullptr);
            } else {
                T* row = reinterpret_cast<T*>(new int8_t[bucket_size * sizeof(T)]);
                for (; j < bucket_size; ++j)
                    if (((i != front_i) || (j >= front_j)) && ((i != back_i) || (j <= back_j)))
                        new(row + j) T(other.data[i][j]);
                data.push_back(row);
            }
        }
    } catch (...) {
        for (size_t di = 0; di <= i; ++di) {
            if (data[di] == nullptr) continue;
            if (di < i) {
                for (size_t dj = 0; dj < bucket_size; ++dj)
                    (data[di] + dj)->~T();
                delete[] reinterpret_cast<int8_t*>(data[i]);
            } else {
                for (size_t dj = 0; dj < j; ++dj)
                    (data[di] + dj)->~T();
                delete[] reinterpret_cast<int8_t*>(data[i]);
            }
        }
        throw;
    }
}

template <typename T>
Deque<T>::Deque(const int& count) {
    data.push_back(nullptr);
    data.push_back(nullptr);

    for (int i = 0; i < count; ++i) {
        T temp;
        push_back(temp);
    }
}

template <typename T>
Deque<T>::Deque(const int& count, const T& element) {
    data.push_back(nullptr);
    data.push_back(nullptr);

    for (int i = 0; i < count; ++i)
        push_back(element);
}

template <typename T>
Deque<T>& Deque<T>::operator=(const Deque& other) {
    if (this == &other) {
        return *this;
    }

    for (size_t i = 0; i < data.size(); ++i) {
        if (data[i] != nullptr) {
            for (size_t j = 0; j < bucket_size; ++j)
                if (((i != front_i) || (j >= front_j)) && ((i != back_i) || (j <= back_j)))
                    data[i][j].~T();
            delete[] reinterpret_cast<int8_t*>(data[i]);
        }
    }
    data.clear();

    front_i = other.front_i;
    front_j = other.front_j;
    back_i = other.back_i;
    back_j = other.back_j;    

    size_t i = 0;
    size_t j = 0;
    try {
        for (; i < other.data.size(); ++i) {
            if (other.data[i] == nullptr) {
                data.push_back(nullptr);
            } else {
                T* row = reinterpret_cast<T*>(new int8_t[bucket_size * sizeof(T)]);
                for (; j < bucket_size; ++j)
                    if (((i != front_i) || (j >= front_j)) && ((i != back_i) || (j <= back_j)))
                        new(row + j) T(other.data[i][j]);
                data.push_back(row);
            }
        }
    } catch (...) {
        for (size_t di = 0; di <= i; ++di) {
            if (data[di] == nullptr) continue;
            if (di < i) {
                for (size_t dj = 0; dj < bucket_size; ++dj)
                    (data[di] + dj)->~T();
                delete[] reinterpret_cast<int8_t*>(data[i]);
            } else {
                for (size_t dj = 0; dj < j; ++dj)
                    (data[di] + dj)->~T();
                delete[] reinterpret_cast<int8_t*>(data[i]);
            }
        }
        throw;
    }

    return *this;
}

template <typename T>
Deque<T>::~Deque() {
    for (size_t i = 0; i < data.size(); ++i) {
        if ((data[i] != nullptr) && (i >= front_i) && (i <= back_i)) {
            for (size_t j = 0; j < bucket_size; ++j)
                if (((i > front_i) || (j >= front_j)) && ((i < back_i) || (j <= back_j)))
                    data[i][j].~T();
            delete[] reinterpret_cast<int8_t*>(data[i]);
        }
    }
}

template <typename T>
size_t Deque<T>::size() const {
    return bucket_size*(back_i - front_i) + back_j - front_j + 1;

}

template <typename T>
T& Deque<T>::operator[](size_t index) {
    size_t new_j = (front_j + index) % bucket_size;
    size_t new_i = front_i + ((front_j + index) / bucket_size);
    return data[new_i][new_j];
}

template <typename T>
const T& Deque<T>::operator[](size_t index) const {
    size_t new_j = (front_j + index) % bucket_size;
    size_t new_i = front_i + ((front_j + index) / bucket_size);
    return data[new_i][new_j];
}

template <typename T>
T& Deque<T>::at(size_t index) {
    if ((index >= 0) && (index < size()))
        return (*this)[index];
    else {
        throw std::out_of_range("Index out range in deque");
    }
}

template <typename T>
const T& Deque<T>::at(size_t index) const {
    if ((index >= 0) && (index < size()))
        return (*this)[index];
    else {
        throw std::out_of_range("Index out range in deque");
    }
}

template <typename T>
void Deque<T>::push_back(const T& element) {
    if (back_j < (bucket_size-1)) {
        ++back_j;
        new(data[back_i] + back_j) T(element);
    } else if (back_i < (data.size()-1)) {
        ++back_i;
        data[back_i] = reinterpret_cast<T*>(new int8_t[bucket_size * sizeof(T)]);
        try {
            new(data[back_i] + 0) T(element);
        } catch (...) {
            delete[] reinterpret_cast<int8_t*>(data[back_i]);
            --back_i;
            throw;
        }
        back_j = 0;
    } else {
        std::vector<T*> new_data(data.size() * 4, nullptr);
        for (size_t i = 0; i < data.size(); ++i)
            new_data[i + (new_data.size()/4)] = data[i];
        back_i += new_data.size()/4;
        front_i += new_data.size()/4;
        data = new_data;

        if (back_j < (bucket_size-1)) {
            ++back_j;
            new(data[back_i] + back_j) T(element);
        } else if (back_i < (data.size()-1)) {
            ++back_i;
            data[back_i] = reinterpret_cast<T*>(new int8_t[bucket_size * sizeof(T)]);
            try {
                new(data[back_i] + 0) T(element);
            } catch (...) {
                delete[] reinterpret_cast<int8_t*>(data[back_i]);
                --back_i;
                throw;
            }
            back_j = 0;
        }
    }
}

template <typename T>
void Deque<T>::pop_back() {
    data[back_i][back_j].~T();
    if (back_j == 0) {
        delete[] reinterpret_cast<int8_t*>(data[back_i]);
        --back_i;
        back_j = bucket_size-1;
    } else {
        --back_j;
    }
}

template <typename T>
void Deque<T>::push_front(const T& element) {
    if (front_j > 0) {
        --front_j;
        new(data[front_i] + front_j) T(element);
    } else if (front_i > 0) {
        --front_i;
        data[front_i] = reinterpret_cast<T*>(new int8_t[bucket_size * sizeof(T)]);
        try {
            new(data[front_i] + bucket_size - 1) T(element);
        } catch (...) {
            delete[] reinterpret_cast<int8_t*>(data[front_i]);
            ++front_i;
            throw;
        }
        front_j = bucket_size - 1;
    } else {
        std::vector<T*> new_data(data.size() * 4, nullptr);
        for (size_t i = 0; i < data.size(); ++i)
            new_data[i + (new_data.size()/4)] = data[i];
        back_i += new_data.size()/4;
        front_i += new_data.size()/4;
        data = new_data;

        if (front_j > 0) {
            --front_j;
            new(data[front_i] + front_j) T(element);
        } else if (front_i > 0) {
            --front_i;
            data[front_i] = reinterpret_cast<T*>(new int8_t[bucket_size * sizeof(T)]);
            try {
                new(data[front_i] + bucket_size - 1) T(element);
            } catch (...) {
                delete[] reinterpret_cast<int8_t*>(data[front_i]);
                ++front_i;
                throw;
            }
            front_j = bucket_size - 1;
        }
    }
}

template <typename T>
void Deque<T>::pop_front() {
    data[front_i][front_j].~T();
    if (front_j == (bucket_size-1)) {
        delete[] reinterpret_cast<int8_t*>(data[front_i]);
        ++front_i;
        front_j = 0;
    } else {
        ++front_j;
    }
}

template <typename T>
typename Deque<T>::iterator Deque<T>::begin() {
    return iterator(front_i, front_j, this);
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::begin() const {
    return const_iterator(front_i, front_j, this);
}

template <typename T>
typename Deque<T>::iterator Deque<T>::end() {
    return iterator(back_i, back_j, this) + 1;
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::end() const {
    return const_iterator(back_i, back_j, this) + 1;
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::cbegin() const {
    return const_iterator(front_i, front_j, this);
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::cend() const {
    return const_iterator(back_i, back_j, this) + 1;
}

template <typename T>
typename Deque<T>::reverse_iterator Deque<T>::rbegin() {
    return reverse_iterator(front_i, front_j, this);
}

template <typename T>
typename Deque<T>::reverse_iterator Deque<T>::rend() {
    return reverse_iterator(back_i, back_j, this);
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crbegin() const {
    return const_reverse_iterator(front_i, front_j, this);
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crend() const {
    return const_reverse_iterator(back_i, back_j, this);
}

template <typename T>
void Deque<T>::insert(Deque<T>::iterator it, const T& element) {
    if (it != end()) {
        insert(it+1, *it);
        (*it).~T();
        new(&(*it)) T(element);
    } else {
        push_back(element);
    }
}

template <typename T>
void Deque<T>::erase(Deque<T>::iterator it) {
    if (it != (end()-1)) {
        (*it).~T();
        new(&(*it)) T(*(it+1));
        erase(it + 1);
    } else {
        pop_back();
    }
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

template <typename T>
Deque<T>::iterator::iterator(int i, int j, Deque<T>* deque)
                                    : i(i), j(j), deque(deque) {}

template <typename T>
typename Deque<T>::iterator& Deque<T>::iterator::operator++() {
    *this += 1;
    return *this;
}

template <typename T>
typename Deque<T>::iterator Deque<T>::iterator::operator++(int) {
    Deque<T>::iterator copy = *this;
    ++*this;
    return copy;
}

template <typename T>
typename Deque<T>::iterator& Deque<T>::iterator::operator--() {
    *this -= 1;
    return *this;
}

template <typename T>
typename Deque<T>::iterator Deque<T>::iterator::operator--(int) {
    Deque<T>::iterator copy = *this;
    --*this;
    return copy;
}

template <typename T>
typename Deque<T>::iterator Deque<T>::iterator::operator+(const int& a) const {
    Deque<T>::iterator copy = *this;
    copy += a;
    return copy;
}

template <typename T>
typename Deque<T>::iterator Deque<T>::iterator::operator-(const int& a) const {
    Deque<T>::iterator copy = *this;
    copy -= a;
    return copy;
}

template <typename T>
int Deque<T>::iterator::operator-(const iterator& it) const {
    int pos = j + i * bucket_size;
    pos -= it.i*bucket_size + it.j;
    return pos;
}

template <typename T>
typename Deque<T>::iterator& Deque<T>::iterator::operator+=(const int& a) {
    int pos = j + i * bucket_size;
    pos += a;
    i = pos / bucket_size;
    j = pos % bucket_size;
    return *this;
}

template <typename T>
typename Deque<T>::iterator& Deque<T>::iterator::operator-=(const int& a) {
    int pos = j + i * bucket_size;
    pos -= a;
    i = pos / bucket_size;
    j = pos % bucket_size;
    return *this;
}

template <typename T>
bool Deque<T>::iterator::operator==(const iterator& it) const {
    return ((i == it.i) && (j == it.j)); 
}

template <typename T>
bool Deque<T>::iterator::operator!=(const iterator& it) const {
    return !(*this == it);
}

template <typename T>
bool Deque<T>::iterator::operator<(const iterator& it) const {
    if (i < it.i) return true;
    if ((i == it.i) && (j < it.j)) return true;
    return false;
}

template <typename T>
bool Deque<T>::iterator::operator>(const iterator& it) const {
    return (it < *this);
}

template <typename T>
bool Deque<T>::iterator::operator<=(const iterator& it) const {
    return ((*this == it) || (*this < it));
}

template <typename T>
bool Deque<T>::iterator::operator>=(const iterator& it) const {
    return ((*this == it) || (*this > it));
}

template <typename T>
T& Deque<T>::iterator::operator*() {
    return deque->data[i][j];
}

template <typename T>
T* Deque<T>::iterator::operator->() {
    return deque->data[i] + j;
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

template <typename T>
Deque<T>::const_iterator::const_iterator(int i, int j, const Deque<T>* deque)
                                    : i(i), j(j), deque(deque) {}

template <typename T>
Deque<T>::const_iterator::const_iterator(const iterator& it)
                                    : i(it.i), j(it.j), deque(it.deque) {}

template <typename T>
typename Deque<T>::const_iterator& Deque<T>::const_iterator::operator++() {
    *this += 1;
    return *this;
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::const_iterator::operator++(int) {
    Deque<T>::const_iterator copy = *this;
    ++*this;
    return copy;
}

template <typename T>
typename Deque<T>::const_iterator& Deque<T>::const_iterator::operator--() {
    *this -= 1;
    return *this;
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::const_iterator::operator--(int) {
    Deque<T>::const_iterator copy = *this;
    --*this;
    return copy;
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::const_iterator::operator+(const int& a) const {
    Deque<T>::const_iterator copy = *this;
    copy += a;
    return copy;
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::const_iterator::operator-(const int& a) const {
    Deque<T>::const_iterator copy = *this;
    copy -= a;
    return copy;
}

template <typename T>
int Deque<T>::const_iterator::operator-(const const_iterator& it) const {
    int pos = j + i * bucket_size;
    pos -= it.i*bucket_size + it.j;
    return pos;
}

template <typename T>
typename Deque<T>::const_iterator& Deque<T>::const_iterator::operator+=(const int& a) {
    int pos = j + i * bucket_size;
    pos += a;
    i = pos / bucket_size;
    j = pos % bucket_size;
    return *this;
}

template <typename T>
typename Deque<T>::const_iterator& Deque<T>::const_iterator::operator-=(const int& a) {
    int pos = j + i * bucket_size;
    pos -= a;
    i = pos / bucket_size;
    j = pos % bucket_size;
    return *this;
}

template <typename T>
bool Deque<T>::const_iterator::operator==(const const_iterator& it) const {
    return ((i == it.i) && (j == it.j)); 
}

template <typename T>
bool Deque<T>::const_iterator::operator!=(const const_iterator& it) const {
    return !(*this == it);
}

template <typename T>
bool Deque<T>::const_iterator::operator<(const const_iterator& it) const {
    if (i < it.i) return true;
    if ((i == it.i) && (j < it.j)) return true;
    return false;
}

template <typename T>
bool Deque<T>::const_iterator::operator>(const const_iterator& it) const {
    return (it < *this);
}

template <typename T>
bool Deque<T>::const_iterator::operator<=(const const_iterator& it) const {
    return ((*this == it) || (*this < it));
}

template <typename T>
bool Deque<T>::const_iterator::operator>=(const const_iterator& it) const {
    return ((*this == it) || (*this > it));
}

template <typename T>
const T& Deque<T>::const_iterator::operator*() const {
    return deque->data[i][j];
}

template <typename T>
const T* Deque<T>::const_iterator::operator->() const {
    return deque->data[i] + j;
}
