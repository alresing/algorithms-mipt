#include <iostream>
#include <vector>


struct Edge {
    int64_t to;
    long double rate;
    long double com;
};
 
 
void dfs(int64_t v, const std::vector<std::vector<Edge>>& graph, std::vector<bool>& used) {
    if (used[v])
        return;
    used[v] = true;
    for (const auto& edge: graph[v])
        dfs(edge.to, graph, used);
}
 

long double calc(long double value, long double rate, long double commision) {
    return (value - commision) * rate;
}


bool ford(const std::vector<std::vector<Edge>>& graph, int64_t s, long double v) {
    std::vector<long double> dist(graph.size(), 0);
    dist[s] = v;
    for (int64_t _ = 0; _ < graph.size(); ++_)
        for (int64_t v = 0; v < graph.size(); ++v)
            for (const auto& edge: graph[v])
                dist[edge.to] = std::max(dist[edge.to], calc(dist[v], edge.rate, edge.com));
 
    std::vector<long double> old_dist = dist;
 
    for (int64_t v = 0; v < graph.size(); ++v)
        for (const auto& edge: graph[v])
            dist[edge.to] = std::max(dist[edge.to], calc(dist[v], edge.rate, edge.com));
 
    std::vector<bool> used(graph.size(), false);
    for (int64_t v = 0; v < graph.size(); ++v)
        if (dist[v] > old_dist[v])
            dfs(v, graph, used);

    return used[s];
}


int main() {
    int64_t n, m, s;
    long double v;
    std::cin >> n >> m >> s >> v;
    std::vector<std::vector<Edge>> graph(n);
    for (int64_t i = 0; i < m; ++i) {
        int64_t a, b;
        long double rab, cab, rba, cba;
        std::cin >> a >> b >> rab >> cab >> rba >> cba;
        graph[a-1].push_back({b-1, rab, cab});
        graph[b-1].push_back({a-1, rba, cba});
    }

    if (ford(graph, s-1, v))
        std::cout << "YES" << std::endl;
    else
        std::cout << "NO" << std::endl;
}
