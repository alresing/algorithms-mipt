#include <iostream>
#include <vector>

int main() {
    std::vector<int> v;
    int s, n; std::cin >> s >> n;
    for (int i = 0; i < n; ++i) {
        int p; std::cin >> p;
        v.push_back(p);
    }

    // dp[index][weight]
    std::vector<std::vector<int>> dp;
    for (int i = 0; i <= n; ++i) {
        std::vector<int> temp;
        for (int j = 0; j <= s; ++j)
            temp.push_back(0);
        dp.push_back(temp);
    }
    for (int index = 0; index < n; ++index) dp[index][0] = 0;
    for (int weight = 0; weight < s; ++weight) dp[0][weight] = 0;

    for (int index = 1; index <= n; ++index) {
        for (int weight = 1; weight <= s; ++weight) {
            if (weight >= v[index-1]) {
                dp[index][weight] = std::max(dp[index-1][weight], dp[index-1][weight-v[index-1]] + v[index-1]);
            } else {
                dp[index][weight] = dp[index-1][weight];
            }
        }
    }

    std::cout << dp[n][s] << std::endl;
}