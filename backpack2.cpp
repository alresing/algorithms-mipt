#include <iostream>
#include <vector>
#include <algorithm>

int64_t pow2(int64_t x) {
    int64_t pow = 0;
    int64_t now = 1;
    while (pow < x) {
        pow += 1;
        now *= 2;
    }
    return now;
}

std::vector<int64_t> getAllSums(std::vector<int64_t>& weights, int64_t maxSum) {
    std::vector<int64_t> res;
    int64_t pow2size = pow2(weights.size());
    for (int64_t mask = 0; mask < pow2size; ++mask) {
        res.push_back(0);
        for (int64_t i = 0; i < weights.size(); ++i) {
            if ((mask >> i)%2 == 1) {
                res[res.size()-1] += weights[i];
            }
            if (res[res.size()-1] > maxSum) break;
        }
        /*
        if (flag) {
            for (const auto& i : res) std::cout << i << " ";
            std::cout << std::endl;
        }
        */
        if (res[res.size()-1] > maxSum) {
            res.pop_back();
        }
    }

    std::sort(res.begin(), res.end());

    return res;
}

int main() {
    int64_t n; std::cin >> n;
    std::vector<int64_t> weights;
    for (int64_t i = 0; i < n; ++i) {
        int64_t temp; std::cin >> temp;
        weights.push_back(temp);
    }
    int64_t C; std::cin >> C;


    int64_t left_size = n/2;
    int64_t right_size = n - (n/2);

    std::vector<int64_t> left;
    for (int64_t i = 0; i < left_size; ++i) {
        left.push_back(weights[i]);
    }

    std::vector<int64_t> right;
    for (int64_t i = 0; i < right_size; ++i) {
        right.push_back(weights[i+left_size]);
    }

    std::vector<int64_t> leftSorted = getAllSums(left, C);
    std::vector<int64_t> rightSorted = getAllSums(right, C);


    int64_t res = 0;
    int64_t i = 0;
    int64_t j = rightSorted.size()-1;
    while ((i < leftSorted.size()) && (j >= 0)) {
        if (leftSorted[i] + rightSorted[j] <= C) {
            res += j+1;
            i += 1;
        } else {
            j -= 1;
        }
    }
    res += leftSorted.size()-i;

    std::cout << res << std::endl;
}