#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct Node Node;

struct Node {
    int val;
    Node* last;
};

typedef struct Stack Stack;

struct Stack {
    int size;
    Node* last;
};

int push(Stack* st, int n) {
    Node* newNode = malloc(sizeof(Node));
    if (newNode == NULL) return -1;

    newNode->val = n;

    if (st->size > 0) {
        newNode->last = st->last;
    } else {
        newNode->last = NULL;
    }
    st->last = newNode;
    st->size += 1;
    return 0;
}

int pop(Stack* st, int* res) {
    if (st->size == 0) return -1;

    *res = st->last->val;
    Node* wasLastNode = st->last;

    st->last = wasLastNode->last;
    st->size -= 1;

    free(wasLastNode);

    return 0;
}

int back(Stack* st, int* res) {
    if (st->size == 0) return -1;

    *res = st->last->val;
    return 0;
}

int size(Stack* st) {
    return st->size;
}

void clear(Stack* st) {
    while (st->size > 0) {
        int t;
        pop(st, &t);
    }
}

int main() {
    Stack st;
    st.size = 0;

    char str[5]; 
    
    while (1) {
        scanf("%s", str);

        if (strcmp(str, "push") == 0) {
            int t; scanf("%d", &t);
            push(&st, t);
            printf("ok\n");
        } else if (strcmp(str, "pop") == 0) {
            int t;
            if (pop(&st, &t) == 0) {
                printf("%d%s", t, "\n");
            } else {
                printf("error\n");
            }   
        } else if (strcmp(str, "back") == 0) {
            int t;
            if (back(&st, &t) == 0) {
                printf("%d%s", t, "\n");
            } else {
                printf("error\n");
            }
        } else if (strcmp(str, "size") == 0) {
            printf("%d%s", size(&st), "\n");
        } else if (strcmp(str, "clear") == 0) {
            clear(&st);
            printf("ok\n");
        } else if (strcmp(str, "exit") == 0) {
            clear(&st);
            printf("bye\n");
            break;
        }
    }

    return 0;
}