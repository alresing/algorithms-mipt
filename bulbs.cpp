#include <iostream>
#include <vector>
#include <algorithm>


class Graph {
public:
    Graph(std::vector<std::vector<int>>& bulbs, std::vector<bool>& isOn, 
                                            int n, int m) : n(n), m(m) {
        vertexCount = 2*m;
        builtGraph(bulbs, isOn);

        for (int i = 0; i < vertexCount; ++i) {
            std::vector<int> row;
            graphR.push_back(row);
        }
        for (int v = 0; v < vertexCount; ++v)
            for (int u : graph[v])
                graphR[u].push_back(v);

        for (int i = 0; i < vertexCount; ++i)
            tout.push_back({0, 0});
        std::vector<bool> used(vertexCount, false);
        int timer = 0;
        for (int i = 0; i < vertexCount; ++i)
            if (!used[i])
                dfs(i, used, timer);

        std::sort(tout.begin(), tout.end());
        for (int i = 0; i < vertexCount; ++i)
            used[i] = false;
        for (int i = 0; i < vertexCount; ++i)
            componentNumber.push_back(0);
        int number = 0;
        for (int i = vertexCount-1; i > -1; --i) {
            int v = tout[i].second;
            if (!used[v]) {
                dfsR(v, used, number);
                ++number;
            }
        }

        for (int i = 0; i < m; ++i) {
            if (componentNumber[2*i] == componentNumber[2*i+1]) {
                result = false;
                break;
            }
        }
    }   

    bool isPossible() {
        return result;
    }

private:
    std::vector<std::vector<int>> graph;
    std::vector<std::vector<int>> graphR;
    int n = 0;
    int m = 0;
    int vertexCount = 0;
    bool result = true;
    std::vector<int> componentNumber;
    std::vector<std::pair<int, int>> tout;

    void builtGraph(std::vector<std::vector<int>>& bulbs, std::vector<bool> isOn) {
        for (int i = 0; i < 2*m; ++i) {
            std::vector<int> temp;
            graph.push_back(temp);
        }

        for (int i = 0; i < n; ++i) {
            if (isOn[i]) {
                graph[bulbs[i][0]*2].push_back(bulbs[i][1]*2);
                graph[bulbs[i][1]*2].push_back(bulbs[i][0]*2);
                graph[bulbs[i][0]*2+1].push_back(bulbs[i][1]*2+1);
                graph[bulbs[i][1]*2+1].push_back(bulbs[i][0]*2+1);
            } else {
                graph[bulbs[i][0]*2].push_back(bulbs[i][1]*2+1);
                graph[bulbs[i][1]*2].push_back(bulbs[i][0]*2+1);
                graph[bulbs[i][0]*2+1].push_back(bulbs[i][1]*2);
                graph[bulbs[i][1]*2+1].push_back(bulbs[i][0]*2);
            }
        }
    }

    void dfs(int v, std::vector<bool>& used, int& timer) {
        used[v] = true;
        for (int to : graph[v])
            if (!used[to])
                dfs(to, used, timer);
        tout[v] = {timer++, v};
    }

    void dfsR(int v, std::vector<bool>& used, int number) {
        used[v] = true;
        componentNumber[v] = number;
        for (int to : graphR[v])
            if (!used[to])
                dfsR(to, used, number);
    }
};


int main() {
    int n, m; std::cin >> n >> m;
    std::vector<bool> r(n, false);
    for (int i = 0; i < n; ++i) {
        bool temp = false;
        std::cin >> temp;
        r[i] = temp;
    }

    std::vector<std::vector<int>> bulbs;
    for (int i = 0; i < n; ++i) {
        std::vector<int> bulb;
        bulbs.push_back(bulb);
    }
    for (int i = 0; i < m; ++i) {
        int p; std::cin >> p;
        for (int j = 0; j < p; ++j) {
            int x; std::cin >> x;
            bulbs[x-1].push_back(i);
        }
    }

    Graph graph(bulbs, r, n, m);

    if (graph.isPossible())
        std::cout << "YES" << std::endl;
    else
        std::cout << "NO" << std::endl;
}   
