#include <iostream>
#include "stdio.h"
#include <vector>
#include <map>

int64_t M = 1000000007;

int main() {
    int64_t n; scanf("%lld", &n);
    std::vector<int64_t> a;
    for (int64_t i = 0; i < n; ++i) {
        int64_t p; scanf("%lld", &p);
        a.push_back(p);
    }

    // dp[length][index]
    std::vector<int64_t> dp;
    dp.push_back(1);

    std::map<int64_t, int64_t> last;
    for (int64_t i = 0; i < n; ++i)
            last[a[i]] = -1;

    for (int64_t i = 1; i <= n; ++i) {
        dp.push_back((dp[i-1] * 2)%M);
        if (last[a[i-1]] != -1) dp[i] = (dp[i] - dp[last[a[i-1]]]) % M;
        last[a[i-1]] = i-1;
    }
    dp[n] = (dp[n] + M -1) % M;
    
    std::cout << dp[n] << std::endl;
}