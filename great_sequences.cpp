#include <iostream>
#include <vector>


const int64_t MODULE = 1'000'000'007;


void print(const std::vector<int64_t>& v) {
    for (const auto& i : v)
        std::cout << i << " ";
    std::cout << std::endl;
}


void print(const std::vector<std::vector<int64_t>>& m) {
    for (const auto& row : m)
        print(row);
    std::cout << std::endl;
}


void multVector(const std::vector<std::vector<int64_t>>& A, std::vector<int64_t>& v) {
    std::vector<int64_t> res(v.size(), 0);
    for (uint64_t i = 0; i < v.size(); ++i) {
        for (uint64_t j = 0; j < v.size(); ++j) {
            res[i] += A[i][j]*v[j];
            res[i] %= MODULE;
        }
    }
    v = res;
}


std::vector<std::vector<int64_t>> mult(const std::vector<std::vector<int64_t>>& left,
                                    const std::vector<std::vector<int64_t>>& right) {
    std::vector<std::vector<int64_t>> res;
    for (uint64_t i = 0; i < left.size(); ++i) {
        res.emplace_back();
        for (uint64_t j = 0; j < left.size(); ++j) {
            int64_t sum = 0;
            for (uint64_t k = 0; k < left.size(); ++k) {
                sum += left[i][k] * right[k][j];
                sum %= MODULE;
            }
            res[i].push_back(sum);
        }
    }
    return res;
}


std::vector<std::vector<int64_t>> e(int64_t n) {
    std::vector<std::vector<int64_t>> res;
    for (int64_t i = 0; i < n; ++i) {
        std::vector<int64_t> row(n, 0);
        row[i] = 1;
        res.push_back(row);
    }
    return res;
}


void binPow(std::vector<std::vector<int64_t>>& A, int64_t pow) {
    std::vector<std::vector<int64_t>> res = e(A.size());
    while (pow != 0) {
        if ((pow % 2) == 1) {
            res = mult(res, A);
        }
        A = mult(A, A);
        pow >>= 1;
    }
    A = res;
}


bool good(const int64_t& a, const int64_t& b) {
    int64_t x = a ^ b;
    int64_t count = 0;
    while (x != 0) {
        if ((x % 2) == 1)
            ++count;
        x >>= 1;
    }
    return ((count % 3) == 0);
}


std::vector<std::vector<int64_t>> getMatrix(const std::vector<int64_t>& a) {
    std::vector<std::vector<int64_t>> matrix;
    for (uint64_t i = 0; i < a.size(); ++i) {
        std::vector<int64_t> row(a.size(),0);
        for (uint64_t j = 0; j < a.size(); ++j) {
            if (good(a[i], a[j]))
                row[j] = 1;
        }
        matrix.push_back(row);
    }
    return matrix;
}


// возводить матрицу 100х100 в степень
// матрица это как меняется количество вариантов при переходе к последовательности длины k+1
int main() {
    int64_t n, k; std::cin >> n >> k;
    std::vector<int64_t> a(n, 0);
    for (int64_t i = 0; i < n; ++i)
        std::cin >> a[i];

    std::vector<int64_t> v(n, 1);

    std::vector<std::vector<int64_t>> matrix = getMatrix(a);

    binPow(matrix, k-1);

    multVector(matrix, v);

    int64_t res = 0;
    for (const auto& i : v) {
        res += i;
        res %= MODULE;
    }
    std::cout << res << std::endl;
}
