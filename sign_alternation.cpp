#include <iostream>
#include <vector>

class BinaryIndexedTree {
public:
    BinaryIndexedTree(std::vector<int>& a) {
        for (uint64_t i = 0; i < a.size(); ++i) {
            t.push_back(0);
            for (uint64_t k = f(i); k <= i; ++k) {
                t[i] += a[k];
            }
        }
    }

    void change(int pos, int val) {
        if ((pos%2) != 0) val *= -1;
        for (uint64_t i = pos; i < t.size(); i = g(i)) {
            t[i] += val;
        }
    }

    int getSum(int l, int r) const {
        int left = (l > 0) ? getSum(l-1) : 0;
        int right = getSum(r);
        if ((l%2) == 0) return right - left;
        else            return left - right;
    }

private:
    int getSum(int pos) const {
        int ans = 0;
        for(int i = pos; i >= 0; i = f(i)-1) {
            ans += t[i];
        }
        return ans;
    }

    static int f(int x) {
        return (x+1) & x;
    }

    static int g(int x) {
        return (x+1) | x;
    }

    std::vector<int> t;
};

int main() {
    int64_t n; std::cin >> n;
    std::vector<int> v;
    for (int i = 0; i < n; ++i) {
        int t; std::cin >> t;
        if ((i%2) == 0) v.push_back(t);
        else            v.push_back(-t);
    }

    BinaryIndexedTree tree(v);

    int m; std::cin >> m;
    for (int useless = 0; useless < m; ++useless) {
        int type; std::cin >> type;
        if (type == 0) {
            int i, j; std::cin >> i >> j;
            int old = tree.getSum(i-1,i-1);
            tree.change(i-1, j-old);
        } else {
            int l, r; std::cin >> l >> r;
            std::cout << tree.getSum(l-1, r-1) << std::endl;
        }
    }
}