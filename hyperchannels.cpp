#include <iostream>
#include <vector>
#include <map>


class Graph {
public:
    Graph(std::vector<std::vector<int>>& graph, int base) : 
                                        graph(graph), base(base) {}

    const std::vector<int>& get(int v) const {
        return graph[v];
    }

    int getBase() const {
        return base;
    }

    int size() const {
        return graph.size();
    }

private:
    std::vector<std::vector<int>> graph;
    int base = 0;
};


void dfs(int v, std::map<std::pair<int, int>, bool>& used,
            std::vector<int>& edges, const Graph& graph) {
    for (int to : graph.get(v)) {
        if (!used[{v, to}]) {
            used[{v, to}] = true;
            dfs(to, used, edges, graph);
        }
    }
    edges.push_back(v);
}


std::vector<std::pair<int, int>> getEdges(const Graph& graph) {
    std::map<std::pair<int, int>, bool> used;
    for (int v = 0; v < graph.size(); ++v)
        for (int u : graph.get(v))
            used[{v, u}] = false;
    
    std::vector<int> edges;

    dfs(graph.getBase(), used, edges, graph);
    
    std::vector<std::pair<int, int>> result;
        for (int i = (edges.size()-2); i > -1; --i)
            result.push_back({edges[i+1]+1, edges[i]+1});
        return result;
}


int main() {
    int n, a; std::cin >> n >> a;
    std::vector<std::vector<int>> vertices;
    for (int i = 0; i < n; ++i) {
        std::vector<int> temp;
        for (int j = 0; j < n; ++j) {
            int p; std::cin >> p;
            if ((p == 0) && (i != j))
                temp.push_back(j);
        }
        vertices.push_back(temp);
    }

    Graph graph(vertices, a-1);

    std::vector<std::pair<int, int>> answer = getEdges(graph);
    for (const auto& i : answer)
        std::cout << i.first << " " << i.second << std::endl;
}
