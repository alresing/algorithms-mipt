#include <iostream>
#include <vector>
#include <limits>
#include <unordered_map>


const int64_t INF = std::numeric_limits<int64_t>::max();


class Graph {
public:
    std::unordered_map<int64_t, int64_t>& operator[] (size_t i) {
        return capacity[i];
    }

    auto begin() {
        return capacity.begin();
    }

    auto end() {
        return capacity.end();
    }

private:
    std::unordered_map<int64_t, std::unordered_map<int64_t, int64_t>> capacity;
};


int64_t dfs(Graph& capacity, std::vector<bool>& used, int64_t v, int64_t t, int64_t minC, int64_t n) {
    if (v == t)
        return minC;
    used[v] = true;
    for (auto& pair: capacity[v]) {
        if (!used[pair.first] && pair.second > 0) {
            int64_t path = dfs(capacity, used, pair.first, t, std::min(pair.second, minC), n);
            if (path > 0) {
                pair.second -= path;
                capacity[pair.first][v] += path;
                return path;
            }
        }
    }
    return 0;
}


int64_t maxFlow(Graph& capacity, int64_t s, int64_t t, int64_t n) {
    int64_t flow = 0;
    while (true) {
        std::vector<bool> used(n, false);
        int64_t path = dfs(capacity, used, s, t, INF, n);
        if (path == 0)
            return flow;
        flow += path;
        if (flow >= 10)
            return flow;
    }
}


bool dfs(Graph& graph, std::vector<bool>& used, 
        std::vector<int64_t>& path, int64_t v, int64_t t) {
    if (used[v])
        return false;
    used[v] = true;
    path.push_back(v);
    if (v == t)
        return true;
    for (auto& pair: graph[v])
        if (pair.second > 0) {
            --pair.second;
            if (dfs(graph, used, path, pair.first, t))
                return true;
        }
    path.pop_back();
    return false;
}


int main() {
    int64_t n, m, s, t;
    std::cin >> n >> m >> s >> t;
    --s; --t;
    Graph capacity;
    
    for (int64_t i = 0; i < m; ++i) {
        int64_t v, u;
        std::cin >> v >> u;
        --v; --u;
        if (v == u)
            continue;
        
        ++capacity[v][u];
    }

    Graph start_capacity = capacity;

    int64_t res = maxFlow(capacity, s, t, n);

    if (res >= 2) {
        std::cout << "YES" << std::endl;
        
        for (const auto& row: capacity)
            for (const auto& pair: row.second)
                if (start_capacity[row.first].count(pair.first))
                    if (start_capacity[row.first][pair.first] > 0)
                        start_capacity[row.first][pair.first] -= pair.second;
        
        std::vector<bool> used(n, false);
        std::vector<int64_t> path1;
        dfs(start_capacity, used, path1, s, t);

        for (const auto& v: path1)
            std::cout << v + 1 << " ";
        std::cout << std::endl;

        used.assign(n, false);
        std::vector<int64_t> path2;
        dfs(start_capacity, used, path2, s, t);

        for (const auto& v: path2)
            std::cout << v + 1 << " ";
        std::cout << std::endl;
    } else {
        std::cout << "NO" << std::endl;
    }
}
