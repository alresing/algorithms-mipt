#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>
#include <set>


class Graph {
public:
    Graph(std::vector<std::vector<int>>& graph) : graph(graph) {}

    const std::vector<int>& get(int v) const {
        return graph[v];
    }

    int size() const {
        return graph.size();
    }

private:
    std::vector<std::vector<int>> graph;
};


void dfs(uint64_t v, std::vector<bool>& used, int& timer,
            std::vector<int>& tin, std::vector<int>& ret, 
            const Graph& graph, std::set<int>& dots, int parent = -1) {
    used[v] = true;
    tin[v] = timer++;
    ret[v] = tin[v];
    for (int to : graph.get(v)) {
        if (to == parent) continue;
        if (used[to]) {
            ret[v] = std::min(ret[v], tin[to]);
        } else {
            dfs(to, used, timer, tin, ret, graph, dots, v);
            if ((ret[to] >= tin[v]) && (parent != -1)) {
                dots.insert(v+1);
            } else if ((ret[to] >= tin[v]) && (parent == -1)) {
                bool isDot = false;
                for (int u : graph.get(v)) {
                    if (!used[u]) {
                        isDot = true;
                        break;
                    }
                }
                if (isDot)
                    dots.insert(v+1);
            }
            ret[v] = std::min(ret[v], ret[to]);
        }
    }
}


std::vector<int> getDots(const Graph& graph) {
    std::set<int> dots;
    std::vector<int> tin(graph.size(), -1);
    std::vector<int> ret(graph.size(), std::numeric_limits<int>::max());
    std::vector<bool> used(graph.size(), false);
    int timer = 0;
    for (int i = 0; i < graph.size(); ++i)
        if (!used[i])
            dfs(i, used, timer, tin, ret, graph, dots);

    std::vector<int> result;
    for (int i : dots)
        result.push_back(i);
    return result;
}


int main() {
    int n, m; std::cin >> n >> m;
    std::vector<std::vector<int>> vertices;
    for (int i = 0; i < n; ++i) {
        std::vector<int> temp;
        vertices.push_back(temp);
    }

    for (int i = 0; i < m; ++i) {
        int a, b; std::cin >> a >> b;
        vertices[a-1].push_back(b-1);
        vertices[b-1].push_back(a-1);
    }

    Graph graph(vertices);

    std::vector<int> bridges = getDots(graph);
    std::cout << bridges.size() << std::endl;
    for (int b : bridges)
        std::cout << b << " ";
    std::cout << std::endl;
}
