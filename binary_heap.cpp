#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>

class Heap {
public:
    Heap() {
        Size = 0;
    }
 
    void AddElem(std::pair<int64_t,int> t, std::vector<int> &v) {
        int index = Size;
        Data.push_back(t);
        v[t.second] = index;

        SiftUp(index, v);

        Size += 1;
    }
 
    void ExtractMin(std::vector<int> &v) {
        if (Size == 0) {
            throw std::runtime_error("Heap is empty for extract");
        }
        Data[0] = Data.back();
        v[Data[0].second] = 0;
        Data.pop_back();
        Size -= 1;
        SiftDown(0, v);
    }

    void DecreaseKey(int index, int64_t d, std::vector<int> &v) {
        Data[index].first -= d;
        SiftUp(index, v);
    }
 
    size_t GetSize() const {
        return Size;
    }
 
    std::pair<int64_t,int> GetMin() const {
        if (Size == 0) {
            throw std::runtime_error("Heap is empty for extract");
        }
        return Data[0];
    }

    void PrintHeap() const {
        for (int i = 0; i < Size; ++i) {
            std::cout << Data[i].first << "-" << Data[i].second << " ";
        }
        std::cout << std::endl;
    }
 
private:
    void SiftDown(int index, std::vector<int> &v) {
        while (2*index+2 < Size) {
            int left = 2*index + 1;
            int right = 2*index + 2;
            
            if (Data[left] <= Data[right] && Data[left] < Data[index]) {
                std::swap(v[Data[index].second], v[Data[left].second]);
                std::swap(Data[index], Data[left]);
                index = left;
                continue;
            } else if (Data[right] <= Data[index]) {
                std::swap(v[Data[index].second], v[Data[right].second]);
                std::swap(Data[index], Data[right]);
                index = right;
                continue;
            }
            break;
        }

        if (2*index+1 < Size && Data[2*index+1] < Data[index]) {
            std::swap(v[Data[index].second], v[Data[2*index+1].second]);
            std::swap(Data[index], Data[2*index+1]);
        }
    }

    void SiftUp(int index, std::vector<int> &v) {
        int parent = (index-1)/2;
        while (parent >= 0 && index > 0) {
            if (Data[index].first > Data[parent].first) {
                 break;
            }
            std::swap(v[Data[index].second], v[Data[parent].second]);
            std::swap(Data[index], Data[parent]);
            index = parent;
            parent = (index-1)/2;
        }
    }
 
    size_t Size;
    std::vector<std::pair<int64_t,int>> Data;
};

int main() {
    int n; scanf("%d", &n);

    Heap heap;
    std::vector<int> v;

    int i = 0;
    while (i < n) {
        char c = getchar();
        
        if (c == 'i') {
            v.push_back(0);
            for (int i = 0; i < 5; ++i) getchar();
            int64_t t; scanf("%lld", &t);
            heap.AddElem({t, v.size()-1}, v);
            ++i;
        } else if (c == 'g') {
            v.push_back(0);
            for (int i = 0; i < 5; ++i) getchar();
            printf("%lld\n", heap.GetMin().first);
            ++i;
        } else if (c == 'e') {
            v.push_back(0);
            for (int i = 0; i < 9; ++i) getchar();
            heap.ExtractMin(v);
            ++i;
        } else if (c == 'd') {
            v.push_back(0);
            for (int i = 0; i < 10; ++i) getchar();
            int index, d; scanf("%d%d", &index, &d);
            heap.DecreaseKey(v[index-1], d, v);
            ++i;
        }
    }
}