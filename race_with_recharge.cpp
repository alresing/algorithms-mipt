#include <iostream>
#include <stdio.h>
#include <set>

void print(std::set<std::pair<int64_t, int64_t>> s) {
    for (const auto& i : s) std::cout << "(" << i.first << ", " << i.second << ") ";
    std::cout << std::endl;
}

int main() {
    int64_t n, Y, X;
    scanf("%lld%lld%lld", &n, &Y, &X);
    std::set<std::pair<int64_t, int64_t>> stationsStarts;               // (Yj1, Xj)
    std::set<std::pair<int64_t, int64_t>> stationsEnds;                 // (Yj2, Xj)

    for (int64_t i = 0; i < n; ++i) {
        int64_t x, y1, y2;
        scanf("%lld%lld%lld", &x, &y1, &y2);
        stationsStarts.insert({y1, x});
        stationsEnds.insert({y2+1, x});
    }

    std::set<std::pair<int64_t, int64_t>> segmentsSortedByStart;        // (x_start, length)
    std::set<std::pair<int64_t, int64_t>> segmentsSortedByLength;       // (length, x_start)
    segmentsSortedByStart.insert({0, X});
    segmentsSortedByLength.insert({X, 0});

    int64_t yNow = 0;
    while (yNow < stationsStarts.begin()->first) {
        printf("%lld\n", X);
        ++yNow;
    }

    while (stationsEnds.size() > 0) {
        // 1. удаляем те, что тут заканчиваются (меняя отрезки)
        while (stationsEnds.size() > 0 && stationsEnds.begin()->first == yNow) {
            int64_t xNow = stationsEnds.begin()->second;

            std::pair<int64_t, int64_t> prevPair = *prev(segmentsSortedByStart.lower_bound({xNow, 0}));
            segmentsSortedByStart.erase(prev(segmentsSortedByStart.lower_bound({xNow, 0})));
            segmentsSortedByLength.erase({prevPair.second, prevPair.first});
            
            std::pair<int64_t, int64_t> nextPair = *segmentsSortedByStart.lower_bound({xNow, 0});
            segmentsSortedByStart.erase(segmentsSortedByStart.lower_bound({xNow, 0}));
            segmentsSortedByLength.erase({nextPair.second, nextPair.first});

            segmentsSortedByStart.insert({prevPair.first, prevPair.second + nextPair.second});
            segmentsSortedByLength.insert({prevPair.second + nextPair.second, prevPair.first});

            stationsEnds.erase(stationsEnds.begin());
        }

        // 2. добавляем те, что тут начинаются (меняя отрезки)
        while (stationsStarts.size() > 0 && stationsStarts.begin()->first == yNow) {
            int64_t xNow = stationsStarts.begin()->second;

            std::pair<int64_t, int64_t> pair = *prev(segmentsSortedByStart.lower_bound({xNow, 0}));
            segmentsSortedByStart.erase(prev(segmentsSortedByStart.lower_bound({xNow, 0})));
            segmentsSortedByLength.erase({pair.second, pair.first});

            segmentsSortedByStart.insert({pair.first, xNow - pair.first});
            segmentsSortedByLength.insert({xNow - pair.first, pair.first});

            segmentsSortedByStart.insert({xNow, pair.second - xNow + pair.first});
            segmentsSortedByLength.insert({pair.second - xNow + pair.first, xNow});

            stationsStarts.erase(stationsStarts.begin());
        }

        // 3. переходим к следующему (выводят ответы для промежуточных)
        while ((yNow < (stationsEnds.size() > 0 ? stationsEnds.begin()->first : Y+1)) && \
               (yNow < (stationsStarts.size() > 0 ? stationsStarts.begin()->first : Y+1))) {
            printf("%lld\n", prev(segmentsSortedByLength.end())->first);
            ++yNow;
        }
    }
}