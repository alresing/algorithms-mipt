#include <iostream>
#include <vector>
#include <algorithm>
#include <set>


void print(const std::vector<auto>& v) {
    for (const auto& i : v)
        std::cout << i << " ";
    std::cout << std::endl;
}


void print(const std::vector<std::pair<int, int>>& v) {
    for (const auto& i : v)
        std::cout << "(" << i.first << ", " << i.second << ") ";
    std::cout << std::endl;
}


void print(const std::vector<std::vector<auto>>& v) {
    for (const auto& i : v)
        print(i);
    std::cout << std::endl;
}


class Graph {
public:
    Graph(std::vector<std::vector<int>>& graph, int n) : graph(graph), n(n) {
        for (int i = 0; i < n; ++i) {
            std::vector<int> row;
            graphR.push_back(row);
        }
        for (int v = 0; v < n; ++v)
            for (int u : graph[v])
                graphR[u].push_back(v);

        for (int i = 0; i < n; ++i)
            tout.push_back({0, 0});
        std::vector<bool> used(n, false);
        int timer = 0;
        for (int i = 0; i < n; ++i)
            if (!used[i])
                dfs(i, used, timer);


        std::sort(tout.begin(), tout.end());
        for (int i = 0; i < n; ++i)
            used[i] = false;
        for (int i = 0; i < n; ++i)
            componentNumber.push_back(0);
        int number = 0;
        for (int i = n-1; i > -1; --i) {
            int v = tout[i].second;
            if (!used[v]) {
                dfsR(v, used, number);
                ++number;
            }
        }

        for (int v = 0; v < n; ++v)
            for (int u : graph[v])
                if (componentNumber[v] != componentNumber[u])
                    edges.insert({componentNumber[v], componentNumber[u]});
    }   

    int countEdges() {
        return edges.size();
    }

private:
    std::vector<std::vector<int>> graph;
    std::vector<std::vector<int>> graphR;
    int n = 0;
    std::set<std::pair<int, int>> edges;
    std::vector<int> componentNumber;
    std::vector<std::pair<int, int>> tout;

    void dfs(int v, std::vector<bool>& used, int& timer) {
        used[v] = true;
        for (int to : graph[v])
            if (!used[to])
                dfs(to, used, timer);
        tout[v] = {timer++, v};
    }

    void dfsR(int v, std::vector<bool>& used, int number) {
        used[v] = true;
        componentNumber[v] = number;
        for (int to : graphR[v])
            if (!used[to])
                dfsR(to, used, number);
    }
};


int main() {
    int n, m; std::cin >> n >> m;
    std::vector<std::vector<int>> vertices;
    for (int i = 0; i < n; ++i) {
        std::vector<int> temp;
        vertices.push_back(temp);
    }

    for (int i = 0; i < m; ++i) {
        int a, b; std::cin >> a >> b;
        if (a != b)
            vertices[a-1].push_back(b-1);
    }

    Graph graph(vertices, n);

    std::cout << graph.countEdges() << std::endl;
}
