#include <iostream>
#include <vector>


int main() {
    size_t n; std::cin >> n;

    std::vector<std::vector<int>> dp(n, std::vector<int>(n, 0));

    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < n; ++j)
            std::cin >> dp[i][j];


    for (size_t k = 0; k < n; ++k)
        for (size_t i = 0; i < n; ++i)
            for (size_t j = 0; j < n; ++j)
                dp[i][j] = std::min(dp[i][j], dp[i][k] + dp[k][j]);

    for (const auto& row : dp) {
        for (const auto& i : row)
            std::cout << i << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}
