#include <iostream>
#include <vector>
#include <limits>
#include <string>


const size_t INF = std::numeric_limits<size_t>::max();


size_t dfs(const std::vector<std::vector<size_t>>& graph,
        std::vector<std::vector<size_t>>& capacity, 
        std::vector<bool>& used, size_t v, size_t t, size_t minC) {
    if (v == t)
        return minC;
    used[v] = true;
    for (const auto& u: graph[v])
        if (!used[u] && capacity[v][u] > 0) {
            size_t path = dfs(graph, capacity, used, u, t, std::min(capacity[v][u], minC));
            if (path > 0) {
                capacity[v][u] -= path;
                capacity[u][v] += path;
                return path;
            }
        }
    return 0;
}


size_t maxFlow(const std::vector<std::vector<size_t>>& graph,
        std::vector<std::vector<size_t>>& capacity, size_t s, size_t t) {
    size_t flow = 0;
    while (true) {
        std::vector<bool> used(graph.size(), false);
        size_t path = dfs(graph, capacity, used, s, t, INF);
        if (path == 0)
            return flow;
        flow += path;
    }
}


void dfs(std::vector<bool>& used, std::vector<std::vector<size_t>> graph, size_t v) {
    if (used[v])
        return;
    used[v] = true;
    for (size_t u = 0; u < graph[v].size(); ++u)
        if (graph[v][u] > 0)
            dfs(used, graph, u);
}


int main() {
    size_t n; std::cin >> n;
    std::vector<std::vector<size_t>> capacity(n);
    std::vector<std::vector<size_t>> graph(n);
    for (size_t i = 0; i < n; ++i) {
        std::string s;
        std::cin >> s;
        for (size_t j = 0; j < n; ++j) {
            size_t p = s[j] - 48;
            capacity[i].push_back(p);
            if (p)
                graph[i].push_back(j);
        }
    }   

    size_t res = INF;
    std::vector<bool> min_used;
    for (size_t s = 0; s < n; ++s)
        for (size_t t = s+1; t < n; ++t) {
            std::vector<std::vector<size_t>> new_cap = capacity;
            size_t cut = maxFlow(graph, new_cap, s, t);
            if (cut < res) {
                res = cut;
                std::vector<bool> used(n, false);
                dfs(used, new_cap, s);
                min_used = used;
            }
        }

    for (size_t i = 0; i < n; ++i)
        if (min_used[i])
            std::cout << i + 1 << " ";
    std::cout << std::endl;
    for (size_t i = 0; i < n; ++i)
        if (!min_used[i])
            std::cout << i + 1 << " ";
    std::cout << std::endl;
}
