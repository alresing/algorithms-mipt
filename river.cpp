#include <iostream>
#include <fstream>

struct Node {
    int64_t size = 1;                               // size of subtree
    int64_t y = rand();                             // for heap
    int64_t val = 0;
    Node* left = nullptr;
    Node* right = nullptr;
};

// left: < ; right: >
class Treap {
public:
    ~Treap() {
        while (head != nullptr) del(0);
    }

    void insert(int64_t x, int64_t val) {
        Node* node = new Node;
        node->size = 1;
        node->val = val;
        if (head == nullptr) {
            head = node;
        } else {
            std::pair<Node*, Node*> p = split(head, x-1);
            head = merge(merge(p.first, node), p.second);
        }
    }

    int64_t del(int64_t x) {
        if (head == nullptr || head->size <= x) return 0;
        std::pair<Node*, Node*> p = split(head, x);
        int64_t ans;
        if (p.first->right == nullptr) {
            Node* to_del = p.first;
            ans = to_del->val;
            p.first = p.first->left;
            delete to_del;
        } else {
            Node* vertex = p.first;
            while (vertex->right->right != nullptr) {
                --vertex->size;
                vertex = vertex->right;
            }
            --vertex->size;
            Node* to_del = vertex->right;
            ans = to_del->val;
            vertex->right = vertex->right->left;
            delete to_del;
        }

        head = merge(p.first, p.second);

        return ans;
    }

    int64_t add(int64_t x, int64_t val) {
        return _add(head, x, val);
    }

    int64_t size() {
        if (head != nullptr) return head->size;
        return 0;
    }

    void print() const {
        if (head != nullptr) _print(head);
    }

private:
    void _print(Node* vertex) const {
        std::cout << vertex->val << " size: " << vertex->size << " ch: ";
        std::cout << (vertex->left == nullptr ? -1 : vertex->left->val) << " ";
        std::cout << (vertex->right == nullptr ? -1 : vertex->right->val) << " ";
        std::cout << std::endl;
        if (vertex->left != nullptr) _print(vertex->left);
        if (vertex->right != nullptr) _print(vertex->right);
    }

    int64_t _add(Node* vertex, int64_t x, int64_t val) {
        if (vertex->left == nullptr && x == 0) {
            vertex->val += val;
            return vertex->val;
        } else if (vertex->left == nullptr && x > 0) {
            return _add(vertex->right, x-1, val);
        } else if (vertex->left != nullptr && x < vertex->left->size) {
            return _add(vertex->left, x, val);
        } else if (vertex->left != nullptr && x == vertex->left->size) {
            vertex->val += val;
            return vertex->val;
        }   // vertex->left != nullptr && x > vertex->left->size
        return _add(vertex->right, x-1-vertex->left->size, val);
    }

    Node* merge(Node* left, Node* right) {
        if (left == nullptr) return right;
        if (right == nullptr) return left;

        if (left->y < right->y) {
            left->size += right->size;
            left->right = merge(left->right, right);
            return left;
        } else {
            right->size += left->size;
            right->left = merge(left, right->left);
            return right;
        }
    }

    // left: first x+1 elements ; right other elements
    std::pair<Node*, Node*> split(Node* node, int64_t x) {
        if (node == nullptr) return {nullptr, nullptr};
        if (((node->left == nullptr) ? 0 : node->left->size) <= x) {
            node->size -= (node->right == nullptr) ? 0 : node->right->size;
            std::pair<Node*, Node*> p = split(node->right, x-1-((node->left == nullptr) ? 0 : node->left->size));
            node->right = p.first;
            node->size += (node->right == nullptr) ? 0 : node->right->size;
            return {node, p.second};
        } else {
            node->size -= (node->left == nullptr) ? 0 : node->left->size;
            std::pair<Node*, Node*> p = split(node->left, x);
            node->left = p.second;
            node->size += (node->left == nullptr) ? 0 : node->left->size;
            return {p.first, node};
        }
    }

    Node* head = nullptr;
};

int main() {
    std::ifstream fin("river.in");
    std::ofstream fout("river.out");

    Treap tree;
    int64_t res = 0;

    int64_t n, p; fin >> n >> p;
    for (int64_t i = 0; i < n; ++i) {
        int64_t a; fin >> a;
        tree.insert(i, a);
        res += a*a;
    }
    fout << res << std::endl;
    int64_t k; fin >> k;
    for (int64_t i = 0; i < k; ++i) {
        int64_t e, v; fin >> e >> v;
        if (e == 1) { 
            if (v == 1) {
                int64_t place = tree.del(v-1);
                res -= place*place;
                int64_t new_place = tree.add(v-1, place);
                res -= (new_place-place)*(new_place-place);
                res += new_place*new_place;
            } else if (v == tree.size()) {
                int64_t place = tree.del(v-1);
                res -= place*place;
                int64_t new_place = tree.add(v-2, place);
                res -= (new_place-place)*(new_place-place);
                res += new_place*new_place;
            } else {
                int64_t place = tree.del(v-1);
                res -= place*place;
                int64_t left_place = place/2;
                int64_t right_place = place - left_place;
                int64_t new_left_place = tree.add(v-2, left_place);
                int64_t new_right_place = tree.add(v-1, right_place);
                res -= (new_left_place-left_place)*(new_left_place-left_place);
                res -= (new_right_place-right_place)*(new_right_place-right_place);
                res += new_left_place*new_left_place;
                res += new_right_place*new_right_place;
            }
        } else {
            int64_t place = tree.del(v-1);
            res -= place*place;
            int64_t left_place = place/2;
            int64_t right_place = place - left_place;
            tree.insert(v-1, left_place);
            tree.insert(v, right_place);
            res += left_place*left_place;
            res += right_place*right_place;
        }
        fout << res << std::endl;
    }
    fin.close();
    fout.close();
}