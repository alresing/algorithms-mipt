#include <iostream>
#include <vector>

class VolumeBinaryIndexedTree {
public:
    VolumeBinaryIndexedTree(int n) {
        for (int i = 0; i < n; ++i) {
            t.emplace_back();
            for (int j = 0; j < n; ++j) {
                t[i].emplace_back(n);
            }
        }
    }

    void change(int x, int y, int z, int val) {
        for (uint64_t i = x; i < t.size(); i = g(i))
            for (uint64_t j = y; j < t.size(); j = g(j))
                for (uint64_t k = z; k < t.size(); k = g(k))
                    t[i][j][k] += val;
    }

    int getSum(int x1, int y1, int z1, int x2, int y2, int z2) const {
        int ans = getSum(x2, y2, z2);
        ans -= getSum(x2, y2, z1) + getSum(x2, y1, z2) + getSum(x1, y2, z2);
        ans += getSum(x2, y1, z1) + getSum(x1, y2, z1) + getSum(x1, y1, z2);
        ans -= getSum(x1, y1, z1);
        return ans;
    }

private:
    int getSum(int x, int y, int z) const {
        int ans = 0;
        for(int i = x; i >= 0; i = f(i)-1)
            for(int j = y; j >= 0; j = f(j)-1)
                for(int k = z; k >= 0; k = f(k)-1)
                    ans += t[i][j][k];
        return ans;
    }

    static int f(int x) {
        return (x+1) & x;
    }

    static int g(int x) {
        return (x+1) | x;
    }

    std::vector<std::vector<std::vector<int>>> t;
};

int main() {
    int n; std::cin >> n;
    VolumeBinaryIndexedTree tree(n);

    while (true) {
        int m; std::cin >> m;
        if (m == 1) {
            int x, y, z, k;
            std::cin >> x >> y >> z >> k;
            tree.change(x, y, z, k);
        } else if (m == 2) {
            int x1, y1, z1, x2, y2, z2;
            std::cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
            std::cout << tree.getSum(x1-1, y1-1, z1-1, x2, y2, z2) << std::endl;
        } else {
            break;
        }
    }
}