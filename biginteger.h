#pragma once

#include <iostream>
#include <string>
#include <vector>

class BigInteger {
friend std::ostream& operator<<(std::ostream& out, const BigInteger& bi);
friend std::istream& operator>>(std::istream& in, BigInteger& bi);
friend bool operator==(const BigInteger& left, const BigInteger& right);
friend bool operator==(const int& left, const BigInteger& right);
friend bool operator==(const BigInteger& left, const int& right);
friend bool operator<(const BigInteger& left, const BigInteger& right);
friend bool operator<(const int& left, const BigInteger& right);
friend bool operator<(const BigInteger& left, const int& right);

public:
    BigInteger();

    BigInteger(const int64_t& integer);

    BigInteger(const BigInteger& bi);

    BigInteger& operator=(const BigInteger& bi);

    operator bool() const;

    BigInteger operator-() const;

    BigInteger& operator++();

    BigInteger operator++(int);

    BigInteger& operator--();

    BigInteger operator--(int);

    BigInteger& operator+=(const BigInteger& bi);

    BigInteger& operator-=(const BigInteger& bi);

    BigInteger& operator*=(const BigInteger& bi);

    BigInteger& operator/=(const BigInteger& bi);

    BigInteger& operator%=(const BigInteger& bi);

    std::string toString() const;

private:
    static bool isNotLess(const std::vector<int>& a, const std::vector<int>& b);

    std::vector<int> number;
    bool is_negative = false;
};

class Rational {
friend bool operator==(const Rational& left, const Rational& right);
friend bool operator==(const int& left, const Rational& right);
friend bool operator==(const Rational& left, const int& right);
friend bool operator==(const BigInteger& left, const Rational& right);
friend bool operator==(const Rational& left, const BigInteger& right);
friend bool operator<(const Rational& left, const Rational& right);
friend bool operator<(const int& left, const Rational& right);
friend bool operator<(const Rational& left, const int& right);
friend bool operator<(const BigInteger& left, const Rational& right);
friend bool operator<(const Rational& left, const BigInteger& right);

public:
    Rational();

    Rational(const int64_t& integer);

    Rational(const BigInteger& bi);

    Rational(const Rational& rat);

    operator double() const;

    Rational& operator=(const Rational& rat);

    Rational operator-() const;

    Rational& operator+=(const Rational& rat);

    Rational& operator-=(const Rational& rat);

    Rational& operator*=(const Rational& rat);

    Rational& operator/=(const Rational& rat);

    std::string toString() const;

    std::string asDecimal(size_t precision=0) const;

private:
    void toShort();

    BigInteger up;
    BigInteger down;
};

BigInteger operator+(const BigInteger& left, const BigInteger& right) {
    BigInteger copy = left;
    copy += right;
    return copy;
}

BigInteger operator+(const int& left, const BigInteger& right) {
    BigInteger copy = left;
    copy += right;
    return copy;
}

BigInteger operator+(const BigInteger& left, const int& right) {
    BigInteger copy = left;
    copy += right;
    return copy;
}

BigInteger operator-(const BigInteger& left, const BigInteger& right) {
    BigInteger copy = left;
    copy -= right;
    return copy;
}

BigInteger operator-(const int& left, const BigInteger& right) {
    BigInteger copy = left;
    copy -= right;
    return copy;
}

BigInteger operator-(const BigInteger& left, const int& right) {
    BigInteger copy = left;
    copy -= right;
    return copy;
}

BigInteger operator*(const BigInteger& left, const BigInteger& right) {
    BigInteger copy = left;
    copy *= right;
    return copy;
}

BigInteger operator*(const int& left, const BigInteger& right) {
    BigInteger copy = left;
    copy *= right;
    return copy;
}

BigInteger operator*(const BigInteger& left, const int& right) {
    BigInteger copy = left;
    copy *= right;
    return copy;
}

BigInteger operator/(const BigInteger& left, const BigInteger& right) {
    BigInteger copy = left;
    copy /= right;
    return copy;
}

BigInteger operator/(const int& left, const BigInteger& right) {
    BigInteger copy = left;
    copy /= right;
    return copy;
}
BigInteger operator/(const BigInteger& left, const int& right) {
    BigInteger copy = left;
    copy /= right;
    return copy;
}

BigInteger operator%(const BigInteger& left, const BigInteger& right) {
    BigInteger copy = left;
    copy %= right;
    return copy;
}

BigInteger operator%(const int& left, const BigInteger& right) {
    BigInteger copy = left;
    copy %= right;
    return copy;
}

BigInteger operator%(const BigInteger& left, const int& right) {
    BigInteger copy = left;
    copy %= right;
    return copy;
}

bool operator==(const BigInteger& left, const BigInteger& right) {
    return (left.number == right.number) && (left.is_negative == right.is_negative);
}

bool operator==(const int& left, const BigInteger& right) {
    return (static_cast<BigInteger>(left).number == right.number) && (static_cast<BigInteger>(left).is_negative == right.is_negative);
}

bool operator==(const BigInteger& left, const int& right) {
    return (left.number == static_cast<BigInteger>(right).number) && (left.is_negative == static_cast<BigInteger>(right).is_negative);
}

bool operator!=(const BigInteger& left, const BigInteger& right) {
    return !(left == right);
}

bool operator!=(const int& left, const BigInteger& right) {
    return !(left == right);
}

bool operator!=(const BigInteger& left, const int& right) {
    return !(left == right);
}

bool operator<(const BigInteger& left, const BigInteger& right) {
    if (left.is_negative && !right.is_negative) return true;
    if (!left.is_negative && right.is_negative) return false;
    bool T = !left.is_negative;
    if (left.number.size() < right.number.size()) return T;
    if (left.number.size() > right.number.size()) return !T;

    for (int64_t i = left.number.size()-1; i > -1; --i) {
        if (left.number[i] < right.number[i]) return T;
        if (left.number[i] > right.number[i]) return !T;
    }
    return !T;
}

bool operator<(const int& left, const BigInteger& right) {
    return static_cast<BigInteger>(left) < right;
}

bool operator<(const BigInteger& left, const int& right) {
    return left < static_cast<BigInteger>(right);
}

bool operator>(const BigInteger& left, const BigInteger& right) {
    return right < left;
}

bool operator>(const int& left, const BigInteger& right) {
    return right < left;
}

bool operator>(const BigInteger& left, const int& right) {
    return right < left;
}

bool operator<=(const BigInteger& left, const BigInteger& right) {
    return !(left > right);
}

bool operator<=(const int& left, const BigInteger& right) {
    return !(left > right);
}

bool operator<=(const BigInteger& left, const int& right) {
    return !(left > right);
}

bool operator>=(const BigInteger& left, const BigInteger& right) {
    return !(left < right);
}

bool operator>=(const int& left, const BigInteger& right) {
    return !(left < right);
}

bool operator>=(const BigInteger& left, const int& right) {
    return !(left < right);
}

std::ostream& operator<<(std::ostream& out, const BigInteger& bi) {
    out << bi.toString();
    return out;
}

std::istream& operator>>(std::istream& in, BigInteger& bi) {
    std::vector<int> temp;
    bi.number.clear();
    bi.is_negative = false;

    char i;
    while (in.get(i)) {
        if (std::isspace(i)) break;
        if (i == '-') bi.is_negative = true;
        else temp.push_back(i-48);
    }

    for (int64_t i = temp.size()-1; i > -1; --i) {
        bi.number.push_back(temp[i]);
    }

    return in;
}


BigInteger::BigInteger() {}

BigInteger::BigInteger(const int64_t& i) {
    int64_t integer = i;
    is_negative = (integer < 0);
    if (integer == 0) number.push_back(0);
    if (integer < 0) integer *= -1;
    while (integer > 0) {
        number.push_back(integer % 10);
        integer /= 10;
    }
}

BigInteger::BigInteger(const BigInteger& bi) : number(bi.number), is_negative(bi.is_negative) {}

BigInteger::operator bool() const {
    return (*this) != static_cast<BigInteger>(0);
}

BigInteger& BigInteger::operator=(const BigInteger& bi) {
    if (this != &bi) {
        number = bi.number;
        is_negative = bi.is_negative;
    }

    return *this;
}

BigInteger BigInteger::operator-() const {
    BigInteger copy = *this;
    copy.is_negative = !is_negative;
    if (copy.number.size() == 1 && copy.number[0] == 0) copy.is_negative = false;
    return copy;
}

BigInteger& BigInteger::operator++() {
    *this += 1;
    return *this;
}

BigInteger BigInteger::operator++(int) {
    BigInteger copy = *this;
    ++*this;
    return copy;
}

BigInteger& BigInteger::operator--() {
    *this -= 1;
    return *this;
}

BigInteger BigInteger::operator--(int) {
    BigInteger copy = *this;
    --*this;
    return copy;
}

BigInteger& BigInteger::operator+=(const BigInteger& bi) {
    if (is_negative == bi.is_negative) {
        int extra = 0;
        uint64_t index = 0;
        while (index < bi.number.size() || extra) {
            while (index >= number.size()) number.push_back(0);
            int newNumber = number[index] + extra + ((index < bi.number.size()) ? bi.number[index] : 0);
            number[index] = newNumber%10;
            extra = newNumber/10;
            ++index;
        }
    } else if (isNotLess(number, bi.number)) {
        int extra = 0;
        uint64_t index = 0;
        while (index < bi.number.size() || extra) {
            while (index >= number.size()) number.push_back(0);
            number[index] -= extra + (index < bi.number.size() ? bi.number[index] : 0);
            extra = number[index] < 0;
            if (extra) number[index] += 10;
            ++index;
        }
        while (number.size() > 1 && number.back() == 0) number.pop_back();
    } else {
        int extra = 0;
        uint64_t index = 0;
        while (index < bi.number.size() || extra) {
            while (index >= number.size()) number.push_back(0);
            number[index] = (index < bi.number.size() ? bi.number[index] : 0) - number[index] - extra;
            extra = number[index] < 0;
            if (extra) number[index] += 10;
            ++index;
        }
        while (number.size() > 1 && number.back() == 0) number.pop_back();
        is_negative = bi.is_negative;
    }

    while (number.size() > 1 && number.back() == 0) number.pop_back();
    if ((number.size() == 1) && number[0] == 0) is_negative = false;
    return *this;
}

BigInteger& BigInteger::operator-=(const BigInteger& bi) {
    if (is_negative != bi.is_negative) {
        int extra = 0;
        uint64_t index = 0;
        while (index < bi.number.size() || extra != 0) {
            while (index >= number.size()) number.push_back(0);
            int newNumber = number[index] + extra + ((index < bi.number.size()) ? bi.number[index] : 0);
            number[index] = newNumber%10;
            extra = newNumber/10;
            ++index;
        }
    } else if (isNotLess(number, bi.number)) {
        int extra = 0;
        uint64_t index = 0;
        while (index < bi.number.size() || extra) {
            while (index >= number.size()) number.push_back(0);
            number[index] -= extra + (index < bi.number.size() ? bi.number[index] : 0);
            extra = number[index] < 0;
            if (extra) number[index] += 10;
            ++index;
        }
        while (number.size() > 1 && number.back() == 0) number.pop_back();
    } else {
        int extra = 0;
        uint64_t index = 0;
        while (index < bi.number.size() || extra) {
            while (index >= number.size()) number.push_back(0);
            number[index] = (index < bi.number.size() ? bi.number[index] : 0) - number[index] - extra;
            extra = number[index] < 0;
            if (extra) number[index] += 10;
            ++index;
        }
        while (number.size() > 1 && number.back() == 0) number.pop_back();
        is_negative = !is_negative;
    }

    while (number.size() > 1 && number.back() == 0) number.pop_back();
    if ((number.size() == 1) && number[0] == 0) is_negative = false;
    return *this;
}

BigInteger& BigInteger::operator*=(const BigInteger& bi) {
    std::vector<int> newNumber;

    for (uint64_t i = 0; i < number.size(); ++i) {
        for (uint64_t j = 0; j < bi.number.size(); ++j) {
            while ((i + j) >= newNumber.size()) newNumber.push_back(0);
            newNumber[i+j] += number[i] * bi.number[j];
        }
    }

    for (uint64_t i = 0; i < newNumber.size(); ++i) {
        if (newNumber[i] > 9) {
            if ((i+1) == newNumber.size()) newNumber.push_back(0);
            newNumber[i+1] += newNumber[i]/10;
            newNumber[i] = newNumber[i]%10;
        }
    }

    number = newNumber;
    is_negative = (is_negative != bi.is_negative);

    while (number.size() > 1 && number.back() == 0) number.pop_back();
    if ((number.size() == 1) && number[0] == 0) is_negative = false;
    return *this;
}

BigInteger& BigInteger::operator/=(const BigInteger& bi) {
    bool was_neg = is_negative;
    is_negative = false;
    if (bi.number.size() == 1 && bi.number[0] == 0) throw std::runtime_error("Division by zero");
    if (!isNotLess(number, bi.number)) {
        number.clear();
        number.push_back(0);
        is_negative = false;
    } else {
        BigInteger res = 0;
        BigInteger t = bi; t.is_negative = false;
        BigInteger nowNumber = *this;
        BigInteger newNumber = t;
        BigInteger ps = 1;
        BigInteger pd = 1;
        BigInteger d  = 1;

        while(nowNumber >= newNumber) {
            d = 1;
            ps = newNumber;
            pd = d;
            while (nowNumber >= newNumber) {
                ps = newNumber;
                pd = d;
                newNumber *= 10;
                d *= 10;
            }
            newNumber = ps;
            d = pd;
            while (nowNumber >= newNumber) {
                nowNumber -= newNumber;
                res += d;
            }
            newNumber = t;
            while (newNumber.number.size() > 1 && newNumber.number.back() == 0) newNumber.number.pop_back();
            if ((newNumber.number.size() == 1) && newNumber.number[0] == 0) newNumber.is_negative = false;
        }
        *this = res;
    }

    is_negative = was_neg;
    is_negative = (is_negative != bi.is_negative);
    while (number.size() > 1 && number.back() == 0) number.pop_back();
    if ((number.size() == 1) && number[0] == 0) is_negative = false;
    return *this;
}

BigInteger& BigInteger::operator%=(const BigInteger& bi) {
    *this -= (*this / bi) * bi;

    while (number.size() > 1 && number.back() == 0) number.pop_back();
    if ((number.size() == 1) && number[0] == 0) is_negative = false;
    return *this;
}

std::string BigInteger::toString() const {
    std::string s;
    if (is_negative) s += '-';
    for (int64_t i = number.size()-1; i > -1; --i) {
        s += number[i] + 48;
    }

    return s;
}

bool BigInteger::isNotLess(const std::vector<int>& a, const std::vector<int>& b) {
    if (a.size() > b.size()) return true;
    if (a.size() < b.size()) return false;
    for (int64_t i = a.size()-1; i > -1; --i) {
        if (a[i] < b[i]) return false;
        if (a[i] > b[i]) return true;
    }
    return true;
}



Rational operator+(const Rational& left, const Rational& right) {
    Rational copy = left;
    copy += right;
    return copy;
}

Rational operator+(const int& left, const Rational& right) {
    Rational copy = left;
    copy += right;
    return copy;
}

Rational operator+(const Rational& left, const int& right) {
    Rational copy = left;
    copy += right;
    return copy;
}

Rational operator+(const BigInteger& left, const Rational& right) {
    Rational copy = left;
    copy += right;
    return copy;
}

Rational operator+(const Rational& left, const BigInteger& right) {
    Rational copy = left;
    copy += right;
    return copy;
}

Rational operator-(const Rational& left, const Rational& right) {
    Rational copy = left;
    copy -= right;
    return copy;
}

Rational operator-(const int& left, const Rational& right) {
    Rational copy = left;
    copy -= right;
    return copy;
}

Rational operator-(const Rational& left, const int& right) {
    Rational copy = left;
    copy -= right;
    return copy;
}

Rational operator-(const BigInteger& left, const Rational& right) {
    Rational copy = left;
    copy -= right;
    return copy;
}

Rational operator-(const Rational& left, const BigInteger& right) {
    Rational copy = left;
    copy -= right;
    return copy;
}

Rational operator*(const Rational& left, const Rational& right) {
    Rational copy = left;
    copy *= right;
    return copy;
}

Rational operator*(const int& left, const Rational& right) {
    Rational copy = left;
    copy *= right;
    return copy;
}

Rational operator*(const Rational& left, const int& right) {
    Rational copy = left;
    copy *= right;
    return copy;
}

Rational operator*(const BigInteger& left, const Rational& right) {
    Rational copy = left;
    copy *= right;
    return copy;
}

Rational operator*(const Rational& left, const BigInteger& right) {
    Rational copy = left;
    copy *= right;
    return copy;
}

Rational operator/(const Rational& left, const Rational& right) {
    Rational copy = left;
    copy /= right;
    return copy;
}

Rational operator/(const int& left, const Rational& right) {
    Rational copy = left;
    copy /= right;
    return copy;
}
Rational operator/(const Rational& left, const int& right) {
    Rational copy = left;
    copy /= right;
    return copy;
}

Rational operator/(const BigInteger& left, const Rational& right) {
    Rational copy = left;
    copy /= right;
    return copy;
}
Rational operator/(const Rational& left, const BigInteger& right) {
    Rational copy = left;
    copy /= right;
    return copy;
}

bool operator==(const Rational& left, const Rational& right) {
    return (left.up == right.up) && (left.down == right.down);
}

bool operator==(const int& left, const Rational& right) {
    return (static_cast<Rational>(left).up == right.up) && (static_cast<Rational>(left).down == right.down);
}

bool operator==(const Rational& left, const int& right) {
    return (left.up == static_cast<Rational>(right).up) && (left.down == static_cast<Rational>(right).down);
}

bool operator==(const BigInteger& left, const Rational& right) {
    return (static_cast<Rational>(left).up == right.up) && (static_cast<Rational>(left).down == right.down);
}

bool operator==(const Rational& left, const BigInteger& right) {
    return (left.up == static_cast<Rational>(right).up) && (left.down == static_cast<Rational>(right).down);
}

bool operator!=(const Rational& left, const Rational& right) {
    return !(left == right);
}

bool operator!=(const int& left, const Rational& right) {
    return !(left == right);
}

bool operator!=(const Rational& left, const int& right) {
    return !(left == right);
}

bool operator!=(const BigInteger& left, const Rational& right) {
    return !(left == right);
}

bool operator!=(const Rational& left, const BigInteger& right) {
    return !(left == right);
}

bool operator<(const Rational& left, const Rational& right) {
    return (left.up*right.down) < (right.up*left.down);
}

bool operator<(const int& left, const Rational& right) {
    return static_cast<Rational>(left).up*right.down < right.up*static_cast<Rational>(left).down;
}

bool operator<(const Rational& left, const int& right) {
    return left.up*static_cast<Rational>(right).down < static_cast<Rational>(right).up*left.down;
}

bool operator<(const BigInteger& left, const Rational& right) {
    return static_cast<Rational>(left).up*right.down < right.up*static_cast<Rational>(left).down;
}

bool operator<(const Rational& left, const BigInteger& right) {
    return left.up*static_cast<Rational>(right).down < static_cast<Rational>(right).up*left.down;
}

bool operator>(const Rational& left, const Rational& right) {
    return right < left;
}

bool operator>(const int& left, const Rational& right) {
    return right < left;
}

bool operator>(const Rational& left, const int& right) {
    return right < left;
}

bool operator>(const BigInteger& left, const Rational& right) {
    return right < left;
}

bool operator>(const Rational& left, const BigInteger& right) {
    return right < left;
}

bool operator<=(const Rational& left, const Rational& right) {
    return !(left > right);
}

bool operator<=(const int& left, const Rational& right) {
    return !(left > right);
}

bool operator<=(const Rational& left, const int& right) {
    return !(left > right);
}

bool operator<=(const BigInteger& left, const Rational& right) {
    return !(left > right);
}

bool operator<=(const Rational& left, const BigInteger& right) {
    return !(left > right);
}

bool operator>=(const Rational& left, const Rational& right) {
    return !(left < right);
}

bool operator>=(const int& left, const Rational& right) {
    return !(left < right);
}

bool operator>=(const Rational& left, const int& right) {
    return !(left < right);
}

bool operator>=(const BigInteger& left, const Rational& right) {
    return !(left < right);
}

bool operator>=(const Rational& left, const BigInteger& right) {
    return !(left < right);
}


Rational::Rational() {}

Rational::Rational(const int64_t& integer) : up(integer), down(1) {}

Rational::Rational(const BigInteger& bi) : up(bi), down(1) {}

Rational::Rational(const Rational& rat) : up(rat.up), down(rat.down) {}

Rational::operator double() const {
    return std::stod(asDecimal(32));
}

Rational& Rational::operator=(const Rational& rat) {
    if (this != &rat) {
        up = rat.up;
        down = rat.down;
    }

    return *this;
}

Rational Rational::operator-() const {
    Rational copy = *this;
    copy.up *= -1;
    return copy;
}

Rational& Rational::operator+=(const Rational& rat) {
    up *= rat.down;
    up += down*rat.up;
    down *= rat.down;

    toShort();
    return *this;
}

Rational& Rational::operator-=(const Rational& rat) {
    up *= rat.down;
    up -= down*rat.up;
    down *= rat.down;

    toShort();
    return *this;
}

Rational& Rational::operator*=(const Rational& rat) {
    up *= rat.up;
    down *= rat.down;

    toShort();
    return *this;
}

Rational& Rational::operator/=(const Rational& rat) {
    if (rat.up == static_cast<BigInteger>(0)) throw std::runtime_error("Division by zero");
    up *= rat.down;
    down *= rat.up;
    if (down < static_cast<BigInteger>(0)) {
        down *= -1;
        up *= -1;
    }

    toShort();
    return *this;
}

std::string Rational::toString() const {
    std::string s;
    s += up.toString();
    if (down != static_cast<BigInteger>(1)) {
        s += '/';
        s += down.toString();
    }
    return s;
}

std::string Rational::asDecimal(size_t precision) const {
    std::string s;
    BigInteger a = up;
    if (a < 0) a *= -1;

    for (uint64_t i = 0; i < precision; ++i) {
        a *= 10;
    }
    s += (a /= down).toString();
    
    if (precision == 0) return ((up < 0) ? "-" : "") + s;
    
    std::string leftS;
    while ((s.size() + leftS.size()) <= precision) {
        leftS += "0";
    }
    s = leftS + s;

    s.insert(s.size()-precision, ".");

    return ((up < 0) ? "-" : "") + s;
}

void Rational::toShort() {
    BigInteger a = up;
    if (a < static_cast<BigInteger>(0)) a *= -1;
    BigInteger b = down;
    if (b < static_cast<BigInteger>(0)) b *= -1;
    while (b != static_cast<BigInteger>(0)) {
        a %= b;
        BigInteger t = a;
        a = b; b = t;
    }

    up /= a;
    down /= a;

    if (down < static_cast<BigInteger>(0)) {
        down *= -1;
        up *= -1;
    }
}