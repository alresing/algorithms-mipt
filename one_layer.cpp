#include <iostream>
#include <vector>

void merge(std::vector<std::pair<int,int>>& a, int l, int m, int r) {
    int n1 = m-l+1;
    int n2 = r-m;
 
    std::vector<std::pair<int,int>> L;
    std::vector<std::pair<int,int>> R;
 
    for (int i = 0; i < n1; i++) {
        L.push_back(a[l+i]);
    }
    for (int j = 0; j < n2; j++) {
        R.push_back(a[m+1+j]);
    }
 
    int i = 0;
    int j = 0;
    int t = l;
    while (i < n1 && j < n2) {
        if (L[i].first <= R[j].first) {
            a[t] = L[i];
            i += 1;
        } else {
            a[t] = R[j];
            j += 1;
        }
        t += 1;
    }
 
    while (i < n1) {
        a[t] = L[i];
        i += 1;
        t += 1;
    }
 
    while (i < n2) {
        a[t] = R[j];
        j += 1;
        t += 1;
    }
}
 
void mergeSort(std::vector<std::pair<int,int>>& a, int l, int r) {
    if (l<r) {
        int m = l+(r-l)/2;
        mergeSort(a, l, m);
        mergeSort(a, m+1, r);
 
        merge(a, l, m, r);
    }
}

int main() {
    int n; std::cin >> n;
    std::vector<std::pair<int,int>> v;

    for (int i = 0; i < n; ++i) {
        int l, r;
        std::cin >> l >> r;
        v.push_back({l,1});
        v.push_back({r,-1});
    }

    mergeSort(v, 0, 2*n-1);

    int res = 0;
    int bal = 0;
    int now_point = 0;

    for (int i = 0; i < 2*n-1; ++i) {
        now_point = v[i].first;
        bal += v[i].second;

        if (bal == 1) {
            res += v[i+1].first - now_point;
        }
    }

    std::cout << res << std::endl;

    return 0;
}