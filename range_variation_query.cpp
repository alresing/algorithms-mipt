#include <iostream>
#include <vector>

struct Node {
    Node(int64_t a, int64_t b, int64_t c) : max(a), min(b), size(c) {}

    int64_t max = 0;
    int64_t min = 0;
    int64_t size = 1;
};

class SegmentTree {
public:
    SegmentTree(std::vector<int64_t>& numbers) {
        for (uint64_t i = 0; i < 2*numbers.size(); ++i) t.push_back(Node(-1'000'000, 1'000'000, 1));
        build(numbers, 0, 0, numbers.size());
    }

    int64_t get(uint64_t l, int64_t r) const {
        return getMax(0, l ,r) - getMin(0, l, r);
    }

    void change(uint64_t index, int64_t val) {  
        change(0, index, val);
    }

private:
    void build(std::vector<int64_t>& numbers, uint64_t i, int64_t tl, int64_t tr) {
        if (tr - tl == 1) {
            t[i] = Node(numbers[tl], numbers[tl], 1);
        } else {
            int64_t tm = (tl + tr)/2;
            build(numbers, 2*i+1, tl, tm);
            build(numbers, 2*i+2, tm, tr);
            t[i].max = std::max(t[2*i+1].max, t[2*i+2].max);
            t[i].min = std::min(t[2*i+1].min, t[2*i+2].min);
            t[i].size = t[2*i+1].size + t[2*i+2].size;
        }
    }

    void change(uint64_t i, int64_t index, int64_t val) {
        if ((2*i+2) < t.size()) {
            if (t[2*i+1].size > index) {
                change(2*i+1, index, val);
                t[i].max = std::max(t[2*i+1].max, t[2*i+2].max);
                t[i].min = std::min(t[2*i+1].min, t[2*i+2].min);
            } else {
                change(2*i+2, index - t[2*i+1].size, val);
                t[i].max = std::max(t[2*i+1].max, t[2*i+2].max);
                t[i].min = std::min(t[2*i+1].min, t[2*i+2].min);
            }
        } else {
            t[i].max = val;
            t[i].min = val;
        }
    }

    int64_t getMax(uint64_t i, int64_t l, int64_t r) const {
        if (i >= t.size()) return -1'000'000;
        if ((l == 0) && (r == (t[i].size-1))) {
            return t[i].max;
        } else {
            if (r < ((t[i].size/2)-1)) return getMax(2*i+1, l, r);
            if (l > ((t[i].size/2)-1)) return getMax(2*i+2, l-(t[i].size/2), r-(t[i].size/2));
            return std::max(getMax(2*i+1, l, (t[i].size/2)-1), getMax(2*i+2, 0, r-(t[i].size/2)));
        }
    }

    int64_t getMin(uint64_t i, int64_t l, int64_t r) const {
        if (i >= t.size()) return 1'000'000;
        if ((l == 0) && (r == (t[i].size-1))) {
            return t[i].min;
        } else {
            if (r < ((t[i].size/2)-1)) return getMin(2*i+1, l, r);
            if (l > ((t[i].size/2)-1)) return getMin(2*i+2, l-(t[i].size/2), r-(t[i].size/2));
            return std::min(getMin(2*i+1, l, (t[i].size/2)-1), getMin(2*i+2, 0, r-(t[i].size/2)));
        }
    }

    std::vector<Node> t;
};

int main() {
    std::vector<int64_t> v;
    for (int64_t n = 1; n <= 1048576; ++n) 
        v.push_back(((n*n)%12345)+((n*n*n)%23456));

    SegmentTree tree(v);
    
    int64_t k; std::cin >> k;
    for (int64_t useless = 0; useless < k; ++useless) {
        int64_t x, y; std::cin >> x >> y;
        if (x > 0) {
            std::cout << tree.get(x-1, y-1) << std::endl;;
        } else {
            tree.change(-x-1, y);
        }
    }
}