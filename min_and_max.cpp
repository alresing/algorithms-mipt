#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

class Heap {
public:
    Heap(bool (*comp) (int64_t, int64_t)) :
        Size(0), cmp(comp) {}
 
    int64_t AddElem(std::pair<int64_t, int64_t> t, Heap &anotherHeap) {
        int64_t index = Size;
        Data.push_back(t);
        int64_t parent = (index-1)/2;
        while (parent >= 0 && index > 0) {
            if ((*cmp)(Data[parent].first, Data[index].first)) {
                break;
            }
            anotherHeap.SetIndex(Data[parent].second, index);
            std::swap(Data[index], Data[parent]);

            index = parent;
            parent = (index-1)/2;
        }
        Size += 1;

        return index;
    }
 
    std::pair<int64_t,int64_t> ExtractHead(Heap &anotherHeap) {
        if (Size == 0) {
            throw std::runtime_error("Heap is empty for extract");
        }
        std::pair<int64_t,int64_t> element = Data[0];
        Data[0] = Data.back();
        anotherHeap.SetIndex(Data[0].second, 0);
        Data.pop_back();
        Size -= 1;
        SiftDown(0, anotherHeap);
 
        return element;
    }

    void DeleteElem(int64_t index, Heap &anotherHeap) {
        Data[index] = Data.back();
        anotherHeap.SetIndex(Data[index].second, index);

        if ((*cmp)(Data[index].first, Data[(index-1)/2].first)) {
            int64_t parent = (index-1)/2;
            while (parent >= 0 && index > 0) {
                if ((*cmp)(Data[parent].first, Data[index].first)) {
                    break;
                }
                anotherHeap.SetIndex(Data[parent].second, index);
                anotherHeap.SetIndex(Data[index].second, parent);
                std::swap(Data[index], Data[parent]);

                index = parent;
                parent = (index-1)/2;
            }
        }

        SiftDown(index, anotherHeap);

        Data.pop_back();
        Size -= 1;
    }

    void SetIndex(int64_t indexHere, int64_t indexAnother) {
        Data[indexHere].second = indexAnother;
    }
 
    size_t GetSize() const {
        return Size;
    }
 
    std::pair<int64_t,int64_t> GetHead() const {
        return Data[0];
    }

    void PrintHeap() const {
        for (int64_t i = 0; i < Size; ++i) {
            std::cout << Data[i].first << "-" << Data[i].second << " ";
        }
        std::cout << std::endl;
    }
 
private:
    void SiftDown(int64_t index, Heap &anotherHeap) {
        int64_t left = 2*index + 1;
        int64_t right = 2*index + 2;

        while (left < Size && right < Size) {
            if ((*cmp)(Data[left].first, Data[right].first) && (*cmp)(Data[left].first, Data[index].first)) {
                anotherHeap.SetIndex(Data[index].second, left);
                anotherHeap.SetIndex(Data[left].second, index);
                std::swap(Data[index], Data[left]);
                index = left;
            } else if ((*cmp)(Data[right].first, Data[left].first) && (*cmp)(Data[right].first, Data[index].first)) {
                anotherHeap.SetIndex(Data[index].second, right);
                anotherHeap.SetIndex(Data[right].second, index);
                std::swap(Data[index], Data[right]);
                index = right;
            } else {
                break;
            }

            left = 2*index+1;
            right = 2*index+2;
        }

        if (left < Size) {
            if ((*cmp)(Data[left].first, Data[index].first)) {
                anotherHeap.SetIndex(Data[index].second, left);
                anotherHeap.SetIndex(Data[left].second, index);
                std::swap(Data[index], Data[left]);
                SiftDown(left, anotherHeap);
            }
        }
 
        if (right < Size) {
            if ((*cmp)(Data[right].first, Data[index].first)) {
                anotherHeap.SetIndex(Data[index].second, right);
                anotherHeap.SetIndex(Data[right].second, index);
                std::swap(Data[index], Data[right]);
                SiftDown(right, anotherHeap);
            }
        }
    }
    
    bool (*cmp) (int64_t a, int64_t b);

    size_t Size;
    std::vector<std::pair<int64_t,int64_t>> Data;
};

bool minHeapComparator(int64_t a, int64_t b) {
    return a < b;
}

bool maxHeapComparator(int64_t a, int64_t b) {
    return a > b;
}
 
int main() {
    int64_t n; scanf("%lld", &n);

    Heap minHeap(&minHeapComparator);
    Heap maxHeap(&maxHeapComparator);

    while (n > 0) {
        char c = getchar();
        if (c == 'G') {
            for (int i = 0; i < 4; ++i) c = getchar();
            if (c == 'i') {
                getchar();
                maxHeap.DeleteElem(minHeap.GetHead().second, minHeap);
                printf("%lld\n", minHeap.ExtractHead(maxHeap).first);
            } else {
                getchar();
                minHeap.DeleteElem(maxHeap.GetHead().second, maxHeap);
                printf("%lld\n", maxHeap.ExtractHead(minHeap).first);
            }
            --n;
        } else if (c == 'I') {
            for (int i = 0; i < 5; ++i) getchar(); 
            int64_t number;
            scanf("(%lld)", &number);
            
            int64_t indexMax = maxHeap.AddElem( {number, -1}, minHeap );
            int64_t indexMin = minHeap.AddElem( {number, indexMax}, maxHeap );
            maxHeap.SetIndex(indexMax, indexMin);
            --n;
        }
    }

    return 0;
}