#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>


class Graph {
public:
    Graph(std::vector<std::vector<std::pair<int, int>>>& graph, int n) : graph(graph), n(n) {
        std::vector<int> tin(n, -1);
        std::vector<int> ret(n, std::numeric_limits<int>::max());
        std::vector<bool> used(n, false);
        int timer = 0;
        for (int i = 0; i < n; ++i)
            if (!used[i])
                dfs(i, used, timer, tin, ret);
    }   

    int bridgesCount() const {
        return count;
    }

    std::vector<int> getBridges() {
        std::sort(bridges.begin(), bridges.end());
        return bridges;
    }

private:
    std::vector<std::vector<std::pair<int, int>>> graph;
    int n = 0;
    int count = 0;
    std::vector<int> bridges;

    void dfs(uint64_t v, std::vector<bool>& used, int& timer,
                std::vector<int>& tin, std::vector<int>& ret, int parent = -1) {
        used[v] = true;
        tin[v] = timer++;
        ret[v] = tin[v];
        for (uint64_t index = 0; index < graph[v].size(); ++index) {
            int to = graph[v][index].first;
            if (to == parent) continue;
            if (used[to]) {
                ret[v] = std::min(ret[v], tin[to]);
            } else {
                dfs(to, used, timer, tin, ret, v);
                if (ret[to] >= tin[to]) {
                    ++count;
                    bridges.push_back(graph[v][index].second);
                }
                ret[v] = std::min(ret[v], ret[to]);
            }
        }
    }
};


int main() {
    int n, m; std::cin >> n >> m;
    // vertices[v][index] = {u, i}
    std::vector<std::vector<std::pair<int, int>>> vertices;
    for (int i = 0; i < n; ++i) {
        std::vector<std::pair<int, int>> temp;
        vertices.push_back(temp);
    }

    for (int i = 0; i < m; ++i) {
        int a, b; std::cin >> a >> b;
        vertices[a-1].push_back({b-1, i+1});
        vertices[b-1].push_back({a-1, i+1});
    }

    Graph graph(vertices, n);

    std::vector<int> bridges = graph.getBridges();
    std::cout << graph.bridgesCount() << std::endl;
    for (int b : bridges)
        std::cout << b << " ";
    std::cout << std::endl;
}
