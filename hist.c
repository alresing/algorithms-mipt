#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct Node Node;
struct Node {
    int val;
    Node* last;
};

typedef struct Stack Stack;
struct Stack {
    int size;
    Node* last;
};


void push(Stack* st, int n) {
    Node* newNode = (Node*) malloc(sizeof(Node));
    newNode->val = n;

    if (st->size > 0) {
        newNode->last = st->last;
    }
    st->last = newNode;
    st->size += 1;
}

int pop(Stack* st) {
    int t = st->last->val;
    Node* wasLastNode = st->last;

    st->last = wasLastNode->last;
    st->size -= 1;

    free(wasLastNode);

    return t;
}

int back(Stack* st) {
    return st->last->val;
}

int size(Stack* st) {
    return st->size;
}

void clear(Stack* st) {
    while (st->size > 0) {
        pop(st);
    }
}


int main() {
    int n; scanf("%d", &n);
    int* a = (int*) malloc(sizeof(int) * n);

    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }

    Stack st; st.size = 0;
    int* left = (int*) malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++) {
        while (size(&st) != 0 && a[back(&st)] >= a[i]) {
            pop(&st);
        }
        if (size(&st) == 0) {
            left[i] = -1;
        } else {
            left[i] = back(&st);
        }
        push(&st, i);
    }


    clear(&st);
    int* right = (int*) malloc(sizeof(int) * n);
    for (int i = n-1; i > -1; i--) {
        while (size(&st) != 0 && a[back(&st)] >= a[i]) {
            pop(&st);
        }
        if (size(&st) == 0) {
            right[i] = n;
        } else {
            right[i] = back(&st);
        }
        push(&st, i);
    }


    int64_t result = 0;
    for (int i = 0; i < n; i++) {
        int64_t tmp1 = a[i];
        int64_t tmp2 = right[i] - left[i] - 1;
        int64_t temp_result = tmp1 * tmp2;
        if (temp_result > result) {
            result = temp_result;
        }
    }

    printf("%lld\n", result);

    return 0;
}