#include <iostream>
#include <map>


const int64_t MODULE = 999'999'937;
const char LETTERS[5] = {'a', 'b', 'c', 'd', 'e'};


int64_t getSum(std::map<std::pair<char, char>, int64_t> s) {
    int64_t res = 0;
    for (const auto& l : LETTERS) {
        for (const auto& r : LETTERS) {
            res += s[{l, r}];
            res %= MODULE;
        }
    }
    return res;
}


bool good(const char& l, const char& r) {
    if ((l == 'c') && (r == 'd')) return false;
    if ((l == 'c') && (r == 'e')) return false;
    if ((l == 'e') && (r == 'd')) return false;
    if ((l == 'e') && (r == 'e')) return false;
    return true;
}


std::map<std::pair<char, char>, int64_t> glue(std::map<std::pair<char, char>, int64_t> left, 
                                            std::map<std::pair<char, char>, int64_t> right) {
    std::map<std::pair<char, char>, int64_t> res;
    for (const auto& l : LETTERS) {
        for (const auto& r : LETTERS) {
            res[{l, r}] = 0;
            for (const auto& cl : LETTERS) {
                for (const auto& cr : LETTERS) {
                    if (good(cl, cr)) {
                        res[{l, r}] += (left[{l, cl}] * right[{cr, r}]);
                        res[{l, r}] %= MODULE;
                    }
                }
            }
        }
    }

    return res;
}


std::map<std::pair<char, char>, int64_t> one() {
    std::map<std::pair<char, char>, int64_t> res;
    for (const auto& l : LETTERS)
        for (const auto& r : LETTERS)
            res[{l, r}] = ((l == r) ? 1 : 0);
    return res;
}


std::map<std::pair<char, char>, int64_t> dp(const int64_t& n, std::map<int64_t, std::map<std::pair<char, char>, int64_t>>& cashe) {
    if (n == 1) return one();
    auto iter = cashe.find(n);
    if (iter != cashe.end()) {
        return iter->second;
    }

    std::map<std::pair<char, char>, int64_t> res = glue(dp(n/2, cashe), dp(n - (n/2), cashe));
    cashe[n] = res;
    return res;
}
    

int main() {
    std::map<int64_t, std::map<std::pair<char, char>, int64_t>> cashe;

    while (true) {
        int64_t n; std::cin >> n;
        if (n == 0) break;
        std::cout << getSum(dp(n, cashe)) << std::endl;
    }
}
