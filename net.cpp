#include <iostream>
#include <vector>


int64_t pow2(const int64_t& x) {
    int64_t pow = 0;
    int64_t now = 1;
    while (pow < x) {
        ++pow;
        now *= 2;
    }
    return now;
}


int main() {
    int64_t n; std::cin >> n;
    int64_t pow2n = pow2(n);

    std::vector<std::vector<bool>> matrix;
    for (int64_t i = 0; i < n; ++i) {
        std::vector<bool> temp;
        for (int64_t j = 0; j < n; ++j) {
            char p; std::cin >> p;
            temp.push_back(p == 'Y');
        }
        matrix.push_back(temp);
    }

    std::vector<int64_t> dp(pow2n, 0);

    for (int64_t mask = 0; mask < pow2n; ++mask) {
        for (int64_t i = 0; i < n; ++i) {           
            if ((mask >> i) & 1) {
                for (int64_t j = 0; j < n; ++j) {
                    if ((j != i) && ((mask >> j) & 1) && matrix[i][j]) {
                        int64_t new_mask = mask - ((1 << j) + (1 << i));
                        dp[mask] = std::max(dp[mask], 2 + dp[new_mask]);
                    }
                }
            }
        }
    }

    std::cout << dp[pow2n-1] << std::endl;

}
