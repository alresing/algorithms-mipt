#include <iostream>
#include <vector>
#include <set>


int pow2(const int& n) {
    int pow = 0;
    int number = 1;
    while (pow < n) {
        pow += 1;
        number *= 2;
    }
    return number;
}


int bit(int a, int i) {
    return ((a >> i) & 1);
}


bool good(const int& m1, const int& m2, const int& n) {
    for (int i = 0; i < (n-1); ++i) {
        if ((bit(m1, i) == bit(m2,i)) && (bit(m1,i+1) == bit(m2,i+1)) && (bit(m1,i) == bit(m1,i+1)))
            return false;
    }
    return true;
}


int main() {
    int me, ne; std::cin >> me >> ne;
    int m = std::max(me, ne);
    int n = std::min(me, ne);

    int pow2n = pow2(n);

    std::set<std::pair<int, int>> correct;
    for (int mask = 0; mask < pow2n; ++mask)
        for (int mask2 = 0; mask2 < pow2n; ++mask2)
            if (good(mask, mask2, n))
                correct.insert({mask, mask2});

    std::vector<std::vector<int>> dp;
    for (int j = 0; j < m; ++j) {
        std::vector<int> temp(pow2n+1, 0);
        dp.push_back(temp);
    }

    for (int mask = 0; mask < pow2n; ++mask) {
        dp[0][mask] = 1;
    }
    
    for (int j = 1; j < m; ++j)
        for (int mask_prev = 0; mask_prev < pow2n; ++mask_prev)
            for (int mask = 0; mask < pow2n; ++mask)
                if (correct.count({mask_prev, mask}) == 1)
                    dp[j][mask_prev] += dp[j-1][mask];

    int result = 0;
    for (int mask = 0; mask < pow2n; ++mask)
        result += dp[m-1][mask];

    std::cout << result << std::endl;
}
