#include <iostream>

struct Node {
    int64_t sum = 0;
    int64_t val = 0;
    Node* parent = nullptr;
    Node* left = nullptr;
    Node* right = nullptr;
};

// left: <= ; right: >
class SplayTree {
public:
    ~SplayTree() {
        while (size != 0) del(head->val);
    }

    void add(int64_t x) {
        Node* node = new Node;
        node->val = x;
        node->sum = x;
        if (size == 0) {
            head = node;
            ++size;
        }
        else if (!find(x)) {
            _add(head, node);
            ++size; 
            splay(node);
        }
    }

    void del(int64_t x) {
        if (size == 0) return;
        Node* vertex = head;
        while (true) {
            if (vertex == nullptr) return;
            if (vertex->val == x)  break;
            if (vertex->val < x)   vertex = vertex->right;
            else                   vertex = vertex->left;
        }
        splay(vertex);
        Node* left = head->left;
        Node* right = head->right;
        delete head;
        if (left != nullptr) left->parent = nullptr;
        if (right != nullptr) right->parent = nullptr;
        head = merge(left, right);
        --size;
    }

    bool find(int64_t x) {
        if (size == 0) return false;
        Node* vertex = head;
        while (true) {
            if (vertex == nullptr) return false;
            if (vertex->val == x)  break;
            if (vertex->val < x)   vertex = vertex->right;
            else                   vertex = vertex->left;
        }
        splay(vertex);
        return true;
    }

    int64_t getSum(int64_t l, int64_t r) {
        if (l > r) return 0;
        if (size == 0) return 0;
        Node* vertex = head;
        while(vertex->right != nullptr) vertex = vertex->right;
        int64_t max = vertex->val;
        vertex = head;
        while(vertex->left != nullptr) vertex = vertex->left;
        int64_t min = vertex->val;

        if (r < min) return 0;
        if (l > max) return 0;
        if (l <= min && r >= max) return head->sum;
        if (l <= min) {
            std::pair<Node*, Node*> p = split(head, next(head, r)->val, false);
            int64_t ans = p.first->sum;
            head = merge(p.first, p.second);
            return ans;
        }
        if (r >= max) {
            std::pair<Node*, Node*> p = split(head, prev(head, l)->val, true);
            int64_t ans = p.second->sum;
            head = merge(p.first, p.second);
            return ans;
        }
        std::pair<Node*, Node*> p1 = split(head, next(head, r)->val, false);
        std::pair<Node*, Node*> p2 = split(p1.first, prev(p1.first, l)->val, true);
        int64_t ans = (p2.second != nullptr) ? p2.second->sum : 0;
        head = merge(merge(p2.first, p2.second), p1.second);
        return ans;
    }

    int64_t getSize() const {
        return size;
    }

    void print() const {
        std::cout << "size: " <<  size << std::endl;
        if (size > 0) _print(head);
    }

private:
    static void _print(Node* vertex) {
        std::cout << vertex->val << " sum: " << vertex->sum << " ch: ";
        std::cout << (vertex->parent == nullptr ? -1 : vertex->parent->val) << " ";
        std::cout << (vertex->left == nullptr ? -1 : vertex->left->val) << " ";
        std::cout << (vertex->right == nullptr ? -1 : vertex->right->val) << " ";
        std::cout << std::endl;
        if (vertex->left != nullptr) _print(vertex->left);
        if (vertex->right != nullptr) _print(vertex->right);
    }

    static Node* next(Node* vertex, int64_t x) {
        if (vertex->val <= x && vertex->right != nullptr) return next(vertex->right, x);
        if (vertex->val <= x && vertex->right == nullptr) return nullptr;
        if (vertex->left == nullptr) return vertex;
        Node* fromLeft = next(vertex->left, x);
        if (fromLeft == nullptr) return vertex;
        return fromLeft;
    }

    static Node* prev(Node* vertex, int64_t x) {
        if (vertex->val >= x && vertex->left != nullptr) return prev(vertex->left, x);
        if (vertex->val >= x && vertex->left == nullptr) return nullptr;
        if (vertex->right == nullptr) return vertex;
        Node* fromRight = prev(vertex->right, x);
        if (fromRight == nullptr) return vertex;
        return fromRight;
    }

    static void _add(Node* vertex, Node* node) {
        vertex->sum += node->val;
        if      (vertex->val >= node->val && vertex->left != nullptr) _add(vertex->left, node);
        else if (vertex->val < node->val && vertex->right != nullptr) _add(vertex->right, node);
        else if (vertex->val >= node->val && vertex->left == nullptr) {
            node->parent = vertex;
            vertex->left = node;
        } else if (vertex->val < node->val && vertex->right == nullptr) {
            node->parent = vertex;
            vertex->right = node;
        }
    }

    static void splay(Node* x) {
        while (x->parent != nullptr) {
            if (x->parent->parent == nullptr) {
                if (x == x->parent->left)   zigRight(x);
                else                        zigLeft(x);
                x = x->parent;
            } else {
                if (x->parent->parent->left == x->parent && x->parent->left == x) {
                    zigZigRight(x);
                } else if (x->parent->parent->left == x->parent && x->parent->right == x) {
                    zigZagRight(x);
                } else if (x->parent->parent->right == x->parent && x->parent->left == x) {
                    zigZagLeft(x);
                } else if (x->parent->parent->right == x->parent && x->parent->right == x) {
                    zigZigLeft(x);
                }
                x = x->parent;
            }
        }
    }

    static std::pair<Node*, Node*> split(Node* node, int64_t x, bool arg=true) {
        Node* vertex = node;
        while (true) {
            if (vertex == nullptr) return {nullptr, nullptr};
            if (vertex->val == x)  break;
            if (vertex->val < x)   vertex = vertex->right;
            else                   vertex = vertex->left;
        }
        splay(vertex);
        if (arg) {
            if (node->right != nullptr) {
                node->right->parent = nullptr;
                node->sum -= node->right->sum;
                Node* right = node->right;
                node->right = nullptr;
                return {node, right};
            } else {
                return {node, nullptr};
            }
        } else {
            if (node->left != nullptr) {
                node->left->parent = nullptr;
                node->sum -= node->left->sum;
                Node* left = node->left;
                node->left = nullptr;
                return {left, node};
            } else {
                return {nullptr, node};
            }
        }
    }

    static Node* merge(Node* left, Node* right) {
        if (left == nullptr) return right;
        if (right == nullptr) return left;
        Node* mostRightFromLeft = left;
        while (mostRightFromLeft->right != nullptr) {
            mostRightFromLeft = mostRightFromLeft->right;
        }
        Node* vertex = left;
        while (vertex->right != nullptr) {
            vertex->sum -= mostRightFromLeft->val;
            vertex = vertex->right;
        }

        if (mostRightFromLeft == left && mostRightFromLeft->left != nullptr) {
            left = mostRightFromLeft->left;
            left->parent = nullptr;
        } else if (mostRightFromLeft == left && mostRightFromLeft->left == nullptr) {
            left = nullptr;
        } else if (mostRightFromLeft != left && mostRightFromLeft->left != nullptr) {
            mostRightFromLeft->parent->right = mostRightFromLeft->left;
            mostRightFromLeft->left->parent = mostRightFromLeft->parent;
        } else if (mostRightFromLeft != left && mostRightFromLeft->left == nullptr) {
            mostRightFromLeft->parent->right = nullptr;
        }

        mostRightFromLeft->parent = nullptr;
        mostRightFromLeft->left = left;
        mostRightFromLeft->right = right;
        if (left == nullptr) mostRightFromLeft->sum = right->sum + mostRightFromLeft->val;
        if (left != nullptr) {
            mostRightFromLeft->sum = left->sum + right->sum + mostRightFromLeft->val;
            left->parent = mostRightFromLeft;
        }
        if (right != nullptr) right->parent = mostRightFromLeft;

        return mostRightFromLeft;
    }

    static void zigRight(Node* x) {
        Node* a = x->left;
        Node* b = x->right;
        Node* p = x->parent;
        Node* c = p->right;

        std::swap(x->val, p->val);
        x->sum += x->val;
        x->sum -= p->val;

        p->left = a;
        if (a != nullptr) a->parent = p;
        p->right = x;
        x->left = b;
        x->right = c;
        if (c != nullptr) c->parent = x;
        if (c != nullptr) x->sum += c->sum;
        if (a != nullptr) x->sum -= a->sum;
    }

    static void zigLeft(Node* x) {
        Node* a = x->right;
        Node* b = x->left;
        Node* p = x->parent;
        Node* c = p->left;

        std::swap(x->val, p->val);
        x->sum += x->val;
        x->sum -= p->val;

        p->right = a;
        if (a != nullptr) a->parent = p;
        p->left = x;
        x->right = b;
        x->left = c;
        if (c != nullptr) c->parent = x;
        if (c != nullptr) x->sum += c->sum;
        if (a != nullptr) x->sum -= a->sum;
    }

    static void zigZigRight(Node* x) {
        zigRight(x->parent);
        zigRight(x);
    }

    static void zigZigLeft(Node* x) {
        zigLeft(x->parent);
        zigLeft(x);
    }

    static void zigZagRight(Node* x) {
        zigLeft(x);
        zigRight(x->parent);
    }

    static void zigZagLeft(Node* x) {
        zigRight(x);
        zigLeft(x->parent);
    }

    Node* head = nullptr;
    size_t size = 0;
};

int main() {
    SplayTree tree;

    int n; std::cin >> n;
    bool needToPlus = false;
    int64_t y = 0;
    for (int useless = 0; useless < n; ++useless) {
        char c; std::cin >> c;
        if (c == '+') {
            int64_t x; std::cin >> x;
            if (needToPlus) x = (x+y) % 1000000000;
            needToPlus = false;
            tree.add(x);
        } else if (c == '?') {
            int64_t l, r; std::cin >> l >> r;
            needToPlus = true;
            y = tree.getSum(l, r);
            std::cout << y << std::endl;
        }
    }
}