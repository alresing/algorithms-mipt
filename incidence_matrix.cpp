#include <iostream>
#include <vector>


int main() {
    int n, m; std::cin >> n >> m;

    std::vector<std::vector<int>> matrix;
    for (int i = 0; i < n; ++i) {
        std::vector<int> row(m, 0);
        matrix.push_back(row);
    }

    for (int i = 0; i < m; ++i) {
        int v, u; std::cin >> v >> u;
        matrix[v-1][i] = 1;
        matrix[u-1][i] = 1;
    }

    for (const auto& row : matrix) {
        for (const auto& i : row)
            std::cout << i << " ";
        std::cout << std::endl;
    }
}
