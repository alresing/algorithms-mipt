#include <iostream>
#include <vector>


bool augment(const std::vector<std::vector<size_t>>& g, std::vector<bool>& used,
        std::vector<int64_t>& match, int64_t v) {
    if (used[v])
        return false;
    used[v] = true;
    for (int64_t to: g[v]) {
        if ((match[to] == -1) || augment(g, used, match, match[to])) {
            match[to] = v;
            return true;
        }
    }
    return false;
}


std::vector<int> kun(const std::vector<std::vector<size_t>>& g, size_t n) {
    std::vector<bool> used(g.size(), false);
    std::vector<int64_t> match(g.size(), -1);
    for (uint64_t v = 0; v < g.size(); ++v) {
        used.assign(g.size(), false);
        augment(g, used, match, v);
    }

    std::vector<int> res(n, -1);
    for (uint64_t i = 0; i < g.size(); ++i)
        if (match[i] != -1)
            res[match[i]] = i - n;

    return res;
}


void dfs(std::vector<bool>& used, std::vector<int>& g, size_t& res, size_t v) {
    if (used[v])
        return;
    used[v] = true;
    if (g[v] == -1) {
        ++res;
        return;
    }
    dfs(used, g, res, g[v]);
}


int main() {
    size_t n, m;
    std::cin >> n >> m;
    std::vector<std::vector<size_t>> graph(2*n);
    for (size_t i = 0; i < m; ++i) {
        size_t v, u;
        std::cin >> v >> u;
        --v; --u;
        graph[v].push_back(u + n);
    }

    std::vector<bool> used(n, false);
    std::vector<int> g = kun(graph, n);

    size_t res = 0;
    for (size_t i = 0; i < g.size(); ++i)
        dfs(used, g, res, i);

    std::cout << res << std::endl;
}
