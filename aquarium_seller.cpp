#include <iostream>
#include <vector>


const int64_t INF = 1152921504606846976;


int64_t pow2(const int64_t& x) {
    int64_t pow = 0;
    int64_t now = 1;
    while (pow < x) {
        ++pow;
        now *= 2;
    }
    return now;
}


int main() {
    int64_t n; std::cin >> n;
    std::vector<std::vector<int64_t>> weight;
    for (int64_t i = 0; i < n; ++i) {
        std::vector<int64_t> temp;
        for (int64_t j = 0; j < n; ++j) {
            int64_t p; std::cin >> p;
            temp.push_back(p);
        }
        weight.push_back(temp);
    }

    std::vector<std::vector<int64_t>> dp;
    int64_t pow2n = pow2(n);
    for (int64_t i = 0; i < n; ++i) {
        std::vector<int64_t> temp(pow2n, INF);
        dp.push_back(temp);
    }

    for (int64_t i = 0; i < n; ++i) {
        dp[i][0] = 0;
        dp[i][1 << i] = 0;
    }

    for (int64_t mask = 1; mask < pow2n; ++mask) {
        for (int64_t i = 0; i < n; ++i) {
            for (int64_t j = 0; j < n; ++j) {
                if ((mask >> j) & 1)
                    dp[i][mask] = std::min(dp[i][mask], weight[i][j] + dp[j][mask - (1 << j)]);
            }
        }
    }
    
    int64_t best_index = 0;
    for (int64_t i = 0; i < n; ++i) {
        if (dp[best_index][pow2n-1] > dp[i][pow2n-1]) {
            best_index = i;
        }
    }

    std::cout << dp[best_index][pow2n-1] << std::endl;

    int64_t mask = pow2n-1;
    mask -= (1 << best_index);
    std::cout << (1+best_index) << " ";
    while (mask != 0) {
        for (int64_t j = 0; j < n; ++j) {
            if (((mask >> j) & 1) && (dp[best_index][mask] == (weight[best_index][j] + dp[j][mask - (1 << j)]))) {
                std::cout << (1+j) << " ";
                best_index = j;
                mask -= (1 << j);
            }
        }
    }
    std::cout << std::endl;
}
